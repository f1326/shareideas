﻿using System;

namespace Forum.Services.Exceptions
{
    public class EntityNotFoundException :ApplicationException
    {
        public EntityNotFoundException(string msg)
            :base(msg)
        {
        }
    }
}
