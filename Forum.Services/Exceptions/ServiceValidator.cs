﻿using System;

namespace Forum.Services.Exceptions
{
    public static class ServiceValidator
    {
        public static void ValidateNotNull(Object item,string msg)
        {
            if (item == null)
            {
                throw new EntityNotFoundException(msg);
            }
        }
    }
}
