﻿using Forum.Services.Providers.Contracts;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace Forum.Services.Providers
{
    public class SortProvider : ISortProvider
    {
        public IQueryable<T> Sort<T>(string column, string direction, IQueryable<T> collection)
        {
            if (!(string.IsNullOrEmpty(column) || string.IsNullOrEmpty(direction)))
            {
                string sortingType = "OrderBy";
                if (direction.ToLower() == "descending")
                {
                    sortingType = "OrderByDescending";
                }

                try
                {
                    var parameter = Expression.Parameter(typeof(T), "x");
                    var property = Expression.Property(parameter, column);
                    var lambda = Expression.Lambda(property, parameter);

                    var orderByMethod = typeof(Queryable).GetMethods().First(x => x.Name == sortingType && x.GetParameters().Length == 2);
                    var orderByGeneric = orderByMethod.MakeGenericMethod(typeof(T), property.Type);
                    var result = orderByGeneric.Invoke(null, new object[] { collection, lambda });
                    collection = (IQueryable<T>)result;

                }
                catch (Exception)
                {
                    return  collection;
                }
            }

            return  collection;
        }
    }
}
