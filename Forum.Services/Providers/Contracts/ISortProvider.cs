﻿using System.Linq;

namespace Forum.Services.Providers.Contracts
{
    public interface ISortProvider
    {        IQueryable<T> Sort<T>(string column, string direction, IQueryable<T> collection);
    }
}
