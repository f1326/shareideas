﻿using System.Linq;
using System.Threading.Tasks;

namespace Forum.Services.Providers.Contracts
{
    public interface IFilterProvider
    {
        Task<IQueryable<T>> FilterAsync<T>(IQueryable<T> collection, string filteringPairs);
    }
}
