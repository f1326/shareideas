﻿using Forum.Services.Providers.Contracts;

using Microsoft.EntityFrameworkCore;

using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Forum.Services.Providers
{
    public class FilterProvider : IFilterProvider
    {
        // filteringPairs: "filterColumn1=filterValue1&filterColumn2=filterValue2"
        public async Task<IQueryable<T>> FilterAsync<T>(IQueryable<T> collection, string filteringPairs)
        {
            var typeValuePairs = filteringPairs.Split("&");
            foreach (var pair in typeValuePairs)
            {
                var pairParts = pair.Split("=");
                var filterColumn = pairParts[0];
                var filterValue = pairParts[1];
                if (!string.IsNullOrEmpty(filterColumn) && !string.IsNullOrEmpty(filterValue))
                {
                    if (char.IsLower(filterColumn[0]))
                    {
                        filterColumn = char.ToUpper(filterColumn[0]) + filterColumn.Substring(1, filterColumn.Length - 1);
                    }
                  
                    var firstEntity = await collection.FirstOrDefaultAsync();
                    var prop = firstEntity.GetType().GetProperty(filterColumn)?.GetValue(firstEntity);
                    if (firstEntity != null && prop != null)
                    {
                        var parameter = Expression.Parameter(typeof(T), "x");
                        var property = Expression.Property(parameter, filterColumn);
                        object value = filterValue;
                        if (property.Type != typeof(string))
                        {
                            value = Convert.ChangeType(value, property.Type);
                        }
                        var lambda = Expression.Lambda<Func<T, bool>>(Expression.Equal(property, Expression.Constant(value)), parameter);
                        collection = collection.Where(lambda);
                        var list = collection;
                    }
                }
            }

            return collection; 
        }
    }
}
