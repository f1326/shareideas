﻿namespace Forum.Services.Models
{
    public class CommentRequestDto
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public int UserId { get; set; }
        public int PostId { get; set; }
    }
}
