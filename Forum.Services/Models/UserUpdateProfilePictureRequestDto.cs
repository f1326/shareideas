﻿using Microsoft.AspNetCore.Http;

namespace Forum.Services.Models
{
    public class UserUpdateProfilePictureRequestDto
    {
        public int Id { get; set; }
        public string Password { get; set; }
        public IFormFile Image { get; set; }
    }
}
