﻿namespace Forum.Services.Models
{
    public class PostRequestDto
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public int UserId { get; set; }
    }
}
