﻿using System;
using System.Collections.Generic;

using Forum.Data.Models;
using Forum.Services.Models.Contracts;

namespace Forum.Services.Models
{
    public class RequestMapper : IRequestMapper
    {

        public Like ToLikeModel(LikeRequestDto model)
        {
            return new Like()
            {
                UserId = model.UserId,
                PostId = model.PostId
            };
        }
        public  Comment ToCommentModel(CommentRequestDto model)
        {
            return new Comment()
            {
                Title = model.Title,
                Content = model.Content,
                CreatedAt = DateTime.UtcNow,
                UserId = model.UserId,
                PostId = model.PostId,
            };
        }
      
        public Post ToPostModel(PostRequestDto model)
        {
            return new Post()
            {
                Title = model.Title,
                Content = model.Content,
                CreatedAt = DateTime.UtcNow,
                UserId = model.UserId,
                Likes = new List<Like>(),
                Comments = new List<Comment>()
            };
        }

        public User ToUserModel(UserRequestDto model)
        {

            var user =  new User()
            {
                Username = model.Username,
                Password = model.Password,
                Email = model.Email,
                DisplayName = model.DisplayName,
                IsActive = true,
                Rating = 0,
                MemberSince = DateTime.UtcNow,
                Role = Data.Models.Enums.UserRole.Member,
                Blocked = false,
                ProfileImage = "/images/userDefault.png", 
                Posts = new List<Post>(),
                Comments = new List<Comment>()
            };

            return user;
        }
    }
}
