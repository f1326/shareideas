﻿using Forum.Data.Models;

namespace Forum.Services.Models.Contracts
{
    public interface IRequestMapper
    {
        Like ToLikeModel(LikeRequestDto model);
        Comment ToCommentModel(CommentRequestDto model);
        User ToUserModel(UserRequestDto model);
        Post ToPostModel(PostRequestDto model);
    }
}
