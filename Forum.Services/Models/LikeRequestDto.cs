﻿namespace Forum.Services.Models
{
    public class LikeRequestDto
    {
        public int UserId { get; set; }
        public int PostId { get; set; }
    }
}
