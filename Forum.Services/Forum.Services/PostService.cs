﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

using Forum.Data;
using Forum.Data.Models;
using Forum.Services.Exceptions;
using Forum.Services.Forum.Services.Contracts;
using Forum.Services.Forum.Services.DTOs;
using Forum.Services.Models;
using Forum.Services.Models.Contracts;
using Forum.Services.Providers.Contracts;

using Microsoft.EntityFrameworkCore;


namespace Forum.Services.Forum.Services
{
    public class PostService : IPostService
    {
        private readonly ForumDbContext dbContext;
        private readonly IUserService userService;
        private readonly IRequestMapper mapper;
        private readonly IFilterProvider filterProvider;
        private readonly ISortProvider sortProvider;

        public PostService(ForumDbContext dbContext,
            IUserService userService,
            IRequestMapper mapper,
            IFilterProvider filterProvider,
            ISortProvider sortProvider)
        {
            this.dbContext = dbContext;
            this.userService = userService;
            this.mapper = mapper;
            this.filterProvider = filterProvider;
            this.sortProvider = sortProvider;
        }
        private IQueryable<Post> Query
        {
            get
            {
                return this.dbContext.Posts
                  .Include(p => p.User)
                  .Include(p => p.Comments)
                  .ThenInclude(c => c.User);
            }
        }

        public async Task<PostResponseDto> CreateAsync(PostRequestDto requestModel)
        {
            var post = mapper.ToPostModel(requestModel);
            var user = await this.userService.GetUserAsync(requestModel.UserId);
            user.Rating += 10;
            post.User = user;
            this.dbContext.Posts.Add(post);
            await this.dbContext.SaveChangesAsync();
            return new PostResponseDto(post);
        }

        public async Task<PostResponseDto> AddCommentAsync(int id, int commentId)
        {
            var post = await this.GetPostAsync(id);
            var comment = this.dbContext.Comments.FirstOrDefault(c => c.Id == commentId);
            ServiceValidator.ValidateNotNull(comment, $"Comment with id: {commentId} doesn't exist!");
            post.Comments.Add(comment);
            await this.dbContext.SaveChangesAsync();

            return new PostResponseDto(post);
        }

        public async Task<PostResponseDto> DeleteAsync(int id)
        {
            var post = await this.GetPostAsync(id);
            this.dbContext.Posts.Remove(post);
            await this.dbContext.SaveChangesAsync();
            return new PostResponseDto(post);
        }

        public async Task<PostResponseDto> EditAsync(int id, PostRequestDto requestModel)
        {
            var postToUpdate = await this.GetPostAsync(id);
            var post = this.mapper.ToPostModel(requestModel);

            postToUpdate.Title = post.Title;
            postToUpdate.Content = post.Content;

            await this.dbContext.SaveChangesAsync();
            return new PostResponseDto(postToUpdate);
        }

        public async Task<IEnumerable<PostResponseDto>> GetPaggination(int skip, int take)
        {
            var result = await this.Query.Skip(skip).Take(take).Select(p => new PostResponseDto(p)).ToListAsync();
            var post2Likes = this.dbContext.Likes.Where(l => l.PostId == 2).Select(l => l.User.Email);
            foreach (var item in result)
            {
                item.Likes = this.dbContext.Likes.Where(l => l.PostId == item.Id).Select(l => l.User.Email).ToList();
            }

            return result;
        }

        public async Task<IEnumerable<PostResponseDto>> GetFilterPaggination(string sortingType, string sortingDirection,
                                                                            string startDate, string endDate, string containsTitle,
                                                                            int skip, int take)
        {
            var posts = this.Query;
            if (containsTitle != "")
            {
                posts = posts.Where(p => p.Title.ToLower().Contains(containsTitle.ToLower()));
            }
            if (startDate != "")
            {
                var dt = DateTime.ParseExact(startDate, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                posts = posts.Where(p => p.CreatedAt >= dt);
            }
            if (endDate != "")
            {
                var dt = DateTime.ParseExact(endDate, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                posts = posts.Where(p => p.CreatedAt <= dt);
            }
            switch (sortingType.ToLower())
            {
                case "date":
                    if (sortingDirection.ToLower() == "ascending")
                    {
                        var resultDateAsc = await posts.OrderBy(p => p.CreatedAt).Skip(skip).Take(take).Select(p => new PostResponseDto(p)).ToListAsync();
                        foreach (var item in resultDateAsc)
                        {
                            item.Likes = this.dbContext.Likes.Where(l => l.PostId == item.Id).Select(l => l.User.Email).ToList();
                        }
                        return resultDateAsc;
                    }
                    var resultDateDes = await posts.OrderByDescending(p => p.CreatedAt).Skip(skip).Take(take).Select(p => new PostResponseDto(p)).ToListAsync();
                    foreach (var item in resultDateDes)
                    {
                        item.Likes = this.dbContext.Likes.Where(l => l.PostId == item.Id).Select(l => l.User.Email).ToList();
                    }
                    return resultDateDes;
                case "comments":
                    if (sortingDirection.ToLower() == "ascending")
                    {
                        var resultCommentAsc = await posts.OrderBy(p => p.Comments.Count).Skip(skip).Take(take).Select(p => new PostResponseDto(p)).ToListAsync();
                        foreach (var item in resultCommentAsc)
                        {
                            item.Likes = this.dbContext.Likes.Where(l => l.PostId == item.Id).Select(l => l.User.Email).ToList();
                        }
                        return resultCommentAsc;
                    }
                    var resultCommentDesc = await posts.OrderByDescending(p => p.Comments.Count).Skip(skip).Take(take).Select(p => new PostResponseDto(p)).ToListAsync();
                    foreach (var item in resultCommentDesc)
                    {
                        item.Likes = this.dbContext.Likes.Where(l => l.PostId == item.Id).Select(l => l.User.Email).ToList();
                    }
                    return resultCommentDesc;
                case "likes": 
                    if (sortingDirection.ToLower() == "ascending")
                    {
                        var resultLikestAsc = await posts.Select(p => new PostResponseDto(p)).ToListAsync();
                        foreach (var item in resultLikestAsc)
                        {
                            item.Likes = this.dbContext.Likes.Where(l => l.PostId == item.Id).Select(l => l.User.Email).ToList();
                        }
                        return resultLikestAsc.OrderBy(p => p.Likes.Count()).Skip(skip).Take(take);
                    }
                    var resultLikestDesc = await posts.Select(p => new PostResponseDto(p)).ToListAsync();
                    foreach (var item in resultLikestDesc)
                    {
                        item.Likes = this.dbContext.Likes.Where(l => l.PostId == item.Id).Select(l => l.User.Email).ToList();
                    }
                    return resultLikestDesc.OrderByDescending(p => p.Likes.Count()).Skip(skip).Take(take);
                default:
                    var resultDefaultAsc = await posts.OrderBy(p => p.CreatedAt).Skip(skip).Take(take).Select(p => new PostResponseDto(p)).ToListAsync();
                    foreach (var item in resultDefaultAsc)
                    {
                        item.Likes = this.dbContext.Likes.Where(l => l.PostId == item.Id).Select(l => l.User.Email).ToList();
                    }
                    return resultDefaultAsc;
            }


        }

        public int GetFilterCount(string startDate, string endDate, string containsTitle)
        {
            var posts = this.Query;
            if (containsTitle != "")
            {
                posts = posts.Where(p => p.Title.ToLower().Contains(containsTitle.ToLower()));
            }
            if (startDate != "")
            {
                var dt = DateTime.ParseExact(startDate, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                posts = posts.Where(p => p.CreatedAt >= dt);
            }
            if (endDate != "")
            {
                var dt = DateTime.ParseExact(endDate, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                posts = posts.Where(p => p.CreatedAt <= dt);
            }

            return posts.Count();
        }

        public IQueryable<PostResponseDto> GetAll()
        {
            return this.Query
             .Select(p => new PostResponseDto(p));
        }

        public async Task<PostResponseDto> GetByIdAsync(int id)
        {
            var post = await this.GetPostAsync(id);
            return new PostResponseDto(post);
        }
        public async Task<Post> GetPostAsync(int id)
        {
            var post = await this.Query
              .FirstOrDefaultAsync(p => p.Id == id);
            ServiceValidator.ValidateNotNull(post, $"Post with Id: {id} doesnt exist!");

            return post;
        }


        public async Task<IEnumerable<PostResponseDto>> GetLatestPostsAsync(int n)
        {
            var list = await this.dbContext.Posts.Include(p => p.User)
                .OrderByDescending(p => p.CreatedAt)
                .Take(n)
                .ToListAsync();

            var latestPostsDtos = list.Select(p => new PostResponseDto(p)).ToList();

            return latestPostsDtos;
        }

        public async Task<IEnumerable<PostResponseDto>> GetMostPopularPostsAsync(int n)
        {
            var postsList = await this.dbContext.Posts.Include(p => p.Comments).Include(p => p.User)
               .OrderByDescending(p => p.Comments.Count)
               .Take(n)
               .ToListAsync();

            var postsDtoFilteredList = postsList.Select(p => new PostResponseDto(p)).ToList();

            return postsDtoFilteredList;
        }

        public async Task<IEnumerable<PostResponseDto>> GetFilteredPostsAsync(string firstType, string firstValue,
            string secondType, string secondValue)
        {
            var posts = this.Query
                .Select(p => new PostResponseDto
                {
                    Id = p.Id,
                    Title = p.Title,
                    Content = p.Content,
                    User = p.User.Username,
                    DateCreated = p.CreatedAt,
                    UserEmail = p.User.Email,
                    DisplayName = p.User.DisplayName,
                    Comments = p.Comments.Select(c => new CommentResponseDto
                    {
                        Id = c.Id,
                        PostId = c.PostId,
                        UserDisplayName = c.User.DisplayName,
                        UserPicturePath = c.User.ProfileImage,
                        UserRating = c.User.Rating,
                        UserId = c.UserId,
                        Title = c.Title,
                        Content = c.Content,
                        CreatedDate = c.CreatedAt
                    }
                    ).ToList()
                });


            var filterPair = $"{firstType}={firstValue}&{secondType}={secondValue}";
            var filteredPosts = await this.filterProvider.FilterAsync<PostResponseDto>(posts, filterPair);

            return filteredPosts;

        }

        public IEnumerable<PostResponseDto> GetSortedPosts(string sortingType, string direction)
        {
            var posts = this.Query
               .Select(p => new PostResponseDto
               {
                   Id = p.Id,
                   Title = p.Title,
                   Content = p.Content,
                   User = p.User.Username,
                   DateCreated = p.CreatedAt,
                   UserEmail = p.User.Email,
                   DisplayName = p.User.DisplayName,
                   Comments = p.Comments.Select(c => new CommentResponseDto
                   {
                       Id = c.Id,
                       PostId = c.PostId,
                       UserDisplayName = c.User.DisplayName,
                       UserPicturePath = c.User.ProfileImage,
                       UserRating = c.User.Rating,
                       UserId = c.UserId,
                       Title = c.Title,
                       Content = c.Content,
                       CreatedDate = c.CreatedAt
                   }
                   ).ToList()
               });

            if (char.IsLower(sortingType[0]))
            {
                sortingType = char.ToUpper(sortingType[0]) + sortingType.Substring(1, sortingType.Length - 1);
            }

            var sortedPosts = this.sortProvider.Sort<PostResponseDto>(sortingType, direction, posts);

            return sortedPosts;
        }

        public int GetCount()
        {
            return this.dbContext.Posts.Count();
        }

        public ICollection<string> GetPostLikes(int id)
        {
            return this.dbContext.Likes.Where(l => l.PostId == id).Select(l => l.User.Email).ToList();
        }

        public async Task<LikeResponseDto> LikePost(LikeRequestDto likeRequestDto)
        {
            var like = this.mapper.ToLikeModel(likeRequestDto);
            like.Post = await this.GetPostAsync(like.PostId);
            like.User = await this.userService.GetUserMinAsync(likeRequestDto.UserId);
            //TODO add validation
            this.dbContext.Likes.Add(like);
            await this.dbContext.SaveChangesAsync();

            return new LikeResponseDto(like);
        }

        public async Task<LikeResponseDto> RemoveLike(LikeRequestDto likeRequestDto)
        {
            var like = this.dbContext.Likes.Include(l => l.User).FirstOrDefault(like => like.UserId == likeRequestDto.UserId
                                                                && like.PostId == likeRequestDto.PostId);
            this.dbContext.Likes.Remove(like);

            await this.dbContext.SaveChangesAsync();

            return new LikeResponseDto(like);
        }

        public async Task<int> CountPosts()
        {
            var postsCount = await this.dbContext.Posts.CountAsync();

            return postsCount;
        }
    }
}
