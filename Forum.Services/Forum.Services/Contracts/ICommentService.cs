﻿using Forum.Data.Models;
using Forum.Services.Forum.Services.DTOs;
using Forum.Services.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Forum.Services.Forum.Services.Contracts
{
    public interface ICommentService
    {
        Task<CommentResponseDto> CreateAsync(CommentRequestDto requestModel);

        Task<CommentResponseDto> DeleteAsync(int id);

        Task<CommentResponseDto> UpdateAsync(int id, CommentRequestDto requestModel);

        Task<CommentResponseDto> GetCommentByIdAsync(int id);
        Task<Comment> GetCommentAsync(int id);
        Task<IEnumerable<CommentResponseDto>> GetAllAsync();
    }
}
