﻿using Forum.Data.Models;
using Forum.Services.Forum.Services.DTOs;
using Forum.Services.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Forum.Services.Forum.Services.Contracts
{
    public interface IUserService
    {
        Task<UserResponseDto> CreateAsync(UserRequestDto requestModel);
        Task<UserResponseDto> DeleteAsync(int id);
        Task<UserResponseDto> UpdateAsync(int id, UserRequestDto requestModel);
        Task<UserResponseDto> GetUserByIdAsync(int id);
        Task<User> GetUserMinAsync(int id);
        Task<User> GetUserAsync(int id);
        Task<IEnumerable<UserResponseDto>> GetAllAsync();
        Task<UserResponseDto> GetUserByEmailAsync(string email);
        Task<bool> CheckUsernamePasswordIsCorrectAsync(int id, string password);
        Task<bool> ExistUserWithEmailAsync(string email);
        Task<bool> ExistUserWithUsernameAsync(string username);
        Task<UserResponseDto> SetRoleToMemberAsync(int id);
        Task<UserResponseDto> SetRoleToAdminAsync(int id);
        Task<UserResponseDto> SetRoleToModeratorAsync(int id);
        Task<UserResponseDto> UpdateProfileImageAsync(int id, UserUpdateProfilePictureRequestDto userUpdatePictureRequestDto, string imagePath, string extension);
        Task<UserResponseDto> UpdateDisplayNameAsync(int id, string newDisplayName);
        Task<UserResponseDto> UpdatePasswordAsync(int id, string newPassword);
        Task<UserResponseDto> UpdateEmailAsync(int id, string newEmail);
        Task<UserResponseDto> BlockUser(int id);
        Task<UserResponseDto> UnblockUser(int id);
        Task<IEnumerable<UserResponseDto>> SearchByDisplayName(string displayName);
        Task<UserResponseDto> SearchByUsername(string username);
        Task<UserRegistrationDto> RegisterAsync(UserRequestDto requestModel);
        Task<string> ActivateAccountAsync(int userId, string activationCode);
        Task<PaginationResponseDto<UserResponseDto>> GetAllPaginationAsync(int skip, int take, int? rating, string sortType, string sortDirection, string userStatus, string searchValue);
        Task<int> GetUsersCountAsync();
        IQueryable<User> Search(string seatchValue);
    }
}
