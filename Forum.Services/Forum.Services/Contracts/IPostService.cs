﻿using Forum.Data.Models;
using Forum.Services.Forum.Services.DTOs;
using Forum.Services.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Forum.Services.Forum.Services.Contracts
{
    public interface IPostService
    {
        Task<PostResponseDto> CreateAsync(PostRequestDto requestModel);
        Task<PostResponseDto> AddCommentAsync(int id, int commentId);
        Task<IEnumerable<PostResponseDto>> GetPaggination(int skip, int take);
        int GetCount();
        Task<PostResponseDto> DeleteAsync(int id);
        Task<Post> GetPostAsync(int id);
        Task<PostResponseDto> EditAsync(int id, PostRequestDto requestModel);
        IQueryable<PostResponseDto> GetAll();
        Task<PostResponseDto> GetByIdAsync(int id);
        Task<IEnumerable<PostResponseDto>> GetLatestPostsAsync(int n);
        Task<IEnumerable<PostResponseDto>> GetMostPopularPostsAsync(int n);
        IEnumerable<PostResponseDto> GetSortedPosts(string sortingType, string direction);
        Task<IEnumerable<PostResponseDto>> GetFilteredPostsAsync(string firstType, string firstValue,
            string secondType, string secondValue);
        ICollection<string> GetPostLikes(int id);
        Task<LikeResponseDto> LikePost(LikeRequestDto likeRequestDto);
        Task<LikeResponseDto> RemoveLike(LikeRequestDto likeRequestDto);

        Task<IEnumerable<PostResponseDto>> GetFilterPaggination(string sortingType, string sortingDirection,
                                                                            string startDate, string endDate, string containsTitle,
                                                                            int skip, int take);
        int GetFilterCount(string startDate, string endDate, string containsTitle);
        Task<int> CountPosts();
    }
}
