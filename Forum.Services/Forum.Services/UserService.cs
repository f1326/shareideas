﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

using Forum.Data;
using Forum.Data.Models;
using Forum.Services.Exceptions;
using Forum.Services.Forum.Services.Contracts;
using Forum.Services.Forum.Services.DTOs;
using Forum.Services.Models;
using Forum.Services.Models.Contracts;
using Forum.Services.Providers.Contracts;

using Microsoft.EntityFrameworkCore;

namespace Forum.Services.Forum.Services
{
    public class UserService : IUserService
    {
        private readonly ForumDbContext dbContext;
        private readonly IRequestMapper mapper;
        private readonly IFilterProvider filterProvider;
        private readonly ISortProvider sortProvider;

        public UserService(ForumDbContext dbContext, IRequestMapper mapper, IFilterProvider filterProvider, ISortProvider sortProvider)
        {
            this.dbContext = dbContext;
            this.mapper = mapper;
            this.filterProvider = filterProvider;
            this.sortProvider = sortProvider;
        }

        private IQueryable<User> Query
        {
            get
            {
                return this.dbContext.Users
                   .Include(u => u.Posts)
                   .ThenInclude(p => p.Comments)
                   .Include(u => u.Posts)
                   .Include(u => u.Comments)
                   .ThenInclude(c => c.Post);
            }
        }
        public async Task<User> GetUserMinAsync(int id)
        {
            var user = await this.dbContext.Users.FirstOrDefaultAsync(u => u.Id == id);
            ServiceValidator.ValidateNotNull(user, $"User with Id: {id} does not exist!");
            return user;
        }
        public async Task<UserResponseDto> CreateAsync(UserRequestDto requestModel)
        {
            var user = this.mapper.ToUserModel(requestModel);
            this.dbContext.Users.Add(user);
            await this.dbContext.SaveChangesAsync();
            return new UserResponseDto(user);
        }

        public async Task<UserResponseDto> DeleteAsync(int id)
        {
            var userTodelete = await this.GetUserAsync(id);
            this.dbContext.Users.Remove(userTodelete);
            await this.dbContext.SaveChangesAsync();

            return new UserResponseDto(userTodelete);
        }

        public async Task<UserResponseDto> UpdateAsync(int id, UserRequestDto requestModel)
        {
            var userToUpdate = await this.GetUserAsync(id);
            var user = this.mapper.ToUserModel(requestModel);
            userToUpdate.DisplayName = user.DisplayName;
            userToUpdate.Email = user.Email;
            userToUpdate.Password = userToUpdate.Password;
            userToUpdate.Username = userToUpdate.Username;

            await this.dbContext.SaveChangesAsync();
            return new UserResponseDto(userToUpdate);
        }
        public async Task<UserResponseDto> GetUserByIdAsync(int id)
        {
            var user = await this.GetUserMinAsync(id);
            return new UserResponseDto(user);
        }
        public async Task<User> GetUserAsync(int id)
        {
            var user = await this.Query.FirstOrDefaultAsync(user => user.Id == id); // SO SLOW !
            ServiceValidator.ValidateNotNull(user, $"User with Id: {id} doesnt exist!");
            return user;
        }

        public async Task<IEnumerable<UserResponseDto>> GetAllAsync()
        {
            return await this.Query.Select(user => new UserResponseDto(user)).ToListAsync();
        }

        public async Task<PaginationResponseDto<UserResponseDto>> GetAllPaginationAsync
            (int skip, int take, int? rating, string sortType, string sortDirection, string userStatus, string searchValue)
        {
            //filterColumn1 = filterValue1 & filterColumn2 = filterValue2

            bool userIsBlocked = false;
            bool hasStatusFilter = false;
            if (userStatus != null)
            {
                const string Blocked = "Blocked";
                const string Unblocked = "Unblocked";

                if (userStatus.Equals(Blocked, StringComparison.CurrentCultureIgnoreCase))
                {
                    userIsBlocked = true;
                    hasStatusFilter = true;
                }
                else if (userStatus.Equals(Unblocked, StringComparison.CurrentCultureIgnoreCase))
                {
                    hasStatusFilter = true;
                }

            }

            StringBuilder filteringPairsBuilder = new StringBuilder();
            if (hasStatusFilter)
            {
                filteringPairsBuilder.Append($"Blocked={userIsBlocked}&");
            }

            filteringPairsBuilder.Append($"Rating={rating}");
            var filterinPairs = filteringPairsBuilder.ToString();
            var list = this.Query;

            if (!string.IsNullOrWhiteSpace(searchValue))
            {
                list = Search(searchValue);
            }

            var filteredList = await this.filterProvider.FilterAsync<User>(list, filterinPairs);
            var totalItemsCount = filteredList.Count();

            IEnumerable<User> users;
            if (sortDirection == null || sortType == null)
            {
                users = await filteredList
               .OrderBy(u => u.Id)
               .Skip(skip)
               .Take(take)
               .ToListAsync();
            }
            else
            {
                var usersQuery = this.sortProvider.Sort<User>(sortType, sortDirection, filteredList);
                users = await usersQuery
                   .Skip(skip)
                   .Take(take)
                   .ToListAsync();
            }

            var userResponseDtos = users.Select(u => new UserResponseDto(u)).ToList();
            var paginationResponse = new PaginationResponseDto<UserResponseDto>
            {
                Items = userResponseDtos,
                TotalItemsCount = totalItemsCount
            };

            return paginationResponse;
        }

        public async Task<UserResponseDto> GetUserByEmailAsync(string email)
        {
            var user = await this.GetUserByEmail(email);
            return new UserResponseDto(user);
        }
        async Task<User> GetUserByEmail(string email)
        {
            var user = await this.dbContext.Users.FirstOrDefaultAsync(user => user.Email == email);
            ServiceValidator.ValidateNotNull(user, $"User with Email: {email} does not exist!");
            return user;
        }

        public async Task<bool> CheckUsernamePasswordIsCorrectAsync(int id, string password)
        {
            var user = await this.GetUserAsync(id);
            if (user.Password == password)
            {
                return true;
            }

            return false;
        }

        public async Task<bool> ExistUserWithEmailAsync(string email)
        {
            return await this.dbContext.Users.AnyAsync(user => user.Email == email);
        }
        public async Task<bool> ExistUserWithUsernameAsync(string username)
        {
            return await this.dbContext.Users.AnyAsync(user => user.Username == username);
        }

        public async Task<UserResponseDto> SetRoleToMemberAsync(int id)
        {
            var user = await this.GetUserAsync(id);

            user.Role = Data.Models.Enums.UserRole.Member;
            await this.dbContext.SaveChangesAsync();
            return new UserResponseDto(user);
        }

        public async Task<UserResponseDto> SetRoleToAdminAsync(int id)
        {
            var user = await this.GetUserAsync(id);
            user.Role = Data.Models.Enums.UserRole.Admin;
            await this.dbContext.SaveChangesAsync();
            return new UserResponseDto(user);
        }

        public async Task<UserResponseDto> SetRoleToModeratorAsync(int id)
        {
            var user = await this.GetUserAsync(id);
            user.Role = Data.Models.Enums.UserRole.Moderator;
            await this.dbContext.SaveChangesAsync();
            return new UserResponseDto(user);
        }

        public async Task<UserResponseDto> UpdateProfileImageAsync(int id, UserUpdateProfilePictureRequestDto userUpdatePictureRequestDto, string imagePath, string extension)
        {
            var user = await this.GetUserMinAsync(id);


            var allowedExtensions = new[] { "jpg", "png" };

            if (!allowedExtensions.Contains(extension))
            {
                throw new Exception("Invalid image extension.");
            }

            var physicalImagePath = $"{imagePath}\\{id}.{extension}";
            using (Stream fileStream = new FileStream(physicalImagePath, FileMode.Create))
            {
                await userUpdatePictureRequestDto.Image.CopyToAsync(fileStream);
            }
            var parts = physicalImagePath.Split("wwwroot\\");
            var secontPart = parts[1];
            secontPart = secontPart.Replace('\\', '/');
            secontPart = $"/{secontPart}";
            user.ProfileImage = secontPart;
            await this.dbContext.SaveChangesAsync();
            return new UserResponseDto(user);
        }

        public async Task<UserResponseDto> UpdateDisplayNameAsync(int id, string newDisplayName)
        {
            var user = await this.GetUserAsync(id);
            user.DisplayName = newDisplayName;
            await this.dbContext.SaveChangesAsync();
            return new UserResponseDto(user);
        }

        public async Task<UserResponseDto> UpdateEmailAsync(int id, string newEmail)
        {
            var user = await this.GetUserAsync(id);
            user.Email = newEmail;
            await this.dbContext.SaveChangesAsync();
            return new UserResponseDto(user);
        }

        public async Task<UserResponseDto> UpdatePasswordAsync(int id, string newPassword)
        {
            var user = await this.GetUserAsync(id);
            user.Password = newPassword;
            await this.dbContext.SaveChangesAsync();
            return new UserResponseDto(user);
        }

        public async Task<UserResponseDto> BlockUser(int id)
        {
            var user = await this.GetUserAsync(id);

            if (user.Blocked == true)
            {
                throw new InvalidOperationException($"User with id {id} is already blocked.");
            }

            user.Blocked = true;
            await this.dbContext.SaveChangesAsync();

            return new UserResponseDto(user);
        }

        public async Task<UserResponseDto> UnblockUser(int id)
        {
            var user = await this.GetUserAsync(id);
            if (user.Blocked == false)
            {
                throw new InvalidOperationException($"User with id {id} is already unblocked.");
            }

            user.Blocked = false;
            await this.dbContext.SaveChangesAsync();

            return new UserResponseDto(user);
        }

        public async Task<UserResponseDto> SearchByUsername(string username)
        {
            var user = await this.Query.FirstOrDefaultAsync(u => u.Username == username);
            // ServiceValidator.ValidateNotNull(user, $"User with username {username} does not exist.");

            return new UserResponseDto(user);
        }

        public async Task<IEnumerable<UserResponseDto>> SearchByDisplayName(string displayName)
        {
            var users = await this.Query.Where(u => u.DisplayName.Contains(displayName)).Select(u => new UserResponseDto(u)).ToListAsync();
            // ServiceValidator.ValidateNotNull(users, $"There are no users with display name {displayName}.");

            return users;
        }

        public IQueryable<User> Search(string seatchValue)
        {
            var users = this.Query
                .Where(u => u.Email.Contains(seatchValue)
                    || u.Username.Contains(seatchValue)
                    || u.DisplayName.Contains(seatchValue));

            return users;
        }

        public async Task<UserRegistrationDto> RegisterAsync(UserRequestDto requestModel)
        {
            var user = this.mapper.ToUserModel(requestModel);
            user.ActivationCode = Guid.NewGuid().ToString();
            this.dbContext.Users.Add(user);
            var i = await this.dbContext.SaveChangesAsync();
            string scheme = "http";
            string host = "localhost";
            string port = "5000";
            var userRegistrationDto = new UserRegistrationDto(user);
            if (i > 0)
            {
                SendVerificationLinkToEmail(user.Email, user.ActivationCode, user.Id, scheme, host, port);
                userRegistrationDto.Message = $"Successfull registration. Please check your email:{userRegistrationDto.Email}, for activation link.";
                userRegistrationDto.Successfull = true;

                return userRegistrationDto;
            }
            else
            {
                userRegistrationDto.Message = "Registration has failed. Please try again.";

                return userRegistrationDto;
            }

        }

        private void SendVerificationLinkToEmail(string email, string activationCode, int id, string scheme, string host, string port)
        {
            var verificationUrl = scheme + "://" + host + ":" + port + "/Auth/ActivateAccount?userId=" + id.ToString() + "&activationCode=" + activationCode;
            var fromEmail = new MailAddress("shareideas.forum2021@gmail.com");
            var toEmail = new MailAddress(email);
            var fromEmailPassword = "Forum10122021";
            var subject = "Your account is accepted";
            var body = "<br/><br/>Your account is successfully created. Please click on the below link to verify your account <br/><br/><a href='" + verificationUrl + "'>" + verificationUrl + "</a> ";
            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword)
            };

            using (var message = new MailMessage(fromEmail, toEmail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
                smtp.Send(message);
        }

        public async Task<string> ActivateAccountAsync(int userId, string activationCode)
        {
            var user = await this.GetUserAsync(userId);
            if (user.ActivationCode != activationCode)
            {
                throw new UnauthorizedAccessException("Unsuccessfull activation of the registration.");
            }

            user.IsActive = true;
            return $"Successfull activation of {user.Email}.";
        }

        public async Task<int> GetUsersCountAsync()
        {
            var usersCount = await this.dbContext.Users.CountAsync();

            return usersCount;
        }
    }
}
