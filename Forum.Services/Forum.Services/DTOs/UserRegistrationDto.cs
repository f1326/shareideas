﻿using Forum.Data.Models;

namespace Forum.Services.Forum.Services.DTOs
{
    public class UserRegistrationDto : UserResponseDto
    {
        public UserRegistrationDto()
        {
        }
        public UserRegistrationDto(User user)
            : base(user)
        {
        }
        public string Message { get; set; }
        public bool Successfull { get; set; }
    }
}
