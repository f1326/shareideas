﻿using Forum.Data.Models;

namespace Forum.Services.Forum.Services.DTOs
{
    public class LikeResponseDto
    {
        public LikeResponseDto()
        {

        }
        public LikeResponseDto(Like like)
        {
            this.userEmail = like.User.Email;
        }
        public string userEmail { get; set; }
    }
}
