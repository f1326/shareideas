﻿using Forum.Data.Models;

using System;
using System.Collections.Generic;
using System.Linq;

namespace Forum.Services.Forum.Services.DTOs
{
    public class PostResponseDto
    {
        public PostResponseDto()
        {
        }
        public PostResponseDto(Post post)
        {
            this.Id = post.Id;
            this.Title = post.Title;
            this.Content = post.Content;
            this.DateCreated = post.CreatedAt;
            this.User = post.User.Username;
            if (post.Comments != null)
            {
                this.Comments = post.Comments.Select(c => new CommentResponseDto 
                {
                Id = c.Id,
                Title = c.Title, 
                Content = c.Content,
                UserDisplayName = c.User.DisplayName,
                UserPicturePath = c.User.ProfileImage,
                UserRating = c.User.Rating,
                UserId = c.UserId,
                CreatedDate = c.CreatedAt,
                PostId = c.PostId
                });
            }
            this.Likes = new List<string>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime DateCreated { get; set; }
        public IEnumerable<CommentResponseDto> Comments { get; set; }
        public IEnumerable<string> Likes { get; set; }
        public string User { get; set; }
        public string UserEmail { get; set; }
        public string DisplayName { get; set; }
        public int NumberOfComments => Comments.ToList().Count();
    }
}
