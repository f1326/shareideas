﻿using System;

using Forum.Data.Models;

namespace Forum.Services.Forum.Services.DTOs
{
    public class CommentResponseDto
    {
        public CommentResponseDto()
        {
        }
        public CommentResponseDto(Comment comment)
        {
           this.Id = comment.Id;
           this.Title = comment.Title;
           this.Content = comment.Content;
           this.UserDisplayName = comment.User.DisplayName;
           this.UserPicturePath = comment.User.ProfileImage;
           this.UserRating = comment.User.Rating;
           this.PostTitle = comment.Post.Title;
           this.UserId = comment.UserId;
           this.PostId = comment.PostId;
           this.CreatedDate = comment.CreatedAt;

        }
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public int UserId { get; set; }
        public string UserDisplayName { get; set; }
        public string UserPicturePath { get; set; }
        public int  UserRating{ get; set; }
        public int PostId { get; set; }
        public string PostTitle { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
