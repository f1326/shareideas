﻿using Forum.Data.Models;
using System.Collections.Generic;
using System.Linq;

namespace Forum.Services.Forum.Services.DTOs
{
    public class UserResponseDto
    {
        public UserResponseDto()
        {
        }
        public UserResponseDto(User user)
        {
            this.Id = user.Id;
            this.Username = user.Username;
            this.DisplayName = user.DisplayName;
            this.Email = user.Email;
            this.Role = user.Role.ToString();
            this.MemberSince = user.MemberSince.ToString();
            this.Rating = user.Rating;
            this.ProfileImageUrl = user.ProfileImage;
            this.Posts = user.Posts?.Select(p => new PostResponseDto(p)).ToList();
            this.Comments = user.Comments?.Select(c => new CommentResponseDto(c)).ToList();
            this.IsActive = user.IsActive;
            this.Password = user.Password;
            this.Blocked = user.Blocked;
        }

        public int Id { get; set; }
        public string Username { get; set; }
        public string DisplayName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        public string MemberSince { get; set; }
        public int Rating { get; set; }
        public string ProfileImageUrl { get; set; }
        public bool IsActive { get; set; }
        public bool Blocked { get; set; }
        public ICollection<PostResponseDto> Posts { get; set; }
        public ICollection<CommentResponseDto> Comments { get; set; }
    }
}
