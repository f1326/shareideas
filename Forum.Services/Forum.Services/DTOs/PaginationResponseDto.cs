﻿using System.Collections.Generic;

namespace Forum.Services.Forum.Services.DTOs
{
    public class PaginationResponseDto<T> where T : class
    {
        public IEnumerable<T> Items { get; set; }

        public int TotalItemsCount { get; set; }
    }
}
