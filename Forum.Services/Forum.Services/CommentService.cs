﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Forum.Data;
using Forum.Data.Models;
using Forum.Services.Exceptions;
using Forum.Services.Forum.Services.Contracts;
using Forum.Services.Forum.Services.DTOs;
using Forum.Services.Models;
using Forum.Services.Models.Contracts;

using Microsoft.EntityFrameworkCore;

namespace Forum.Services.Forum.Services
{
    public class CommentService : ICommentService
    {
        private readonly ForumDbContext dbContext;
        private readonly IRequestMapper mapper;
        private readonly IUserService userService;
        private readonly IPostService postService;

        public CommentService(ForumDbContext dbContext, IRequestMapper mapper, IUserService userService, IPostService postService)
        {
            this.dbContext = dbContext;
            this.mapper = mapper;
            this.userService = userService;
            this.postService = postService;
        }

        private IQueryable<Comment> Query
        {
            get
            {
                return this.dbContext.Comments
                    .Include(c => c.User)
                    .Include(c => c.Post);
            }
        }
        public async Task<CommentResponseDto> CreateAsync(CommentRequestDto requestModel)
        {
            var comment = this.mapper.ToCommentModel(requestModel);
            var user = await this.userService.GetUserMinAsync(requestModel.UserId);
            user.Rating += 5;
            comment.User = user;

            comment.Post = await this.postService.GetPostAsync(requestModel.PostId);
            this.dbContext.Comments.Add(comment);
            await this.dbContext.SaveChangesAsync();
            var commentResponseDto = new CommentResponseDto
            {
                Title = comment.Title,
                Id = comment.Id,
                PostId = comment.PostId,
                UserDisplayName = comment.User.DisplayName,
                UserPicturePath = comment.User.ProfileImage,
                UserRating = comment.User.Rating,
                UserId = comment.UserId,
                Content = comment.Content,
                CreatedDate = comment.CreatedAt,
                PostTitle = comment.Post.Title
            };

            return commentResponseDto;
        }

        public async Task<CommentResponseDto> DeleteAsync(int id)
        {
            var comment = this.dbContext.Comments.Include(c =>c.Post).Include(c => c.User).FirstOrDefault(c => c.Id == id);
             this.dbContext.Comments.Remove(comment);
            await this.dbContext.SaveChangesAsync();

            return new CommentResponseDto(comment);
        }

        public async Task<CommentResponseDto> UpdateAsync(int id, CommentRequestDto requestModel)
        {
            var commentToUpdate = await this.GetCommentAsync(id);
            commentToUpdate.Title = requestModel.Title;
            commentToUpdate.Content = requestModel.Content;

            await this.dbContext.SaveChangesAsync();
            return new CommentResponseDto(commentToUpdate);
        }

        public async Task<CommentResponseDto> GetCommentByIdAsync(int id)
        {
            var comment = await this.GetCommentAsync(id);
            return new CommentResponseDto(comment);
        }
        public async Task<Comment> GetCommentAsync(int id)
        {
            var comment = await Query.
              FirstOrDefaultAsync(comment => comment.Id == id);
            ServiceValidator.ValidateNotNull(comment, $"Comment with Id: {id} doesnt exist!");
            return comment;
        }

        public async Task<IEnumerable<CommentResponseDto>> GetAllAsync()
        {
            var list = await this.Query
                .Select(c => new CommentResponseDto(c))
                .ToListAsync();

            return list;
        }
    }
}
