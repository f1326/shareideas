﻿using System;
using System.Linq;
using System.Threading.Tasks;

using Forum.Data.Models;
using Forum.Services.Models.Contracts;
using Forum.Services.Providers.Contracts;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

namespace Forum.Tests.Services.Tests.UserService.Tests
{
    [TestClass]
    public class Search_Should : BaseTest
    {
        private Mock<IFilterProvider> mockFilterProvider;
        private Mock<ISortProvider> mockSortProvider;
        private Mock<IRequestMapper> mockRequestMapper;

        [TestMethod]
        public async Task ReturnCorrectUsersList_When_SearchValueExists()
        {
            // Arrange
            var user = new User
            {
                Id = 100,
                Username = "TestUsername",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = DateTime.Now,
                IsActive = true,
                Role = Data.Models.Enums.UserRole.Member,
                Blocked = false
            };

            this.PrepareMock();

            using (var assertContext = new Data.ForumDbContext(this.options))
            {
                var sut = new Forum.Services.Forum.Services.UserService(assertContext, mockRequestMapper.Object, mockFilterProvider.Object, mockSortProvider.Object);
                await assertContext.Users.AddAsync(user);
                await assertContext.SaveChangesAsync();
                var searchEmail = "TestEmail";
                var actualUsersQueryable =  sut.Search(searchEmail);

                // Act
                var actualUsersList = actualUsersQueryable.ToList();

                // Assert
                Assert.IsNotNull(actualUsersQueryable);
                Assert.AreEqual(user.Id, actualUsersList[actualUsersList.Count-1].Id);
                Assert.AreEqual(user.Email, actualUsersList[actualUsersList.Count-1].Email);
                Assert.IsInstanceOfType(actualUsersQueryable, typeof(IQueryable<User>));
            }
        }

        [TestMethod]
        public async Task ReturnCorrectUsersList_When_SearchValueExistsInDisplayNameAndUsername()
        {
            // Arrange
            var firstUser = new User
            {
                Id = 100,
                Username = "TestUsername",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = DateTime.Now,
                IsActive = true,
                Role = Data.Models.Enums.UserRole.Member,
                Blocked = false
            };
            var secondUser = new User
            {
                Id = 101,
                Username = "SecondTestUsername",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestUsername",
                Rating = 0,
                MemberSince = DateTime.Now,
                IsActive = true,
                Role = Data.Models.Enums.UserRole.Member,
                Blocked = false
            };

            this.PrepareMock();

            // Act,Arrange
            using (var assertContext = new Data.ForumDbContext(this.options))
            {
                var sut = new Forum.Services.Forum.Services.UserService(assertContext, mockRequestMapper.Object, mockFilterProvider.Object, mockSortProvider.Object);
                await assertContext.Users.AddAsync(firstUser);
                await assertContext.Users.AddAsync(secondUser);
                await assertContext.SaveChangesAsync();
                var searchValue = "TestUsername";
                var actualUsersQueryable = sut.Search(searchValue);
                var actualUsersList = actualUsersQueryable.ToList();
                Assert.IsNotNull(actualUsersQueryable);
                Assert.AreEqual(firstUser.Id, actualUsersList[actualUsersList.Count - 2].Id);
                Assert.AreEqual(firstUser.Username, actualUsersList[actualUsersList.Count - 2].Username);
                Assert.AreEqual(secondUser.DisplayName, actualUsersList[actualUsersList.Count - 1].DisplayName);
                Assert.IsInstanceOfType(actualUsersQueryable, typeof(IQueryable<User>));
            }
        }

        private void PrepareMock()
        {
            mockFilterProvider = new Mock<IFilterProvider>();
            mockSortProvider = new Mock<ISortProvider>();
            mockRequestMapper = new Mock<IRequestMapper>();
        }
    }
}


