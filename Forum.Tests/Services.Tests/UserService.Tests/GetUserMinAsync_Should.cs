﻿using System;
using System.Threading.Tasks;

using Forum.Data.Models;
using Forum.Services.Exceptions;
using Forum.Services.Models.Contracts;
using Forum.Services.Providers.Contracts;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

namespace Forum.Tests.Services.Tests.UserService.Tests
{
    [TestClass]
    public class GetUserMinAsync_Should : BaseTest
    {
        private Mock<IFilterProvider> mockFilterProvider;
        private Mock<ISortProvider> mockSortProvider;
        private Mock<IRequestMapper> mockRequestMapper;

        [TestMethod]
        public async Task ReturnCorrectUser_When_ParameterisCorrect()
        {
            // Arrange
            var user = new User
            {
                Id = 100,
                Username = "TestUsername",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = DateTime.Now,
                IsActive = true,
                Role = Data.Models.Enums.UserRole.Member
            };

            var expectedUser = new User
            {
                Id = 100,
                Username = "TestUsername",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = DateTime.Now,
                IsActive = true,
                Role = Data.Models.Enums.UserRole.Member
            };

            this.PrepareMock();

            using (var assertContext = new Data.ForumDbContext(this.options))
            {
                var sut = new Forum.Services.Forum.Services.UserService(assertContext, mockRequestMapper.Object, mockFilterProvider.Object, mockSortProvider.Object);
                await assertContext.Users.AddAsync(user);
                await assertContext.SaveChangesAsync();
                var userId = 100;

                // Act
                var actualUser = await sut.GetUserMinAsync(userId);

                // Assert
                Assert.IsNotNull(actualUser);
                Assert.AreEqual(expectedUser.Id, actualUser.Id);
                Assert.AreEqual(expectedUser.Username, actualUser.Username);
                Assert.AreEqual(expectedUser.Email, actualUser.Email);
                Assert.AreEqual(expectedUser.DisplayName, actualUser.DisplayName);
                Assert.IsInstanceOfType(actualUser, typeof(User));
            }
        }

        [TestMethod]
        public async Task ThrowException_When_UserIdDontExist()
        {
            // Arrange
            var user = new User
            {
                Id = 100,
                Username = "TestUsername",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = DateTime.Now,
                IsActive = true,
                Role = Data.Models.Enums.UserRole.Member
            };

            this.PrepareMock();
            // Act,Arrange
            using (var assertContext = new Data.ForumDbContext(this.options))
            {
                var sut = new Forum.Services.Forum.Services.UserService(assertContext, mockRequestMapper.Object, mockFilterProvider.Object, mockSortProvider.Object);
                await assertContext.Users.AddAsync(user);
                await assertContext.SaveChangesAsync();
                var userId = 200;
                await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async() =>await sut.GetUserMinAsync(userId));
               
            }

        }

        private void PrepareMock()
        {
            mockFilterProvider = new Mock<IFilterProvider>();
            mockSortProvider = new Mock<ISortProvider>();
            mockRequestMapper = new Mock<IRequestMapper>();
        }
    }
}
