﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Forum.Data.Models;
using Forum.Services.Forum.Services.DTOs;
using Forum.Services.Models.Contracts;
using Forum.Services.Providers.Contracts;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

namespace Forum.Tests.Services.Tests.UserService.Tests
{
    [TestClass]
    public class GetAllAsync_Should :BaseTest
    {
        private Mock<IFilterProvider> mockFilterProvider;
        private Mock<ISortProvider> mockSortProvider;
        private Mock<IRequestMapper> mockRequestMapper;

        [TestMethod]
        public async Task ReturnCollectionOfUserResponseDto()
        {
            var firstUser = new User
            {
                Id = 100,
                Username = "FirstTestUsername",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = DateTime.Now,
                IsActive = true,
                Role = Data.Models.Enums.UserRole.Member
            };
            var secondUser = new User
            {
                Id = 200,
                Username = "SecondTestUsername",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = DateTime.Now,
                IsActive = true,
                Role = Data.Models.Enums.UserRole.Member
            };

            var list = new List<UserResponseDto>
            {
                new UserResponseDto
            {
                Username = "FirstTestUsername",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = null,
                IsActive = true,
                Role = null
            },
                     new UserResponseDto
            {
                Username = "SecondTestUsername",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = null,
                IsActive = true,
                Role = null
            }
        };
            this.PrepareMock();
           
            using (var assertContext = new Data.ForumDbContext(this.options))
            {
                var sut = new Forum.Services.Forum.Services.UserService(assertContext, mockRequestMapper.Object, mockFilterProvider.Object, mockSortProvider.Object);
                await assertContext.Users.AddAsync(secondUser);
                await assertContext.Users.AddAsync(firstUser);
                await assertContext.SaveChangesAsync();

                // Act
                var actualUsersList = await sut.GetAllAsync();

                // Assert
                Assert.IsNotNull(actualUsersList);
                Assert.IsInstanceOfType(actualUsersList,typeof(List<UserResponseDto>));
            }
        }
        private void PrepareMock()
        {
            mockFilterProvider = new Mock<IFilterProvider>();
            mockSortProvider = new Mock<ISortProvider>();
            mockRequestMapper = new Mock<IRequestMapper>();
        }
    }
}
