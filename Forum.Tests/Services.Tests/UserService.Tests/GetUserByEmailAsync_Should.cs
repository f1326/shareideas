﻿using System;
using System.Threading.Tasks;

using Forum.Data.Models;
using Forum.Services.Forum.Services.DTOs;
using Forum.Services.Models.Contracts;
using Forum.Services.Providers.Contracts;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

namespace Forum.Tests.Services.Tests.UserService.Tests
{
    [TestClass]
    public class GetUserByEmailAsync_Should : BaseTest
    {
        private Mock<IFilterProvider> mockFilterProvider;
        private Mock<ISortProvider> mockSortProvider;
        private Mock<IRequestMapper> mockRequestMapper;

        [TestMethod]
        public async Task ReturnUserResponseDto_When_ParameterIsCorrect()
        {
            // Arrange
            var firstUser = new User
            {
                Id = 100,
                Username = "TestUsrname",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = DateTime.Now,
                IsActive = true,
                Role = Data.Models.Enums.UserRole.Member
            };
            var expectedUserResponseDto = new UserResponseDto
            {
                Id = 100,
                Username = "TestUsrname",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = null,
                IsActive = true,
                Role = null
            };
            this.PrepareMock();

            using (var assertContext = new Data.ForumDbContext(this.options))
            {
                var sut = new Forum.Services.Forum.Services.UserService(assertContext, mockRequestMapper.Object, mockFilterProvider.Object, mockSortProvider.Object);
                await assertContext.Users.AddAsync(firstUser);
                await assertContext.SaveChangesAsync();
                var email = "TestEmail";

                // Act
                var actualUserResponseDto = await sut.GetUserByEmailAsync(email);

                // Assert
                Assert.IsNotNull(actualUserResponseDto);
                Assert.IsInstanceOfType(actualUserResponseDto, typeof(UserResponseDto));
                Assert.AreEqual(expectedUserResponseDto.Id, actualUserResponseDto.Id);
                Assert.AreEqual(expectedUserResponseDto.Email, actualUserResponseDto.Email);
                Assert.AreEqual(expectedUserResponseDto.Username, actualUserResponseDto.Username);
            }
        }
        private void PrepareMock()
        {
            mockFilterProvider = new Mock<IFilterProvider>();
            mockSortProvider = new Mock<ISortProvider>();
            mockRequestMapper = new Mock<IRequestMapper>();
        }
    }
}