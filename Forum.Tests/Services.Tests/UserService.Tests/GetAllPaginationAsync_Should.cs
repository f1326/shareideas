﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

using Forum.Data.Models;
using Forum.Services.Forum.Services.DTOs;
using Forum.Services.Models.Contracts;
using Forum.Services.Providers.Contracts;

using Microsoft.EntityFrameworkCore.Query;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

namespace Forum.Tests.Services.Tests.UserService.Tests
{
    [TestClass]
    public class GetAllPaginationAsync_Should : BaseTest
    {
        private Mock<IFilterProvider> mockFilterProvider;
        private Mock<ISortProvider> mockSortProvider;
        private Mock<IRequestMapper> mockRequestMapper;

        [TestMethod]
        public async Task ReturnCollectionOfUserResponseDto()
        {
            // Arrange
            var firstUser = new User
            {
                Id = 100,
                Username = "FirstTestUsername",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = DateTime.Now,
                IsActive = true,
                Role = Data.Models.Enums.UserRole.Member
            };
            var secondUser = new User
            {
                Id = 200,
                Username = "SecondTestUsername",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = DateTime.Now,
                IsActive = true,
                Role = Data.Models.Enums.UserRole.Member
            };

            var usersList = new List<User>();
            usersList.Add(firstUser);
            usersList.Add(secondUser);

            var userResponseDtoList = new List<UserResponseDto>
            {
                new UserResponseDto
            {
                Username = "FirstTestUsername",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = null,
                IsActive = true,
                Role = null
            },
                     new UserResponseDto
            {
                Username = "SecondTestUsername",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = null,
                IsActive = true,
                Role = null
            }
        };
            var usersQuery = new TestAsyncEnumerable<User>(usersList.AsQueryable());
            this.PrepareMock();
            this.mockFilterProvider
            .Setup(p => p.FilterAsync<User>(It.IsAny<IQueryable<User>>(), It.IsAny<string>()))
            .ReturnsAsync(usersQuery);

            using (var assertContext = new Data.ForumDbContext(this.options))
            {
                var sut = new Forum.Services.Forum.Services.UserService(assertContext, mockRequestMapper.Object, mockFilterProvider.Object, mockSortProvider.Object);
                await assertContext.Users.AddAsync(secondUser);
                await assertContext.Users.AddAsync(firstUser);
                await assertContext.SaveChangesAsync();
                var skip = 0;
                var take = 2;

                // Act
                var actualUsersList = await sut.GetAllPaginationAsync(skip, take, null, null, null, null, null);

                // Assert
                Assert.IsNotNull(actualUsersList);
                Assert.IsInstanceOfType(actualUsersList, typeof(PaginationResponseDto<UserResponseDto>));
            }
        }

        [TestMethod]
        public async Task ReturnCollectionOfUserResponseDtoWithSortinng()
        {
            // Arrange
            var firstUser = new User
            {
                Id = 100,
                Username = "FirstTestUsername",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = DateTime.Now,
                IsActive = true,
                Role = Data.Models.Enums.UserRole.Member
            };
            var secondUser = new User
            {
                Id = 200,
                Username = "SecondTestUsername",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = DateTime.Now.AddDays(1),
                IsActive = true,
                Role = Data.Models.Enums.UserRole.Member
            };

            var usersList = new List<User>();
            usersList.Add(firstUser);
            usersList.Add(secondUser);
            var usersQuery = new TestAsyncEnumerable<User>(usersList.AsQueryable());

            var userResponseDtoList = new List<UserResponseDto>
            {
                new UserResponseDto
            {
                Username = "FirstTestUsername",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = (DateTime.Now).ToString(),
                IsActive = true,
                Role = null
            },
                     new UserResponseDto
            {
                Username = "SecondTestUsername",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = (DateTime.Now.AddDays(1)).ToString(),
                IsActive = true,
                Role = null
            }
        };

            this.PrepareMock();
            this.mockFilterProvider
            .Setup(p => p.FilterAsync<User>(It.IsAny<IQueryable<User>>(), It.IsAny<string>()))
            .ReturnsAsync(usersQuery);
            this.mockSortProvider
            .Setup(p => p.Sort<User>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IQueryable<User>>()))
            .Returns(usersQuery);

            using (var assertContext = new Data.ForumDbContext(this.options))
            {
                var sut = new Forum.Services.Forum.Services.UserService(assertContext, mockRequestMapper.Object, mockFilterProvider.Object, mockSortProvider.Object);
                await assertContext.Users.AddAsync(secondUser);
                await assertContext.Users.AddAsync(firstUser);
                await assertContext.SaveChangesAsync();
                var skip = 0;
                var take = 2;
                string sortType = "";
                string sortDirection = "";
                // Act
                var actualUsersList = await sut.GetAllPaginationAsync(skip, take, null, sortType, sortDirection, null, null);

                // Assert
                Assert.IsNotNull(actualUsersList);
                Assert.IsInstanceOfType(actualUsersList, typeof(PaginationResponseDto<UserResponseDto>));
            }
        }

        [TestMethod]
        public async Task ReturnCollectionOfUserResponseDtoAfterSerchValue()
        {
            // Arrange
            var firstUser = new User
            {
                Id = 100,
                Username = "FirstTestUsername",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = DateTime.Now,
                IsActive = true,
                Role = Data.Models.Enums.UserRole.Member
            };
            var secondUser = new User
            {
                Id = 200,
                Username = "SecondTestUsername",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = DateTime.Now.AddDays(1),
                IsActive = true,
                Role = Data.Models.Enums.UserRole.Member
            };

            var usersList = new List<User>();
            usersList.Add(firstUser);
            usersList.Add(secondUser);
            var usersQuery = new TestAsyncEnumerable<User>(usersList.AsQueryable());

            var userResponseDtoList = new List<UserResponseDto>
            {
                new UserResponseDto
            {
                Username = "FirstTestUsername",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = (DateTime.Now).ToString(),
                IsActive = true,
                Role = null
            },
                     new UserResponseDto
            {
                Username = "SecondTestUsername",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = (DateTime.Now.AddDays(1)).ToString(),
                IsActive = true,
                Role = null
            }
        };
            this.PrepareMock();
            this.mockFilterProvider
            .Setup(p => p.FilterAsync<User>(It.IsAny<IQueryable<User>>(), It.IsAny<string>()))
            .ReturnsAsync(usersQuery);

            using (var assertContext = new Data.ForumDbContext(this.options))
            {
                var sut = new Forum.Services.Forum.Services.UserService(assertContext, mockRequestMapper.Object, mockFilterProvider.Object, mockSortProvider.Object);
                await assertContext.Users.AddAsync(secondUser);
                await assertContext.Users.AddAsync(firstUser);
                await assertContext.SaveChangesAsync();
                var skip = 0;
                var take = 2;
                string searchValue = "FirstTestUsername";
                // Act
                var actualUsersList = await sut.GetAllPaginationAsync(skip, take, null, null, null, null, searchValue);

                // Assert
                Assert.IsNotNull(actualUsersList);
                Assert.IsInstanceOfType(actualUsersList, typeof(PaginationResponseDto<UserResponseDto>));
            }
        }

        [TestMethod]
        public async Task ReturnCollectionOfUserResponseDtoWithFilterRating()
        {
            // Arrange
            var firstUser = new User
            {
                Id = 100,
                Username = "FirstTestUsername",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = DateTime.Now,
                IsActive = true,
                Role = Data.Models.Enums.UserRole.Member
            };
            var secondUser = new User
            {
                Id = 200,
                Username = "SecondTestUsername",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = DateTime.Now.AddDays(1),
                IsActive = true,
                Role = Data.Models.Enums.UserRole.Member
            };

            var usersList = new List<User>();
            usersList.Add(firstUser);
            usersList.Add(secondUser);
            var usersQuery = new TestAsyncEnumerable<User>(usersList.AsQueryable());

            var userResponseDtoList = new List<UserResponseDto>
            {
                new UserResponseDto
            {
                Username = "FirstTestUsername",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = (DateTime.Now).ToString(),
                IsActive = true,
                Role = null
            },
                     new UserResponseDto
            {
                Username = "SecondTestUsername",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = (DateTime.Now.AddDays(1)).ToString(),
                IsActive = true,
                Role = null
            }
        };
            this.PrepareMock();
            this.mockFilterProvider
            .Setup(p => p.FilterAsync<User>(It.IsAny<IQueryable<User>>(), It.IsAny<string>()))
            .ReturnsAsync(usersQuery);

            using (var assertContext = new Data.ForumDbContext(this.options))
            {
                var sut = new Forum.Services.Forum.Services.UserService(assertContext, mockRequestMapper.Object, mockFilterProvider.Object, mockSortProvider.Object);
                await assertContext.Users.AddAsync(secondUser);
                await assertContext.Users.AddAsync(firstUser);
                await assertContext.SaveChangesAsync();
                var skip = 0;
                var take = 2;
                var rating = 0;
                // Act
                var actualUsersList = await sut.GetAllPaginationAsync(skip, take, rating, null, null, null, null);

                // Assert
                Assert.IsNotNull(actualUsersList);
                Assert.IsInstanceOfType(actualUsersList, typeof(PaginationResponseDto<UserResponseDto>));
            }
        }

        [TestMethod]
        public async Task ReturnCollectionOfUserResponseDtoWithFilterBlockStatusBlocked()
        {
            // Arrange
            var firstUser = new User
            {
                Id = 100,
                Username = "FirstTestUsername",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = DateTime.Now,
                IsActive = true,
                Blocked = true
            };
            var secondUser = new User
            {
                Id = 200,
                Username = "SecondTestUsername",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = DateTime.Now.AddDays(1),
                IsActive = true,
                Blocked = true
            };

            var usersList = new List<User>();
            usersList.Add(firstUser);
            usersList.Add(secondUser);
            var usersQuery = new TestAsyncEnumerable<User>(usersList.AsQueryable());

            var userResponseDtoList = new List<UserResponseDto>
            {
                new UserResponseDto
            {
                Username = "FirstTestUsername",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = (DateTime.Now).ToString(),
                IsActive = true,
                Role = null
            },
                     new UserResponseDto
            {
                Username = "SecondTestUsername",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = (DateTime.Now.AddDays(1)).ToString(),
                IsActive = true,
                Role = null
            }
        };
            this.PrepareMock();
            this.mockFilterProvider
            .Setup(p => p.FilterAsync<User>(It.IsAny<IQueryable<User>>(), It.IsAny<string>()))
            .ReturnsAsync(usersQuery);

            using (var assertContext = new Data.ForumDbContext(this.options))
            {
                var sut = new Forum.Services.Forum.Services.UserService(assertContext, mockRequestMapper.Object, mockFilterProvider.Object, mockSortProvider.Object);
                await assertContext.Users.AddAsync(secondUser);
                await assertContext.Users.AddAsync(firstUser);
                await assertContext.SaveChangesAsync();
                var skip = 0;
                var take = 2;
                string blocked = "Blocked";
                // Act
                var actualUsersList = await sut.GetAllPaginationAsync(skip, take, null, null, null, blocked, null);

                // Assert
                Assert.IsNotNull(actualUsersList);
                Assert.IsInstanceOfType(actualUsersList, typeof(PaginationResponseDto<UserResponseDto>));
            }
        }
        [TestMethod]
        public async Task ReturnCollectionOfUserResponseDtoWithFilterBlockStatusUnblocked()
        {
            // Arrange
            var firstUser = new User
            {
                Id = 100,
                Username = "FirstTestUsername",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = DateTime.Now,
                IsActive = true,
                Blocked = false
            };
            var secondUser = new User
            {
                Id = 200,
                Username = "SecondTestUsername",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = DateTime.Now.AddDays(1),
                IsActive = true,
                Blocked = false
            };

            var usersList = new List<User>();
            usersList.Add(firstUser);
            usersList.Add(secondUser);
            var usersQuery = new TestAsyncEnumerable<User>(usersList.AsQueryable());

            var userResponseDtoList = new List<UserResponseDto>
            {
                new UserResponseDto
            {
                Username = "FirstTestUsername",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = (DateTime.Now).ToString(),
                IsActive = true,
                Role = null
            },
                     new UserResponseDto
            {
                Username = "SecondTestUsername",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = (DateTime.Now.AddDays(1)).ToString(),
                IsActive = true,
                Role = null
            }};
            this.PrepareMock();
            this.mockFilterProvider
            .Setup(p => p.FilterAsync<User>(It.IsAny<IQueryable<User>>(), It.IsAny<string>()))
            .ReturnsAsync(usersQuery);

            using (var assertContext = new Data.ForumDbContext(this.options))
            {
                var sut = new Forum.Services.Forum.Services.UserService(assertContext, mockRequestMapper.Object, mockFilterProvider.Object, mockSortProvider.Object);
                await assertContext.Users.AddAsync(secondUser);
                await assertContext.Users.AddAsync(firstUser);
                await assertContext.SaveChangesAsync();
                var skip = 0;
                var take = 2;
                string unblocked = "Unblocked";
                // Act
                var actualUsersList = await sut.GetAllPaginationAsync(skip, take, null, null, null, unblocked, null);

                // Assert
                Assert.IsNotNull(actualUsersList);
                Assert.IsInstanceOfType(actualUsersList, typeof(PaginationResponseDto<UserResponseDto>));
            }
        }
        private void PrepareMock()
        {
            mockFilterProvider = new Mock<IFilterProvider>();
            mockSortProvider = new Mock<ISortProvider>();
            mockRequestMapper = new Mock<IRequestMapper>();
        }
    }

    //https://docs.microsoft.com/en-gb/ef/ef6/fundamentals/testing/mocking?redirectedfrom=MSDN
    /*
     Whilst the async methods (ToListAsync, FirstAsync etc.) are only supported 
     when running against an EF query,an in-memory test double of a DbSet.
     In order to use the async methods weneed to create an in-memory AsyncQueryProvider
     to process the async query. 
     */
    internal class TestAsyncQueryProvider<TEntity> : IAsyncQueryProvider
    {
        private readonly IQueryProvider _inner;

        internal TestAsyncQueryProvider(IQueryProvider inner)
        {
            _inner = inner;
        }

        public IQueryable CreateQuery(Expression expression)
        {
            return new TestAsyncEnumerable<TEntity>(expression);
        }

        public IQueryable<TElement> CreateQuery<TElement>(Expression expression)
        {
            return new TestAsyncEnumerable<TElement>(expression);
        }

        public object Execute(Expression expression)
        {
            return _inner.Execute(expression);
        }

        public TResult Execute<TResult>(Expression expression)
        {
            return _inner.Execute<TResult>(expression);
        }

        public IAsyncEnumerable<TResult> ExecuteAsync<TResult>(Expression expression)
        {
            return new TestAsyncEnumerable<TResult>(expression);
        }

        public TResult ExecuteAsync<TResult>(Expression expression, CancellationToken cancellationToken)
        {
            var expectedResultType = typeof(TResult).GetGenericArguments()[0];
            var executionResult = typeof(IQueryProvider)
              .GetMethods()
              .First(method => method.Name == nameof(IQueryProvider.Execute) && method.IsGenericMethod)
              .MakeGenericMethod(expectedResultType)
              .Invoke(this, new object[] { expression });

            return (TResult)typeof(Task).GetMethod(nameof(Task.FromResult))
              .MakeGenericMethod(expectedResultType)
              .Invoke(null, new[] { executionResult });
        }
    }

    internal class TestAsyncEnumerable<T> : EnumerableQuery<T>, IAsyncEnumerable<T>, IQueryable<T>
    {
        public TestAsyncEnumerable(IEnumerable<T> enumerable)
            : base(enumerable)
        { }

        public TestAsyncEnumerable(Expression expression)
            : base(expression)
        { }

        public IAsyncEnumerator<T> GetEnumerator()
        {
            return new TestAsyncEnumerator<T>(this.AsEnumerable().GetEnumerator());
        }

        public IAsyncEnumerator<T> GetAsyncEnumerator(CancellationToken cancellationToken = default)
        {
            return new TestAsyncEnumerator<T>(this.AsEnumerable().GetEnumerator());
        }

        IQueryProvider IQueryable.Provider
        {
            get { return new TestAsyncQueryProvider<T>(this); }
        }
    }

    internal class TestAsyncEnumerator<T> : IAsyncEnumerator<T>
    {
        private readonly IEnumerator<T> _inner;

        public TestAsyncEnumerator(IEnumerator<T> inner)
        {
            _inner = inner;
        }

        public void Dispose()
        {
            _inner.Dispose();
        }

        public T Current
        {
            get
            {
                return _inner.Current;
            }
        }

        public Task<bool> MoveNext(CancellationToken cancellationToken)
        {
            return Task.FromResult(_inner.MoveNext());
        }

        public ValueTask<bool> MoveNextAsync()
        {
            return new ValueTask<bool>(_inner.MoveNext());
        }

        public ValueTask DisposeAsync()
        {
            _inner.Dispose();
            return new ValueTask();
        }
    }
}