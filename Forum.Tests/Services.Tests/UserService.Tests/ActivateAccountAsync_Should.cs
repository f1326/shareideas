﻿using System;
using System.Threading.Tasks;

using Forum.Data.Models;
using Forum.Services.Models.Contracts;
using Forum.Services.Providers.Contracts;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

namespace Forum.Tests.Services.Tests.UserService.Tests
{
    [TestClass]
    public class ActivateAccountAsync_Should : BaseTest
    {
        private Mock<IFilterProvider> mockFilterProvider;
        private Mock<ISortProvider> mockSortProvider;
        private Mock<IRequestMapper> mockRequestMapper;

        [TestMethod]
        public async Task ReturnSuccessfullMessage_When_AccountIsActivated()
        {
            // Arrange
            var user = new User
            {
                Id = 100,
                Username = "TestUsername",
                Password = "testPassword",
                Email = "TestEmail@email.com",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = DateTime.Now,
                IsActive = false,
                Role = Data.Models.Enums.UserRole.Member,
                ActivationCode = "xxxxxxxxxx"
            };
            this.PrepareMock();
           
            using (var assertContext = new Data.ForumDbContext(this.options))
            {
                var sut = new Forum.Services.Forum.Services.UserService(assertContext, mockRequestMapper.Object, mockFilterProvider.Object, mockSortProvider.Object);
                await assertContext.Users.AddAsync(user);
                await assertContext.SaveChangesAsync();
                var userId = 100;
                var activationCode = "xxxxxxxxxx";

                // Act
                var message = await sut.ActivateAccountAsync(userId,activationCode);

                //Assert
                Assert.IsNotNull(message);
                Assert.IsTrue(user.IsActive);
            }

        }

        [TestMethod]
        public async Task ThrowException_When_ActivationCodeIsWrong()
        {
            // Arrange
            var user = new User
            {
                Id = 100,
                Username = "TestUsername",
                Password = "testPassword",
                Email = "TestEmail@email.com",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = DateTime.Now,
                IsActive = false,
                Role = Data.Models.Enums.UserRole.Member,
                ActivationCode = "xxxxxxxxxx"
            };
            this.PrepareMock();

            using (var assertContext = new Data.ForumDbContext(this.options))
            {
                var sut = new Forum.Services.Forum.Services.UserService(assertContext, mockRequestMapper.Object, mockFilterProvider.Object, mockSortProvider.Object);
                await assertContext.Users.AddAsync(user);
                await assertContext.SaveChangesAsync();
                // Act
                var userId = 100;
                var activationCode = "yyyyyyyyyyy";
               
                //Assert
                await Assert.ThrowsExceptionAsync<UnauthorizedAccessException>(async () => await sut.ActivateAccountAsync(userId, activationCode));
            }

        }
        private void PrepareMock()
        {
            mockFilterProvider = new Mock<IFilterProvider>();
            mockSortProvider = new Mock<ISortProvider>();
            mockRequestMapper = new Mock<IRequestMapper>();
        }
    }
}