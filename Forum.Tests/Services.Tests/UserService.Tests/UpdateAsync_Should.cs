﻿using System;
using System.Threading.Tasks;

using Forum.Data.Models;
using Forum.Services.Forum.Services.DTOs;
using Forum.Services.Models;
using Forum.Services.Models.Contracts;
using Forum.Services.Providers.Contracts;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

namespace Forum.Tests.Services.Tests.UserService.Tests
{
    [TestClass]
    public class UpdateAsync_Should : BaseTest
    {
        private Mock<IFilterProvider> mockFilterProvider;
        private Mock<ISortProvider> mockSortProvider;
        private Mock<IRequestMapper> mockRequestMapper;

        [TestMethod]
        public async Task ReturnCorrectUserResponseDto_When_ParameterisCorrect()
        {
            // Arrange
            var user = new User
            {
                Id = 100,
                Username = "TestUsrname",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = DateTime.Now,
                IsActive = true,
                Role = Data.Models.Enums.UserRole.Member
            };
            var mockUser = new User
            {
                Id = 100,
                Username = "TestUsrname",
                Password = "updatedPassword",
                Email = "TestEmail",
                DisplayName = "updatedDisplayName",
                Rating = 0,
                MemberSince = DateTime.Now,
                IsActive = true,
                Role = Data.Models.Enums.UserRole.Member
            };

            var userRequestDto = new UserRequestDto
            {
                Username = "TestUsrname",
                Password = "updatedPassword",
                Email = "TestEmail",
                DisplayName = "updatedDisplayName"
            };

            var expectedUserResponseDto = new UserResponseDto
            {
                Id = 100,
                Username = "TestUsrname",
                Password = "updatedPassword",
                Email = "TestEmail",
                DisplayName = "updatedDisplayName",
                Rating = 0,
                MemberSince = null,
                IsActive = true,
                Role = null
            };

            this.PrepareMock();
            mockRequestMapper.Setup(m => m.ToUserModel(It.IsAny<UserRequestDto>()))
                .Returns(mockUser);
      
            using (var assertContext = new Data.ForumDbContext(this.options))
            {
                var sut = new Forum.Services.Forum.Services.UserService(assertContext, mockRequestMapper.Object, mockFilterProvider.Object, mockSortProvider.Object);
                await assertContext.Users.AddAsync(user);
                await assertContext.SaveChangesAsync();
                var userId = 100;

                // Act
                var actualUser = await sut.UpdateAsync(userId, userRequestDto);

                // Assert
                Assert.IsNotNull(actualUser);
                Assert.AreEqual(expectedUserResponseDto.Id, actualUser.Id);
                Assert.AreEqual(expectedUserResponseDto.Username, actualUser.Username);
                Assert.AreEqual(expectedUserResponseDto.Email, actualUser.Email);
                Assert.AreEqual(expectedUserResponseDto.DisplayName, actualUser.DisplayName);
                Assert.IsInstanceOfType(actualUser, typeof(UserResponseDto));
            }
        }
        private void PrepareMock()
        {
            mockFilterProvider = new Mock<IFilterProvider>();
            mockSortProvider = new Mock<ISortProvider>();
            mockRequestMapper = new Mock<IRequestMapper>();
        }
    }
}
