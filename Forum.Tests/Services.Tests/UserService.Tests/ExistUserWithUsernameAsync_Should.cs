﻿using System;
using System.Threading.Tasks;

using Forum.Data.Models;
using Forum.Services.Models.Contracts;
using Forum.Services.Providers.Contracts;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

namespace Forum.Tests.Services.Tests.UserService.Tests
{
    [TestClass]
    public class ExistUserWithUsernameAsync_Should: BaseTest
    {

        private Mock<IFilterProvider> mockFilterProvider;
        private Mock<ISortProvider> mockSortProvider;
        private Mock<IRequestMapper> mockRequestMapper;

        [TestMethod]
        public async Task ReturnTrue_When_UsernameExists()
        {
            var firstUser = new User
            {
                Id = 100,
                Username = "TestUsername",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = DateTime.Now,
                IsActive = true,
                Role = Data.Models.Enums.UserRole.Member
            };
            this.PrepareMock();

            using (var assertContext = new Data.ForumDbContext(this.options))
            {
                var sut = new Forum.Services.Forum.Services.UserService(assertContext, mockRequestMapper.Object, mockFilterProvider.Object, mockSortProvider.Object);
                await assertContext.Users.AddAsync(firstUser);
                await assertContext.SaveChangesAsync();
                var username = "TestUsername";

                // Act
                var actualResult = await sut.ExistUserWithUsernameAsync(username);

                // Assert
                Assert.IsNotNull(actualResult);
                Assert.IsInstanceOfType(actualResult, typeof(Boolean));
                Assert.IsTrue(actualResult);
            }
        }

        [TestMethod]
        public async Task ReturnFalse_When_UsernameNotExist()
        {
            var firstUser = new User
            {
                Id = 100,
                Username = "TestUsername",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = DateTime.Now,
                IsActive = true,
                Role = Data.Models.Enums.UserRole.Member
            };
            this.PrepareMock();

            // Act,Arrange
            using (var assertContext = new Data.ForumDbContext(this.options))
            {
                var sut = new Forum.Services.Forum.Services.UserService(assertContext, mockRequestMapper.Object, mockFilterProvider.Object, mockSortProvider.Object);
                await assertContext.Users.AddAsync(firstUser);
                await assertContext.SaveChangesAsync();
                var username = "WrongUsername";
                var actualResult = await sut.ExistUserWithUsernameAsync(username);

                Assert.IsNotNull(actualResult);
                Assert.IsInstanceOfType(actualResult, typeof(Boolean));
                Assert.IsFalse(actualResult);
            }
        }

        private void PrepareMock()
        {
            mockFilterProvider = new Mock<IFilterProvider>();
            mockSortProvider = new Mock<ISortProvider>();
            mockRequestMapper = new Mock<IRequestMapper>();
        }
    }
}