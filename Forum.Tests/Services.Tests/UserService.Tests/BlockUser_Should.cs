﻿using System;
using System.Threading.Tasks;

using Forum.Data.Models;
using Forum.Services.Forum.Services.DTOs;
using Forum.Services.Models.Contracts;
using Forum.Services.Providers.Contracts;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

namespace Forum.Tests.Services.Tests.UserService.Tests
{
    [TestClass]
    public class BlockUser_Should : BaseTest
    {
        private Mock<IFilterProvider> mockFilterProvider;
        private Mock<ISortProvider> mockSortProvider;
        private Mock<IRequestMapper> mockRequestMapper;

        [TestMethod]
        public async Task ReturnCorrectUserResponseDto_When_ParameterIsCorrect()
        {
            // Arrange
            var user = new User
            {
                Id = 100,
                Username = "TestUsername",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = DateTime.Now,
                IsActive = true,
                Role = Data.Models.Enums.UserRole.Member,
                Blocked = false
            };

            var expectedUserResponseDto = new UserResponseDto
            {
                Id = 100,
                Username = "TestUsername",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "UpdatedDisplayName",
                Rating = 0,
                MemberSince = null,
                IsActive = true,
                Blocked = true
            };

            this.PrepareMock();

            using (var assertContext = new Data.ForumDbContext(this.options))
            {
                var sut = new Forum.Services.Forum.Services.UserService(assertContext, mockRequestMapper.Object, mockFilterProvider.Object, mockSortProvider.Object);
                await assertContext.Users.AddAsync(user);
                await assertContext.SaveChangesAsync();
                var userId = 100;

                // Act
                var actualUser = await sut.BlockUser(userId);

                // Assert
                Assert.IsNotNull(actualUser);
                Assert.AreEqual(expectedUserResponseDto.Id, actualUser.Id);
                Assert.AreEqual(expectedUserResponseDto.Blocked, actualUser.Blocked);
                Assert.IsInstanceOfType(actualUser, typeof(UserResponseDto));
            }
        }

        [TestMethod]
        public async Task ThrowException_When_UserIsAlreadyBlocked()
        {
            // Arrange
            var user = new User
            {
                Id = 100,
                Username = "TestUsername",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = DateTime.Now,
                IsActive = true,
                Role = Data.Models.Enums.UserRole.Member,
                Blocked = true
            };
            this.PrepareMock();

            // Act,Arrange
            using (var assertContext = new Data.ForumDbContext(this.options))
            {
                var sut = new Forum.Services.Forum.Services.UserService(assertContext, mockRequestMapper.Object, mockFilterProvider.Object, mockSortProvider.Object);
                await assertContext.Users.AddAsync(user);
                await assertContext.SaveChangesAsync();
                var userId = 100;
                
                await Assert.ThrowsExceptionAsync<InvalidOperationException>(async () => await sut.BlockUser(userId));
            }
        }

        private void PrepareMock()
        {
            mockFilterProvider = new Mock<IFilterProvider>();
            mockSortProvider = new Mock<ISortProvider>();
            mockRequestMapper = new Mock<IRequestMapper>();
        }
    }
}


