﻿using System;
using System.Threading.Tasks;

using Forum.Data.Models;
using Forum.Services.Forum.Services.DTOs;
using Forum.Services.Models.Contracts;
using Forum.Services.Providers.Contracts;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

namespace Forum.Tests.Services.Tests.UserService.Tests
{
    [TestClass]
    public class UpdatePasswordAsync_Should : BaseTest
    {

        private Mock<IFilterProvider> mockFilterProvider;
        private Mock<ISortProvider> mockSortProvider;
        private Mock<IRequestMapper> mockRequestMapper;

        [TestMethod]
        public async Task ReturnCorrectUserResponseDto_When_ParameterIsCorrect()
        {
            // Arrange
            var user = new User
            {
                Id = 100,
                Username = "TestUsrname",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = DateTime.Now,
                IsActive = true,
                Role = Data.Models.Enums.UserRole.Member
            };

            var expectedUserResponseDto = new UserResponseDto
            {
                Id = 100,
                Username = "TestUsrname",
                Password = "updatedPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = null,
                IsActive = true,
                Role = "Member"
            };

            this.PrepareMock();
            
            using (var assertContext = new Data.ForumDbContext(this.options))
            {
                var sut = new Forum.Services.Forum.Services.UserService(assertContext, mockRequestMapper.Object, mockFilterProvider.Object, mockSortProvider.Object);
                await assertContext.Users.AddAsync(user);
                await assertContext.SaveChangesAsync();
                var userId = 100;
                var newPassword = "updatedPassword";

                // Act
                var actualUser = await sut.UpdatePasswordAsync(userId, newPassword);

                // Assert
                Assert.IsNotNull(actualUser);
                Assert.AreEqual(expectedUserResponseDto.Id, actualUser.Id);
                Assert.AreEqual(expectedUserResponseDto.DisplayName, actualUser.DisplayName);
                Assert.IsInstanceOfType(actualUser, typeof(UserResponseDto));
            }
        }
        private void PrepareMock()
        {
            mockFilterProvider = new Mock<IFilterProvider>();
            mockSortProvider = new Mock<ISortProvider>();
            mockRequestMapper = new Mock<IRequestMapper>();
        }
    }
}


