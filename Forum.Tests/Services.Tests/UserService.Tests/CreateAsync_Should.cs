﻿using System;
using System.Threading.Tasks;

using Forum.Data.Models;
using Forum.Services.Forum.Services.DTOs;
using Forum.Services.Models;
using Forum.Services.Models.Contracts;
using Forum.Services.Providers.Contracts;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

namespace Forum.Tests.Services.Tests.UserService.Tests
{
    [TestClass]
    public class CreateAsync_Should : BaseTest
    {
        private Mock<IFilterProvider> mockFilterProvider;
        private Mock<ISortProvider> mockSortProvider;
        private Mock<IRequestMapper> mockRequestMapper;

        [TestMethod]
        public async Task ReturnUserResponseDto()
        {
            // Arrange
            var user = new User
            {
                Username = "TestUsrname",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = DateTime.Now,
                IsActive = true,
                Role = Data.Models.Enums.UserRole.Member
            };

            var expectedUserResponseDto = new UserResponseDto
            {
                Username = "TestUsrname",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = null,
                IsActive = true,
                Role = null
            };
            var userRequestDto = new UserRequestDto
            {
                Username = "TestUsrname",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName"
            };
           this.PrepareMock();
           mockRequestMapper.Setup(m => m.ToUserModel(It.IsAny<UserRequestDto>()))
               .Returns(user);

            
            using (var assertContext = new Data.ForumDbContext(this.options))
            {
                var sut = new Forum.Services.Forum.Services.UserService(assertContext, mockRequestMapper.Object, mockFilterProvider.Object, mockSortProvider.Object);

                // Act
                var actualUserResponseDto = await sut.CreateAsync(userRequestDto);

                // Assert
                Assert.IsNotNull(actualUserResponseDto);
                Assert.AreEqual(expectedUserResponseDto.Username, actualUserResponseDto.Username);
                Assert.AreEqual(expectedUserResponseDto.Email, actualUserResponseDto.Email);
                Assert.AreEqual(expectedUserResponseDto.DisplayName, actualUserResponseDto.DisplayName);
                Assert.IsInstanceOfType(actualUserResponseDto, typeof(UserResponseDto));
            }
        }

        private void PrepareMock()
        {
            mockFilterProvider = new Mock<IFilterProvider>();
            mockSortProvider = new Mock<ISortProvider>();
            mockRequestMapper = new Mock<IRequestMapper>();
        }
    }
}
