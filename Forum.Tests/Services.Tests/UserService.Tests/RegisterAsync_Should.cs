﻿using System;
using System.Threading.Tasks;

using Forum.Data.Models;
using Forum.Services.Forum.Services.DTOs;
using Forum.Services.Models;
using Forum.Services.Models.Contracts;
using Forum.Services.Providers.Contracts;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

namespace Forum.Tests.Services.Tests.UserService.Tests
{
    [TestClass]
    public class RegisterAsync_Should : BaseTest
    {
        private Mock<IFilterProvider> mockFilterProvider;
        private Mock<ISortProvider> mockSortProvider;
        private Mock<IRequestMapper> mockRequestMapper;

        [TestMethod]
        public async Task ReturnUserRegistrationDto_When_UserRequestIsCrrect()
        {
            // Arrange
            var user = new User
            {
                Username = "TestUsername",
                Password = "testPassword",
                Email = "TestEmail@email.com",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = DateTime.Now,
                IsActive = true,
                Role = Data.Models.Enums.UserRole.Member
            };
            var userRequestDto = new UserRequestDto
            {
                Username = "TestUsername",
                Password = "testPassword!1",
                Email = "TestEmail@email.com",
                DisplayName = "TestDisplayName"
            };

            var expectedUserRegistrationDto = new UserRegistrationDto();
            expectedUserRegistrationDto.Successfull = true;
            expectedUserRegistrationDto.Username = "TestUsername";
            expectedUserRegistrationDto.DisplayName = "TestDisplayName";
            expectedUserRegistrationDto.Email = "TestEmail@email.com";

            this.PrepareMock();
            this.mockRequestMapper
                .Setup(m => m.ToUserModel(It.IsAny<UserRequestDto>()))
                .Returns(user);
            using (var assertContext = new Data.ForumDbContext(this.options))
            {
                var sut = new Forum.Services.Forum.Services.UserService(assertContext, mockRequestMapper.Object, mockFilterProvider.Object, mockSortProvider.Object);

                // Act
                var actualUser = await sut.RegisterAsync(userRequestDto);

                //Assert
                Assert.IsNotNull(actualUser);
                Assert.AreEqual(expectedUserRegistrationDto.Username, actualUser.Username);
                Assert.AreEqual(expectedUserRegistrationDto.Email, actualUser.Email);
                Assert.AreEqual(expectedUserRegistrationDto.DisplayName, actualUser.DisplayName);
                Assert.IsTrue(actualUser.Successfull);
                Assert.IsInstanceOfType(actualUser, typeof(UserResponseDto));
            }

        }
        private void PrepareMock()
        {
            mockFilterProvider = new Mock<IFilterProvider>();
            mockSortProvider = new Mock<ISortProvider>();
            mockRequestMapper = new Mock<IRequestMapper>();
        }
    }
}