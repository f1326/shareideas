﻿
using Forum.Data.Models;
using Forum.Services.Models;
using Forum.Services.Models.Contracts;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Forum.Tests.Services.Tests.RequestMapper.Tests
{
    [TestClass]
    public class RequestMapper_Should : BaseTest
    {
        private readonly IRequestMapper mapper = new Forum.Services.Models.RequestMapper();

        [TestMethod]
        public void ReturnLikeModel()
        {
            // Arrange
            var likeRequestDto = new LikeRequestDto
            { 
            UserId = 1,
            PostId = 2
            };

            // Act
            var sut = mapper.ToLikeModel(likeRequestDto);

            // Assert
            Assert.IsNotNull(sut);
            Assert.IsInstanceOfType(sut, typeof(Like));
        }

        [TestMethod]
        public void ReturnCommentModel()
        {
            // Arrange
            var commentRequestDto = new CommentRequestDto
            {
                Title = "Test title",
                Content = "Test Content",
                UserId = 1, 
                PostId = 1
            };

            // Act
            var sut = mapper.ToCommentModel(commentRequestDto);

            // Assert
            Assert.IsNotNull(sut);
            Assert.IsInstanceOfType(sut, typeof(Comment));
        }

        [TestMethod]
        public void ReturnPostModel()
        {
            // Arrange
            var postRequestDto = new PostRequestDto
            {
                Title = "Test title",
                Content = "Test Content",
                UserId = 1                
            };

            // Act
            var sut = mapper.ToPostModel(postRequestDto);

            // Assert
            Assert.IsNotNull(sut);
            Assert.IsInstanceOfType(sut, typeof(Post));
        }

        [TestMethod]
        public void ReturnUserModel()
        {
            // Arrange
            var userRequestDto = new UserRequestDto
            {
                Username = "Test username",
                Password = "Test password",
                Email = "Test email",
                DisplayName = "Test displayname"
            };

            // Act
            var sut = mapper.ToUserModel(userRequestDto);

            // Assert
            Assert.IsNotNull(sut);
            Assert.IsInstanceOfType(sut, typeof(User));
        }
    }
}
