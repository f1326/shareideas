﻿using System;
using System.Threading.Tasks;

using Forum.Data.Models;
using Forum.Services.Forum.Services.Contracts;
using Forum.Services.Forum.Services.DTOs;
using Forum.Services.Models;
using Forum.Services.Models.Contracts;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

namespace Forum.Tests.Services.Tests.CommentService.Tests
{
    [TestClass]
    public class CreateAsync_Should : BaseTest
    {
        private Mock<IRequestMapper> mockRequestMapper;
        private Mock<IUserService> mockUserService;
        private Mock<IPostService> mockPostService;

        [TestMethod]
        public async Task ReturnCommentResponseDto()
        {
            var comment = new Comment
            {
                Title = "FirstTestTitle",
                Content = "FirstTestContent",
                CreatedAt = DateTime.UtcNow,
                UserId = 100,
                PostId = 100
            };
            var user = new User
            {
                Id = 100,
                Username = "FirstTestUsername",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = DateTime.Now,
                IsActive = true,
                Role = Data.Models.Enums.UserRole.Member
            };

            var post = new Post
            {
                Id = 100,
                Title = "Post Title",
                Content = "Post Content",
                CreatedAt = DateTime.UtcNow,
                UserId = 100
            };

            this.PrepareMock();
            this.mockRequestMapper
            .Setup(m => m.ToCommentModel(It.IsAny<CommentRequestDto>()))
            .Returns(comment);
            this.mockUserService
                .Setup(u => u.GetUserMinAsync(It.IsAny<int>()))
                .ReturnsAsync(user);
            this.mockPostService
                .Setup(p => p.GetPostAsync(It.IsAny<int>()))
                .ReturnsAsync(post);

            var expectedCommentResponseDto = new CommentResponseDto
            {
                Title = "FirstTestTitle",
                Content = "FirstTestContent",
                UserDisplayName = "TestDisplayName",
                PostTitle = "Post Title",
                UserId = 100,
                PostId = 100
            };
            var commentRequestModel = new CommentRequestDto();

            using (var assertContext = new Data.ForumDbContext(this.options))
            {
                var sut = new Forum.Services.Forum.Services.CommentService(assertContext, mockRequestMapper.Object, mockUserService.Object, mockPostService.Object);
                await assertContext.Users.AddAsync(user);
                await assertContext.Posts.AddAsync(post);
                await assertContext.SaveChangesAsync();

                // Act
                var actualCommentResponseDto = await sut.CreateAsync(commentRequestModel);

                // Assert
                Assert.IsNotNull(actualCommentResponseDto);
                Assert.AreEqual(expectedCommentResponseDto.UserId, actualCommentResponseDto.UserId);
                Assert.AreEqual(expectedCommentResponseDto.PostId, actualCommentResponseDto.PostId);
                Assert.IsInstanceOfType(actualCommentResponseDto, typeof(CommentResponseDto));
            }

        }
        private void PrepareMock()
        {
            mockPostService = new Mock<IPostService>();
            mockUserService = new Mock<IUserService>();
            mockRequestMapper = new Mock<IRequestMapper>();
        }
    }
}
