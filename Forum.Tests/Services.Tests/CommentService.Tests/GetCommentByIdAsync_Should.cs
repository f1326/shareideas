﻿using System;
using System.Threading.Tasks;

using Forum.Data.Models;
using Forum.Services.Forum.Services.Contracts;
using Forum.Services.Forum.Services.DTOs;
using Forum.Services.Models.Contracts;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

namespace Forum.Tests.Services.Tests.CommentService.Tests
{
    [TestClass]
    public class GetCommentByIdAsync_Should : BaseTest
    {
        private Mock<IRequestMapper> mockRequestMapper;
        private Mock<IUserService> mockUserService;
        private Mock<IPostService> mockPostService;

        public Mock<IUserService> MockUserService { get => mockUserService; set => mockUserService = value; }

        [TestMethod]
        public async Task ReturnCommentResponseDto()
        {
            var comment = new Comment
            {
                Id = 100,
                Title = "FirstTestTitle",
                Content = "FirstTestContent",
                CreatedAt = DateTime.UtcNow,
                UserId = 100,
                PostId = 100
            };

            var user = new User
            {
                Id = 100,
                Username = "FirstTestUsername",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = DateTime.Now,
                IsActive = true,
                Role = Data.Models.Enums.UserRole.Member
            };

            var post = new Post
            {
                Id = 100,
                Title = "Post Title",
                Content = "Post Content",
                CreatedAt = DateTime.UtcNow,
                UserId = 100
            };

            var expectedCommentResponseDto = new CommentResponseDto
            {
                Id = 100,
                Title = "FirstTestTitle",
                Content = "FirstTestContent",
                UserDisplayName = "TestDisplayName",
                PostTitle = "Post Title",
                UserId = 100,
                PostId = 100
            };
            this.PrepareMock();

            using (var assertContext = new Data.ForumDbContext(this.options))
            {
                var sut = new Forum.Services.Forum.Services.CommentService(assertContext, mockRequestMapper.Object, MockUserService.Object, mockPostService.Object);
                await assertContext.Users.AddAsync(user);
                await assertContext.Posts.AddAsync(post);
                await assertContext.Comments.AddAsync(comment);
                await assertContext.SaveChangesAsync();

                var commentId = 100;

                // Act
                var actualCommentResponseDto = await sut.GetCommentByIdAsync(commentId);

                // Assert
                Assert.IsNotNull(actualCommentResponseDto);
                Assert.AreEqual(expectedCommentResponseDto.Id, actualCommentResponseDto.Id);
                Assert.AreEqual(expectedCommentResponseDto.Title, actualCommentResponseDto.Title);
                Assert.AreEqual(expectedCommentResponseDto.Content, actualCommentResponseDto.Content);
                Assert.IsInstanceOfType(actualCommentResponseDto, typeof(CommentResponseDto));
            }

        }
        private void PrepareMock()
        {
            mockPostService = new Mock<IPostService>();
            MockUserService = new Mock<IUserService>();
            mockRequestMapper = new Mock<IRequestMapper>();
        }
    }
}
