﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Forum.Data.Models;
using Forum.Services.Forum.Services.Contracts;
using Forum.Services.Forum.Services.DTOs;
using Forum.Services.Models.Contracts;
using Forum.Services.Providers.Contracts;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

namespace Forum.Tests.Services.Tests.PostService.Tests
{
    [TestClass]
    public class GetMostPopularPostsAsync_Should : BaseTest
    {
        private Mock<IUserService> mockUserService;
        private Mock<IRequestMapper> mockMapper;
        private Mock<IFilterProvider> mockFilterProvider;
        private Mock<ISortProvider> mockSortProvider;

        [TestMethod]
        public async Task ReturnCollectionOfPostResponseDtos()
        {
            // Arrange
            var user = new User
            {
                Id = 100,
                Username = "TestUsrname",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = DateTime.Now,
                IsActive = true,
                Role = Data.Models.Enums.UserRole.Member
            };

            var firstPost = new Post
            {
                Id = 100,
                Title = "Test Title",
                Content = "Test content",
                UserId = 100,
                CreatedAt = DateTime.Now,
            };

            var secondPost = new Post
            {
                Id = 200,
                Title = "Second Test Title",
                Content = "Second Test content",
                UserId = 100,
                CreatedAt = DateTime.Now.AddDays(1)
            };

            var expectedListPostResponseDtos = new List<PostResponseDto>
            {
                   new PostResponseDto
                   {
                   Title = "Test Title",
                   Content = "Test content"
                   },

                  new PostResponseDto
                  {
                  Title = "Second Test Title",
                  Content = "Second Test content"
                  }
            };

            this.PrepareMock();

            using (var assertContext = new Data.ForumDbContext(this.options))
            {
                await assertContext.Users.AddAsync(user);
                await assertContext.Posts.AddAsync(firstPost);
                await assertContext.Posts.AddAsync(secondPost);
                await assertContext.SaveChangesAsync();
                
                var sut = new Forum.Services.Forum.Services.PostService(assertContext, mockUserService.Object, mockMapper.Object, mockFilterProvider.Object, mockSortProvider.Object);
                int postCount = 10;

                // Act
                var actualPostResponseDtosCollection = await sut.GetMostPopularPostsAsync(postCount);
                var actualListPostresponseDtos = actualPostResponseDtosCollection.ToList();

                // Assert
                Assert.IsNotNull(actualPostResponseDtosCollection);
                Assert.AreEqual(expectedListPostResponseDtos[expectedListPostResponseDtos.Count - 1].Title, actualListPostresponseDtos[actualListPostresponseDtos.Count - 1].Title);
                Assert.AreEqual(expectedListPostResponseDtos[expectedListPostResponseDtos.Count - 2].Content, actualListPostresponseDtos[actualListPostresponseDtos.Count - 2].Content);
                Assert.IsInstanceOfType(actualPostResponseDtosCollection, typeof(IEnumerable<PostResponseDto>));
            }
        }

        private void PrepareMock()
        {
            mockUserService = new Mock<IUserService>();
            mockFilterProvider = new Mock<IFilterProvider>();
            mockSortProvider = new Mock<ISortProvider>();
            mockMapper = new Mock<IRequestMapper>();
        }
    }
}
