﻿using System;
using System.Threading.Tasks;

using Forum.Data.Models;
using Forum.Services.Forum.Services.Contracts;
using Forum.Services.Forum.Services.DTOs;
using Forum.Services.Models;
using Forum.Services.Models.Contracts;
using Forum.Services.Providers.Contracts;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

namespace Forum.Tests.Services.Tests.PostService.Tests
{
    [TestClass]
    public class CreateAsync_Should : BaseTest
    {
        private Mock<IUserService> mockUserService;
        private Mock<IRequestMapper> mockMapper;
        private Mock<IFilterProvider> mockFilterProvider;
        private Mock<ISortProvider> mockSortProvider;

        [TestMethod]
        public async Task ReturnPostResponseDto()
        {
            // Arrange
            var user = new User
            {
                Username = "TestUsrname",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = DateTime.Now,
                IsActive = true,
                Role = Data.Models.Enums.UserRole.Member
            };

            var post = new Post
            {
                Title = "Test Title",
                Content = "Test content",
                UserId = 100
            };

            var postRequestDto = new PostRequestDto
            {
                Title = "Test Title",
                Content = "Test content",
                UserId = 100
            };

            var expectedPostResponseDto = new PostResponseDto
            {
            Title = "Test Title",
            Content = "Test content",
            DateCreated = DateTime.Now,
        };

            this.PrepareMock();
            mockMapper.Setup(m => m.ToPostModel(It.IsAny<PostRequestDto>()))
                .Returns(post);
            mockUserService.Setup(m => m.GetUserAsync(It.IsAny<int>()))
                .ReturnsAsync(user);

            using (var assertContext = new Data.ForumDbContext(this.options))
            {
                await assertContext.Users.AddAsync(user);
                var sut = new Forum.Services.Forum.Services.PostService(assertContext, mockUserService.Object, mockMapper.Object, mockFilterProvider.Object, mockSortProvider.Object);

                // Act
                var actualPostResponseDto = await sut.CreateAsync(postRequestDto);

                // Assert
                Assert.IsNotNull(actualPostResponseDto);
                Assert.AreEqual(expectedPostResponseDto.Title, actualPostResponseDto.Title);
                Assert.AreEqual(expectedPostResponseDto.Content, actualPostResponseDto.Content);
                Assert.IsInstanceOfType(actualPostResponseDto, typeof(PostResponseDto));
            }

        }


        private void PrepareMock()
        {
            mockUserService = new Mock<IUserService>();
            mockFilterProvider = new Mock<IFilterProvider>();
            mockSortProvider = new Mock<ISortProvider>();
            mockMapper = new Mock<IRequestMapper>();
        }
    }
}
