﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Forum.Services.Forum.Services.Contracts;
using Forum.Services.Forum.Services.DTOs;
using Forum.Services.Models.Contracts;
using Forum.Services.Providers.Contracts;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

namespace Forum.Tests.Services.Tests.PostService.Tests
{
    [TestClass]
   public  class GetFilteredPostsAsync_Should : BaseTest
    {
        private Mock<IUserService> mockUserService;
        private Mock<IRequestMapper> mockMapper;
        private Mock<IFilterProvider> mockFilterProvider;
        private Mock<ISortProvider> mockSortProvider;

        [TestMethod]
        public async Task ReturnCollectionOfPostResponseDtos()
        {
            // Arrange
          
            var firstPost = new PostResponseDto
            {
                Id = 100,
                Title = "Test Title",
                Content = "Test content"
            };

            var secondPost = new PostResponseDto
            {
                Id = 200,
                Title = "Second Test Title",
                Content = "Second Test content"
            };

            var expectedListPostResponseDtos = new List<PostResponseDto>
            {
                   new PostResponseDto
                   {
                   Title = "Test Title",
                   Content = "Test content"
                   },

                  new PostResponseDto
                  {
                  Title = "Second Test Title",
                  Content = "Test content"
                  }
            };

            var postsList = new List<PostResponseDto>();
            postsList.Add(firstPost);
            postsList.Add(secondPost);
            var postsQueryable = postsList.AsQueryable();
            this.PrepareMock();
            mockFilterProvider.Setup(m => m.FilterAsync<PostResponseDto>(It.IsAny<IQueryable<PostResponseDto>>(), It.IsAny<string>()))
                .ReturnsAsync(postsQueryable);

            using (var assertContext = new Data.ForumDbContext(this.options))
            {
                var sut = new Forum.Services.Forum.Services.PostService(assertContext, mockUserService.Object, mockMapper.Object, mockFilterProvider.Object, mockSortProvider.Object);
                var firstFilterType = "Content";
                var firstFilterValue = "Test content";
                string secondFilterType = null;
                string secondFilterValue = null;

                // Act
                var actualPostResponseDtosCollection = await sut.GetFilteredPostsAsync(firstFilterType, firstFilterValue, secondFilterType, secondFilterValue);
                var actualListPostresponseDtos = actualPostResponseDtosCollection.ToList();

                // Assert
                Assert.IsNotNull(actualPostResponseDtosCollection);
                Assert.AreEqual(expectedListPostResponseDtos[expectedListPostResponseDtos.Count - 1].Title, actualListPostresponseDtos[actualListPostresponseDtos.Count - 1].Title);
                Assert.AreEqual(expectedListPostResponseDtos[expectedListPostResponseDtos.Count - 2].Content, actualListPostresponseDtos[actualListPostresponseDtos.Count - 2].Content);
                Assert.IsInstanceOfType(actualPostResponseDtosCollection, typeof(IEnumerable<PostResponseDto>));
            }
        }

        private void PrepareMock()
        {
            mockUserService = new Mock<IUserService>();
            mockFilterProvider = new Mock<IFilterProvider>();
            mockSortProvider = new Mock<ISortProvider>();
            mockMapper = new Mock<IRequestMapper>();
        }
    }
}