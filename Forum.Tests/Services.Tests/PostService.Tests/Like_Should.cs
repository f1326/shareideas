﻿using System;
using System.Threading.Tasks;

using Forum.Data.Models;
using Forum.Services.Forum.Services.Contracts;
using Forum.Services.Forum.Services.DTOs;
using Forum.Services.Models;
using Forum.Services.Models.Contracts;
using Forum.Services.Providers.Contracts;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

namespace Forum.Tests.Services.Tests.PostService.Tests
{
    [TestClass]
    public class Like_Should : BaseTest
    {
        private Mock<IUserService> mockUserService;
        private Mock<IRequestMapper> mockMapper;
        private Mock<IFilterProvider> mockFilterProvider;
        private Mock<ISortProvider> mockSortProvider;

        [TestMethod]
        public async Task ReturnLikeResponseDto()
        {
            // Arrange
            var user = new User
            {
                Id = 100,
                Username = "TestUsrname",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = DateTime.Now,
                IsActive = true,
                Role = Data.Models.Enums.UserRole.Member
            };
            
            var post = new Post
            {
                Id = 100,
                Title = "Test Title",
                Content = "Test content",
                UserId = 100
            };

            var like = new Like
            {
                UserId = 100,
                User = user,
                PostId = 100,
                Post = post
            };
            var likeRequestDto = new LikeRequestDto
            {
                UserId = 100,
                PostId = 100
            };

            var expectedLikeResponseDto = new LikeResponseDto
            {
                userEmail = "TestEmail"
            };
            this.PrepareMock();
            mockMapper.Setup(m => m.ToLikeModel(It.IsAny<LikeRequestDto>()))
                .Returns(like);
            mockUserService.Setup(m => m.GetUserAsync(It.IsAny<int>()))
                .ReturnsAsync(user);

            using (var assertContext = new Data.ForumDbContext(this.options))
            {
                await assertContext.Users.AddAsync(user);
                await assertContext.Posts.AddAsync(post);
                await assertContext.SaveChangesAsync();

                var sut = new Forum.Services.Forum.Services.PostService(assertContext, mockUserService.Object, mockMapper.Object, mockFilterProvider.Object, mockSortProvider.Object);
                //var postId = 100;
                //var commentId = 100;

                // Act
                var actualLikeResponseDto = await sut.LikePost(likeRequestDto);

                // Assert
                Assert.IsNotNull(actualLikeResponseDto);
                Assert.AreEqual(actualLikeResponseDto.userEmail, expectedLikeResponseDto.userEmail);
                Assert.IsInstanceOfType(actualLikeResponseDto, typeof(LikeResponseDto));
            }

        }


        private void PrepareMock()
        {
            mockUserService = new Mock<IUserService>();
            mockFilterProvider = new Mock<IFilterProvider>();
            mockSortProvider = new Mock<ISortProvider>();
            mockMapper = new Mock<IRequestMapper>();
        }
    }
}
