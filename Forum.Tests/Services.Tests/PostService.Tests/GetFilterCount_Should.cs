﻿using System.Threading.Tasks;

using Forum.Services.Forum.Services.Contracts;
using Forum.Services.Models.Contracts;
using Forum.Services.Providers.Contracts;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

namespace Forum.Tests.Services.Tests.PostService.Tests
{
    [TestClass]
    public class GetFilterCount_Should : BaseTest
    {
        private Mock<IUserService> mockUserService;
        private Mock<IRequestMapper> mockMapper;
        private Mock<IFilterProvider> mockFilterProvider;
        private Mock<ISortProvider> mockSortProvider;

        [TestMethod]
        public async Task ReturnCorectNUmberOfPosts()
        {

            this.PrepareMock();

            using (var assertContext = new Data.ForumDbContext(this.options))
            {

                await assertContext.SaveChangesAsync();

                var sut = new Forum.Services.Forum.Services.PostService(assertContext, mockUserService.Object, mockMapper.Object, mockFilterProvider.Object, mockSortProvider.Object);

                // Act
                var expectedOutput = 1;
                string containsTitle = "CORS";
                string startDate = "2018-10-10";
                string endDate = "2021-01-01";

               

                var actualResponseDto =  sut.GetFilterCount(startDate, endDate, containsTitle);
                // Assert
                Assert.AreEqual(expectedOutput, actualResponseDto);
             
            }
        }

        private void PrepareMock()
        {
            mockUserService = new Mock<IUserService>();
            mockFilterProvider = new Mock<IFilterProvider>();
            mockSortProvider = new Mock<ISortProvider>();
            mockMapper = new Mock<IRequestMapper>();
        }
    }
}
