﻿using System.Linq;
using System.Threading.Tasks;

using Forum.Services.Forum.Services.Contracts;
using Forum.Services.Models.Contracts;
using Forum.Services.Providers.Contracts;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

namespace Forum.Tests.Services.Tests.PostService.Tests
{
    [TestClass]
    public class GetPaggination_Should : BaseTest
    {
        private Mock<IUserService> mockUserService;
        private Mock<IRequestMapper> mockMapper;
        private Mock<IFilterProvider> mockFilterProvider;
        private Mock<ISortProvider> mockSortProvider;

        [TestMethod]
        public async Task ReturnCorectCollectionWithDto()
        {

            this.PrepareMock();

            using (var assertContext = new Data.ForumDbContext(this.options))
            {

                await assertContext.SaveChangesAsync();

                var sut = new Forum.Services.Forum.Services.PostService(assertContext, mockUserService.Object, mockMapper.Object, mockFilterProvider.Object, mockSortProvider.Object);

                // Act
                int skip = 1;
                int take = 2;
                var actualPostResponseDto = await sut.GetPaggination(skip, take);
                var collection = actualPostResponseDto.ToList();
                // Assert
                Assert.AreEqual(take, collection.Count);
            }
        }

        private void PrepareMock()
        {
            mockUserService = new Mock<IUserService>();
            mockFilterProvider = new Mock<IFilterProvider>();
            mockSortProvider = new Mock<ISortProvider>();
            mockMapper = new Mock<IRequestMapper>();
        }
    }
}
