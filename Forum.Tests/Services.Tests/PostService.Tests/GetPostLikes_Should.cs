﻿using System;
using System.Linq;
using System.Threading.Tasks;

using Forum.Data.Models;
using Forum.Services.Forum.Services.Contracts;
using Forum.Services.Models.Contracts;
using Forum.Services.Providers.Contracts;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

namespace Forum.Tests.Services.Tests.PostService.Tests
{
    [TestClass]
    public class GetPostLikes_Should : BaseTest
    {
        private Mock<IUserService> mockUserService;
        private Mock<IRequestMapper> mockMapper;
        private Mock<IFilterProvider> mockFilterProvider;
        private Mock<ISortProvider> mockSortProvider;

        [TestMethod]
        public async Task ReturnCollectionOfEmails()
        {
            // Arrange
            var user = new User
            {
                Id = 100,
                Username = "TestUsrname",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = DateTime.Now,
                IsActive = true,
                Role = Data.Models.Enums.UserRole.Member
            };

            var user2 = new User
            {
                Id = 102,
                Username = "TestUsrname2",
                Password = "testPassword2",
                Email = "TestEmail2",
                DisplayName = "TestDisplayName2",
                Rating = 0,
                MemberSince = DateTime.Now,
                IsActive = true,
                Role = Data.Models.Enums.UserRole.Member
            };
            var post = new Post
            {
                Id = 100,
                Title = "Test Title",
                Content = "Test content",
                UserId = 100
            };

            var like = new Like
            {
                UserId = 100,
                User = user,
                PostId = 100,
                Post = post
            };
            var like2 = new Like
            {
                UserId = 102,
                User = user2,
                PostId = 100,
                Post = post
            };


            this.PrepareMock();

            using (var assertContext = new Data.ForumDbContext(this.options))
            {
                await assertContext.Users.AddAsync(user);
                await assertContext.Users.AddAsync(user2);
                await assertContext.Posts.AddAsync(post);
                await assertContext.Likes.AddAsync(like);
                await assertContext.Likes.AddAsync(like2);
                await assertContext.SaveChangesAsync();

                var sut = new Forum.Services.Forum.Services.PostService(assertContext, mockUserService.Object, mockMapper.Object, mockFilterProvider.Object, mockSortProvider.Object);
                var postId = 100;

                // Act
                var actualCollectionOfEmails =  sut.GetPostLikes(postId).ToList();

                // Assert
                Assert.IsNotNull(actualCollectionOfEmails);
                Assert.AreEqual("TestEmail", actualCollectionOfEmails[0]);
                Assert.AreEqual("TestEmail2", actualCollectionOfEmails[1]);
            }
        }

        private void PrepareMock()
        {
            mockUserService = new Mock<IUserService>();
            mockFilterProvider = new Mock<IFilterProvider>();
            mockSortProvider = new Mock<ISortProvider>();
            mockMapper = new Mock<IRequestMapper>();
        }
    }
}
