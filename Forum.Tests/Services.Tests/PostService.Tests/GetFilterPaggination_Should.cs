﻿using System.Linq;
using System.Threading.Tasks;

using Forum.Services.Forum.Services.Contracts;
using Forum.Services.Models.Contracts;
using Forum.Services.Providers.Contracts;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

namespace Forum.Tests.Services.Tests.PostService.Tests
{
    [TestClass]
    public class GetFilterPaggination_Should : BaseTest
    {
        private Mock<IUserService> mockUserService;
        private Mock<IRequestMapper> mockMapper;
        private Mock<IFilterProvider> mockFilterProvider;
        private Mock<ISortProvider> mockSortProvider;

        [TestMethod]
        public async Task ReturnCorectNUmberOfPosts()
        {

            this.PrepareMock();

            using (var assertContext = new Data.ForumDbContext(this.options))
            {

                await assertContext.SaveChangesAsync();

                var sut = new Forum.Services.Forum.Services.PostService(assertContext, mockUserService.Object, mockMapper.Object, mockFilterProvider.Object, mockSortProvider.Object);

                // Act
                var expectedOutput = "How to enable CORS in ASP.net Core WebAPI";
                string sortingType1 = "likes";
                string sortingDirection1 = "ascedning";
                string startDate1 = "";
                string endDate1 = "2021-01-01";
                string containsTitle1 = "CORS";

                string sortingDirection2 = "descending";
                string startDate2 = "2018-10-10";
                string endDate2 = "2021-01-01";

                string sortingType2 = "date";
                string sortingType3 = "comments";
                string sortingType4 = "";

                int skip = 0;
                int take = 2;
                var actualResponseDto1 = await sut.GetFilterPaggination(sortingType1, sortingDirection1, startDate1, endDate1, containsTitle1, skip, take);
                var collection1 = actualResponseDto1.ToList();
                var actualResponseDto2 = await sut.GetFilterPaggination(sortingType1, sortingDirection2, startDate2, endDate2, containsTitle1, skip, take);
                var collection2 = actualResponseDto1.ToList();
                var actualResponseDto3 = await sut.GetFilterPaggination(sortingType2, sortingDirection1, startDate2, endDate2, containsTitle1, skip, take);
                var collection3 = actualResponseDto1.ToList();
                var actualResponseDto4 = await sut.GetFilterPaggination(sortingType2, sortingDirection2, startDate2, endDate2, containsTitle1, skip, take);
                var collection4 = actualResponseDto1.ToList();
                var actualResponseDto5 = await sut.GetFilterPaggination(sortingType3, sortingDirection1, startDate2, endDate2, containsTitle1, skip, take);
                var collection5 = actualResponseDto1.ToList();
                var actualResponseDto6 = await sut.GetFilterPaggination(sortingType3, sortingDirection2, startDate2, endDate2, containsTitle1, skip, take);
                var collection6 = actualResponseDto1.ToList();
                var actualResponseDto7 = await sut.GetFilterPaggination(sortingType4, sortingDirection1, startDate2, endDate2, containsTitle1, skip, take);
                var collection7 = actualResponseDto1.ToList();
                var actualResponseDto8 = await sut.GetFilterPaggination(sortingType4, sortingDirection2, startDate2, endDate2, containsTitle1, skip, take);
                var collection8 = actualResponseDto1.ToList();
                // Assert
                Assert.AreEqual(1, collection1.Count);
                Assert.AreEqual(expectedOutput, collection1[0].Title);
                Assert.AreEqual(expectedOutput, collection2[0].Title);
                Assert.AreEqual(expectedOutput, collection3[0].Title);
                Assert.AreEqual(expectedOutput, collection4[0].Title);
                Assert.AreEqual(expectedOutput, collection5[0].Title);
                Assert.AreEqual(expectedOutput, collection6[0].Title);
                Assert.AreEqual(expectedOutput, collection7[0].Title);
                Assert.AreEqual(expectedOutput, collection8[0].Title);
            }
        }

        private void PrepareMock()
        {
            mockUserService = new Mock<IUserService>();
            mockFilterProvider = new Mock<IFilterProvider>();
            mockSortProvider = new Mock<ISortProvider>();
            mockMapper = new Mock<IRequestMapper>();
        }
    }
}
