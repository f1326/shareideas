﻿using System;
using System.Threading.Tasks;

using Forum.Data.Models;
using Forum.Services.Forum.Services.Contracts;
using Forum.Services.Forum.Services.DTOs;
using Forum.Services.Models;
using Forum.Services.Models.Contracts;
using Forum.Services.Providers.Contracts;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

namespace Forum.Tests.Services.Tests.PostService.Tests
{
    [TestClass]
    public class EditAsync_Should : BaseTest
    {

        private Mock<IUserService> mockUserService;
        private Mock<IRequestMapper> mockMapper;
        private Mock<IFilterProvider> mockFilterProvider;
        private Mock<ISortProvider> mockSortProvider;

        [TestMethod]
        public async Task ReturnPostResponseDto()
        {
            // Arrange
            var user = new User
            {
                Id = 100,
                Username = "TestUsrname",
                Password = "testPassword",
                Email = "TestEmail",
                DisplayName = "TestDisplayName",
                Rating = 0,
                MemberSince = DateTime.Now,
                IsActive = true,
                Role = Data.Models.Enums.UserRole.Member
            };

            var post = new Post
            {
                Id = 100,
                Title = "Test Title",
                Content = "Test content",
                UserId = 100
            };


            var Mappedpost = new Post
            {
                Id = 100,
                Title = "UpdatedTest Title",
                Content = "UpdatedTest Content",
                UserId = 100
            };

            var postRequestDto = new PostRequestDto
            {
                Title = "UpdatedTest Title",
                Content = "UpdatedTest Content",
                UserId = 100
            };

            var expectedPostResponseDto = new PostResponseDto
            {
                Title = "UpdatedTest Title",
                Content = "UpdatedTest Content",
            };

            this.PrepareMock();
            mockMapper.Setup(m => m.ToPostModel(It.IsAny<PostRequestDto>()))
               .Returns(Mappedpost);

            using (var assertContext = new Data.ForumDbContext(this.options))
            {
                await assertContext.Users.AddAsync(user);
                await assertContext.Posts.AddAsync(post);
                await assertContext.SaveChangesAsync();

                var sut = new Forum.Services.Forum.Services.PostService(assertContext, mockUserService.Object, mockMapper.Object, mockFilterProvider.Object, mockSortProvider.Object);
                var postId = 100;

                // Act
                var actualPostResponseDto = await sut.EditAsync(postId, postRequestDto);

                // Assert
                Assert.IsNotNull(actualPostResponseDto);
                Assert.AreEqual(expectedPostResponseDto.Title, actualPostResponseDto.Title);
                Assert.AreEqual(expectedPostResponseDto.Content, actualPostResponseDto.Content);
                Assert.IsInstanceOfType(actualPostResponseDto, typeof(PostResponseDto));
            }
        }

        private void PrepareMock()
        {
            mockUserService = new Mock<IUserService>();
            mockFilterProvider = new Mock<IFilterProvider>();
            mockSortProvider = new Mock<ISortProvider>();
            mockMapper = new Mock<IRequestMapper>();
        }
    }
}
