﻿using Forum.Data;

using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Forum.Tests.Services.Tests
{
    public class BaseTest
    {
        public DbContextOptions<ForumDbContext> options;


        [TestInitialize]
        public void Initialize()
        {
            this.options = Utils.GetOptions(nameof(TestContext.TestName));

            using (var arrangeContext = new ForumDbContext(this.options))
            {
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.Posts.AddRange(Utils.GetPosts());
                arrangeContext.Comments.AddRange(Utils.GetComments());
                arrangeContext.SaveChanges();
            }
        }
        [TestCleanup]
        public void Cleanup()
        {
            var options = Utils.GetOptions(nameof(TestContext.TestName));
            var context = new ForumDbContext(options);
            context.Database.EnsureDeleted();
        }
    }
}
