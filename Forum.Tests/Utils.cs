﻿using Forum.Data;
using Forum.Data.Models;

using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace Forum.Tests
{
    public class Utils
    {
        public static DbContextOptions<ForumDbContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<ForumDbContext>()
                       .UseInMemoryDatabase(databaseName)
                       .Options;
        }
        public static IEnumerable<User> GetUsers()
        {
            return ModelBuilderExtension.Users;
        }
        public static IEnumerable<Post> GetPosts()
        {
            return ModelBuilderExtension.Posts;
        }
        public static IEnumerable<Comment> GetComments()
        {
            return ModelBuilderExtension.Comments;
        }
    }
}
