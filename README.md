 # The Software lab

 ## The Software Lab is a forum web application where pleaople can share and search for ideas in software development.

 - The application is prepared for Telerik Academy final project.
 - To start the application, please, setup the Solution properties to start multiple projects: Forum and Forum.WebApi
 - You can check the Swagger documentation locally here: http://localhost:6001/swagger/index.html
 - You can visit the website on https://softwarelabtelerik.azurewebsites.net/

 ## Main functionalities ##

 - People can register in the web site. Registration of a user is based on email verification flow.
 - Everyone can check what are the forum's post together with sorting and filtering them. 
 - After registration and login, the users can check all comments for every created post in the site. 
 - Registered users has the capabilities to create posts. They also can add comments to existing posts and based on this the users can have  comunication between each other.
 - Registered users has the ability to manage their accounts like changing password, profile picture and display name.
 - There are admins who maintain the website. Their additional function is to block a user if he does not follow the forum rules. In case of some misunderstanding, every blocked user can be unblocked from an admin.
 - If some user need some help of working with the website, there is a chat bot which is available for everyone visiting the forum.

 ## Technologies ##

 - ASP.NET Core 3.1 MVC
 - ASP.NET Core 3.1 Web API
 - MS SQL Server
 - Entity Framework Core 5.0
 - Bootstrap
 - jQuery
 - JavaScript
 - Moq

## Pictures related to the project ## 
 -Database diagram:

![](Images/DatabaseDiagram.png)

- Registration form:

![](Images/Registration.png)

- Users grid where admin can menage users' access to the website.
 
![](Images/UsersGrid.png)

- Posts grid where each user can menage all posts.

![](Images/PostsGrid.jpg)

