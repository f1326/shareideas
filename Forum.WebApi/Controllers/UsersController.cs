﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Forum.Services.Forum.Services.Contracts;
using Forum.Services.Forum.Services.DTOs;
using Forum.WebApi.Exceptions;
using Forum.WebApi.Helpers;
using Forum.WebApi.Models;
using Forum.WebApi.Models.Contracts;

using Microsoft.AspNetCore.Mvc;

using Swashbuckle.AspNetCore.Annotations;

namespace Forum.Api.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IUserService userService;
        private readonly IWebModelMapper webModelMapper;
        private readonly IAuthHelper authHelper;
        public UsersController(IUserService userService, IWebModelMapper webModelMapper, IAuthHelper authHelper)
        {
            this.userService = userService;
            this.webModelMapper = webModelMapper;
            this.authHelper = authHelper;
        }

        [HttpGet("")]
        [SwaggerOperation(Summary = "Gets a list of all users.")]
        [ProducesResponseType(typeof(IEnumerable<UserResponseDto>), statusCode: 200)]
        [SwaggerResponse(404, "A User with the requested credentials does not exist.")]
        [SwaggerResponse(401, "The user does not have the necessary authorization.")]
        public async Task<IActionResult> GetAsync([FromHeader] string authorizationEmail, [FromHeader] string authorizationPassword)
        {
            try
            {
                var user = await this.authHelper.TryGetUserAsync(authorizationEmail,authorizationPassword);
                if (user.Role == "Admin" || user.Role == "Moderator")
                {
                    var users = await this.userService.GetAllAsync();
                    return this.Ok(users);
                }

                return this.Unauthorized("You are not authorized.");
            }
            catch (AuthenticationException ex)
            {
                return this.Unauthorized(ex.Message);
            }
           
        }

        [HttpGet("{id}")]
        [SwaggerOperation(Summary = "Gets a user.")]
        [ProducesResponseType(typeof(UserResponseDto), statusCode: 200)]
        [SwaggerResponse(404, "A User with the requested credentials does not exist.")]
        [SwaggerResponse(401, "The user does not have the necessary authorization.")]
        public async Task<IActionResult> GetAsync( [FromHeader] string authorizationEmail, [FromHeader] string authorizationPassword, int id)
        {
            try
            {
                var user = await this.authHelper.TryGetUserAsync(authorizationEmail, authorizationPassword);
                if (user.Role == "Admin" || user.Role == "Moderator" || user.Id == id)
                {
                    var userResponseDto = await this.userService.GetUserByIdAsync(id);
                    return this.Ok(userResponseDto);
                }
                return this.Unauthorized("You are not authorized.");
            }
            catch (AuthenticationException ex)
            {
                return this.Unauthorized(ex.Message);
            }
        }

        [HttpPost("")]
        [SwaggerOperation(Summary = "Creates a new user and returns it.")]
        [ProducesResponseType(typeof(UserResponseDto), statusCode: 200)]
        public async Task<IActionResult> PostAsync([FromBody] UserWebModel userWebModel)
        {
            var userRequestDto = this.webModelMapper.ToUserRequestDto(userWebModel);
            var userResponseDto = await this.userService.CreateAsync(userRequestDto);

            return this.Ok(userResponseDto);
        }

        [HttpPut("{id}")]
        [SwaggerOperation(Summary = "Updates the user's information and returns the user..", 
            Description = "Updating information of username, email, password and display name.")]
        [ProducesResponseType(typeof(UserResponseDto), statusCode: 200)]
        [SwaggerResponse(404, "A User with the requested credentials does not exist.")]
        [SwaggerResponse(401, "The user does not have the necessary authorization.")]
        public async Task<IActionResult> PutAsync( [FromHeader] string authorizationEmail, [FromHeader] string authorizationPassword, int id, [FromBody] UserWebModel userWebModel)
        {
            try
            {
                var user = await this.authHelper.TryGetUserAsync(authorizationEmail, authorizationPassword);
                if (user.Role == "Admin" || user.Role == "Moderator" || user.Id == id)
                {
                    var userRequestDto = this.webModelMapper.ToUserRequestDto(userWebModel);
                    var userResponseDto = await this.userService.UpdateAsync(id, userRequestDto);

                    return this.Ok(userResponseDto);
                }
                return this.Unauthorized("You are not authorized.");
            }
            catch (AuthenticationException ex)
            {
                return this.Unauthorized(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        [SwaggerOperation(Summary = "Deletes a user.")]
        [ProducesResponseType(typeof(UserResponseDto), statusCode: 204)]
        [SwaggerResponse(404, "A User with the requested credentials does not exist.")]
        [SwaggerResponse(401, "The user does not have the necessary authorization.")]
        public async Task<IActionResult> DeleteAsync([FromHeader] string authorizationEmail, [FromHeader] string authorizationPassword, int id)
        {
            try
            {
                var user = await this.authHelper.TryGetUserAsync(authorizationEmail, authorizationPassword);
                if (user.Role == "Admin")
                {
                    var userResponceDto = await this.userService.DeleteAsync(id);
                   
                    return this.NoContent();
                }

                return this.Unauthorized("You are not authorized.");
            }
            catch (AuthenticationException ex)
            {
                return this.Unauthorized(ex.Message);
            }
            
        }

        [HttpGet("{id}/block")]
        [SwaggerOperation(Summary = "Gets the blocked user.")]
        [ProducesResponseType(typeof(UserResponseDto), statusCode: 200)]
        [SwaggerResponse(404, "A User with the requested credentials does not exist.")]
        [SwaggerResponse(401, "The user does not have the necessary authorization.")]
        public async Task<IActionResult> BlockUserAsync( [FromHeader] string authorizationEmail, [FromHeader] string authorizationPassword, int id)
        {
            try
            {
                var user = await this.authHelper.TryGetUserAsync(authorizationEmail, authorizationPassword);
                if (user.Role == "Admin")
                {
                    var userResponseDto = await this.userService.BlockUser(id);

                    return this.Ok(userResponseDto);
                }

                return this.Unauthorized("You are not authorized.");
            }
            catch (AuthenticationException ex)
            {
                return this.Unauthorized(ex.Message);
            }   
        }

       [HttpGet("{id}/unblock")]
       [SwaggerOperation(Summary = "Gets the unblocked user.")]
        [ProducesResponseType(typeof(UserResponseDto), statusCode: 200)]
        [SwaggerResponse(404, "A User with the requested credentials does not exist.")]
       [SwaggerResponse(401, "The user does not have the necessary authorization.")]
        public async Task<IActionResult> UnblockUserAsync( [FromHeader] string authorizationEmail, [FromHeader] string authorizationPassword, int id)
        {
            try
            {
                var user = await this.authHelper.TryGetUserAsync(authorizationEmail, authorizationPassword);
                if (user.Role == "Admin")
                {
                    var userResponseDto = await this.userService.UnblockUser(id);

                    return this.Ok(userResponseDto);
                }

                return this.Unauthorized("You are not authorized.");
            }
            catch (AuthenticationException ex)
            {
                return this.Unauthorized(ex.Message);
            }
        }

        [HttpGet("search/username")]
        [SwaggerOperation(Summary = "Gets the searched user.")]
        [ProducesResponseType(typeof(UserResponseDto), statusCode: 200)]
        [SwaggerResponse(404, "A User with the requested credentials does not exist.")]
        [SwaggerResponse(401, "The user does not have the necessary authorization.")]
        public async Task<IActionResult> SearchByUsername( [FromHeader] string authorizationEmail, [FromHeader] string authorizationPassword, [FromQuery] string username)
        {
            try
            {
                var user = await this.authHelper.TryGetUserAsync(authorizationEmail, authorizationPassword);
                if (user.Role == "Admin" || user.Role == "Moderator")
                {
                    var userResponseDto = await this.userService.SearchByUsername(username);
                    return this.Ok(userResponseDto);
                }

                return this.Unauthorized("You are not authorized.");
            }
            catch (AuthenticationException ex)
            {
                return this.Unauthorized(ex.Message);
            }
        }

        [HttpGet("search/email")]
      
        [SwaggerOperation(Summary = "Gets the searched user.")]
        [ProducesResponseType(typeof(UserResponseDto), statusCode: 200)]
        [SwaggerResponse(404, "A User with the requested credentials does not exist.")]
        [SwaggerResponse(401, "The user does not have the necessary authorization.")]
        public async Task<IActionResult> SearchByEmail( [FromHeader] string authorizationEmail, [FromHeader] string authorizationPassword, [FromQuery] string email)
        {
            try
            {
                var user = await this.authHelper.TryGetUserAsync(authorizationEmail, authorizationPassword);
                if (user.Role == "Admin" || user.Role == "Moderator")
                {
                    var userResponseDto = await this.userService.GetUserByEmailAsync(email);
                    return this.Ok(userResponseDto);
                }

                return this.Unauthorized("You are not authorized.");
            }
            catch (AuthenticationException ex)
            {
                return this.Unauthorized(ex.Message);
            }
        }

        [HttpPatch]
        [Route("ChangeDisplayName")]
        public async Task<IActionResult> ChangeDisplayName(UserUpdateDisplayNameViewModel updateNameViewModel)
        {
            int id = int.Parse(updateNameViewModel.Id);
            var user = await this.userService.GetUserAsync(id);
            if (user.Password != updateNameViewModel.Password)
            {
                return this.Unauthorized();
            }
            await this.userService.UpdateDisplayNameAsync(id, updateNameViewModel.NewDisplayName);
            return this.Ok("IT Worked");

        }

        [HttpPatch]
        [Route("Email")]
        public async Task<IActionResult> Email(UserUpdateEmailModel updateEmailModel)
        {
            int id = int.Parse(updateEmailModel.Id);
            var user = await this.userService.GetUserAsync(id);
            if (user.Password != updateEmailModel.Password)
            {
                return this.Unauthorized();
            }
            await this.userService.UpdateEmailAsync(id, updateEmailModel.NewEmail);
            return this.Ok("IT Worked");
        }

        [HttpPatch]
        [Route("Password")]
        public async Task<IActionResult> Password(UserUpdatePasswordModel updatePasswordModel)
        {
            int id = int.Parse(updatePasswordModel.Id);
            var user = await this.userService.GetUserAsync(id);
            if (user.Password != updatePasswordModel.OldPassword)
            {
                return this.Unauthorized();
            }
            await this.userService.UpdatePasswordAsync(id, updatePasswordModel.NewPassword);
            return this.Ok("IT Worked");
        }

    }
}
