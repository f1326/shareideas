﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Forum.Services.Forum.Services.Contracts;
using Forum.Services.Forum.Services.DTOs;
using Forum.Services.Models;
using Forum.WebApi.Exceptions;
using Forum.WebApi.Helpers;
using Forum.WebApi.Models;
using Forum.WebApi.Models.Contracts;

using Microsoft.AspNetCore.Mvc;

using Swashbuckle.AspNetCore.Annotations;

namespace Forum.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PostsController : ControllerBase
    {
        private readonly IPostService postService;
        private readonly IWebModelMapper webModelMapper;
        private readonly IAuthHelper authHelper;
        private readonly ICommentService commentService;

        public PostsController(IPostService postService, IWebModelMapper webModelMapper, IAuthHelper authHelper, ICommentService commentService)
        {
            this.postService = postService;
            this.webModelMapper = webModelMapper;
            this.authHelper = authHelper;
            this.commentService = commentService;
        }

        [HttpGet("")]
        [SwaggerOperation(Summary = "Gets a list of all posts.")]
        [ProducesResponseType(typeof(IEnumerable<PostResponseDto>), statusCode:200)]
        [SwaggerResponse(404,"A User with the requested credentials does not exist.")]
        [SwaggerResponse(401, "The user does not have the necessary authorization.")]
        public async Task<IActionResult> GetAsync([FromHeader] string authorizationEmail, [FromHeader] string authorizationPassword)
        {
            try
            {
                var user = await this.authHelper.TryGetUserAsync(authorizationEmail, authorizationPassword);
                var posts =  this.postService.GetAll().ToList();

                return this.Ok(posts);
            }
            catch (AuthenticationException ex)
            {
                return this.Unauthorized(ex.Message);
            }
        }

        [HttpGet("{id}")]
        [SwaggerOperation(Summary = "Gets a single post.")]
        [ProducesResponseType(typeof(PostResponseDto), statusCode: 200)]
        [SwaggerResponse(404, "A User with the requested credentials does not exist.")]
        [SwaggerResponse(401, "The user does not have the necessary authorization.")]
        public async Task<IActionResult> GetAsync([FromHeader] string authorizationEmail, [FromHeader] string authorizationPassword, int id)
        {
            try
            {
                var user = await this.authHelper.TryGetUserAsync(authorizationEmail,authorizationPassword);
                var post = await this.postService.GetByIdAsync(id);
                return this.Ok(post);
            }
            catch (AuthenticationException ex)
            {
                return this.Unauthorized(ex.Message);
            }
        }

        
        [HttpPost("")]
        [SwaggerOperation(Summary = "Creates a sigle post and returns a list of all posts.",
            Description = "Need information of post's title, content, userId and forumId.")]
        [ProducesResponseType(typeof(IEnumerable<PostResponseDto>), statusCode: 200)]
        [SwaggerResponse(404, "A user with the requested credentials does not exist.")]
        [SwaggerResponse(401, "The user does not have the necessary authorization.")]
        public async Task<IActionResult> Post([FromHeader] string authorizationEmail, [FromHeader]
                                    string authorizationPassword, PostWebModel postWebModel)
        {
            try
            {
                var user = await this.authHelper.TryGetUserAsync(authorizationEmail, authorizationPassword);
                var postRequestDto = new PostRequestDto
                {
                    Title = postWebModel.Title,
                    Content = postWebModel.Content,
                    UserId = user.Id
                };
                var result = await this.postService.CreateAsync(postRequestDto);
                return this.Ok(result);
            }
            catch (AuthenticationException ex)
            {
                return this.Unauthorized(ex.Message);
            }
        }

        [HttpPut("{id}")]
        [SwaggerOperation(Summary = "Updates a single post and return it.", 
            Description = "Gets post's information about title and content.")]
        [ProducesResponseType(typeof(PostResponseDto), statusCode: 200)]
        [SwaggerResponse(404, "A User with the requested credentials does not exist.")]
        [SwaggerResponse(401, "The user does not have the necessary authorization.")]
        public async Task<IActionResult> PutAsync(  [FromHeader] string authorizationEmail, 
            [FromHeader] string authorizationPassword, int id, [FromBody] PostUpdateWebModel postWebModel)
        {
            if (postWebModel == null)
            {
                return this.BadRequest();
            }
            try
            {
                var user = await this.authHelper.TryGetUserAsync(authorizationEmail, authorizationPassword);
                var currentPost = await this.postService.GetByIdAsync(id);
                var postRequestDto = this.webModelMapper.ToPostRequestDto(postWebModel, user.Id);
                var postResponseDto = await this.postService.EditAsync(id, postRequestDto);
                return this.Ok(postResponseDto);

            }
            catch (AuthenticationException ex)
            {
                return this.Unauthorized(ex.Message);
            }
          
        }

        [HttpDelete("{id}")]
        [SwaggerOperation(Summary = "Deletes a single post.")]
        [ProducesResponseType(typeof(PostResponseDto), statusCode: 204)]
        [SwaggerResponse(404, "A User with the requested credentials does not exist.")]
        [SwaggerResponse(401, "The user does not have the necessary authorization.")]
        public async Task<IActionResult> DeleteAsync( [FromHeader] string authorizationEmail, [FromHeader] string authorizationPassword, int id)
        {
            try
            {
                var user = await this.authHelper.TryGetUserAsync(authorizationEmail, authorizationPassword);
                await this.postService.DeleteAsync(id);
                return this.NoContent();
            }
            catch (AuthenticationException ex)
            {
                return this.Unauthorized(ex.Message);
            }

        }

        [HttpGet("filter")]
        [SwaggerOperation(Summary = "Gets a list of all posts filtered by the concrete types.", 
            Description = "There are two options to filter. You can choose to filter only by one filter type and value or to filter by two types." +
            "Filter type can be: Title, Content, DateCreated, User, ForumTitle, ForumId.")]
        [ProducesResponseType(typeof(IEnumerable<PostResponseDto>), statusCode: 200)]
        [SwaggerResponse(404, "A User with the requested credentials does not exist.")]
        [SwaggerResponse(401, "The user does not have the necessary authorization.")]
        public async Task<IActionResult> GetFilteredPosts([FromHeader] string authorizationEmail, [FromHeader] string authorizationPassword,
         [FromQuery] string firstType, [FromQuery] string firstValue, [FromQuery] string secondType, [FromQuery] string secondValue)
        {
            try
            {
                await this.authHelper.TryGetUserAsync(authorizationEmail, authorizationPassword);
                var posts = await this.postService.GetFilteredPostsAsync(firstType, firstValue, secondType, secondValue);
                return this.Ok(posts);
            }
            catch (AuthenticationException ex)
            {
                return this.Unauthorized(ex.Message);
            }
        
        }

        [HttpGet("sort")]
        [SwaggerOperation(Summary = "Gets a list of all posts sorted by a conrete type.", 
            Description = "Sorting type can be Title, Content, DateCreated, User, ForumTitle, ForumId. Direction of the sort: ascending or descending.")]
        [ProducesResponseType(typeof(IEnumerable<PostResponseDto>), statusCode: 200)]
        [SwaggerResponse(404, "A User with the requested credentials does not exist.")]
        [SwaggerResponse(401, "The user does not have the necessary authorization.")]
        public async Task<IActionResult> GetSortedPosts([FromHeader] string authorizationEmail, [FromHeader] string authorizationPassword, 
            [FromQuery] string type,[FromQuery] string direction)
        {
            try
            {
                await this.authHelper.TryGetUserAsync(authorizationEmail, authorizationPassword);
              
                var posts =  this.postService.GetSortedPosts(type,direction);

                return this.Ok(posts);
            }
            catch (AuthenticationException ex)
            {
                return this.Unauthorized(ex.Message);
            }
          
        }

        [HttpPost("{id}/comments")]
        [SwaggerOperation(Summary = "Creates a new comment for an existing post and returns the post with all comments.",
            Description = "The new comment must have information about its title and content.")]
        [ProducesResponseType(typeof(PostResponseDto), statusCode: 200)]
        [SwaggerResponse(404, "A User with the requested credentials does not exist.")]
        [SwaggerResponse(401, "The user does not have the necessary authorization.")]
        public async Task<IActionResult> AddCommentToPost( [FromHeader] string authorizationEmail, 
            [FromHeader] string authorizationPassword, int id, [FromBody] CommentWebModel commentWebModel )
        {
            try
            {
                var user = await this.authHelper.TryGetUserAsync(authorizationEmail, authorizationPassword);
                var commentRequestDto = this.webModelMapper.ToCommentRequestDto(commentWebModel, id, user.Id);
                var commentResponseDto = await this.commentService.CreateAsync(commentRequestDto);

                return this.Ok(commentResponseDto);
            }
            catch (AuthenticationException ex)
            {
                return this.Unauthorized(ex.Message);
            }
          
        }

        [HttpDelete("comments/{commentId}")]
        [SwaggerOperation(Summary = "Deletes the comment and returns it.")]
        [ProducesResponseType(typeof(PostResponseDto), statusCode: 200)]
        [SwaggerResponse(404, "A User with the requested credentials does not exist.")]
        [SwaggerResponse(401, "The user does not have the necessary authorization.")]
        public async Task<IActionResult> DeleteCommentFromPost( [FromHeader] string authorizationEmail,
         [FromHeader] string authorizationPassword, int commentId)
        {
            try
            {
                var user = await this.authHelper.TryGetUserAsync(authorizationEmail, authorizationPassword);
                var commentResponseDto = await this.commentService.DeleteAsync(commentId);

                return this.Ok(commentResponseDto);
            }
            catch (AuthenticationException ex)
            {
                return this.Unauthorized(ex.Message);
            }
         
        }

        [HttpPut("{id}/comments/{commentId}")]
        [SwaggerOperation(Summary = "Updates a comment and returns it..", 
            Description = "Comment updting inforamation is title and content.")]
        [ProducesResponseType(typeof(PostResponseDto), statusCode: 200)]
        [SwaggerResponse(404, "A User with the requested credentials does not exist.")]
        [SwaggerResponse(401, "The user does not have the necessary authorization.")]
        public async Task<IActionResult> UpdateCommentToPost([FromHeader] string authorizationEmail,
           [FromHeader] string authorizationPassword, int postId, int commentId, [FromBody] CommentWebModel commentWebModel)
        {
            try
            {
                var user = await this.authHelper.TryGetUserAsync(authorizationEmail, authorizationPassword);

                var commentRequestDto = new CommentRequestDto
                {
                    Title = commentWebModel.Title,
                    Content = commentWebModel.Content,
                    PostId = postId,
                    UserId = user.Id
                };
                var commentResponseDto = await this.commentService.UpdateAsync(commentId, commentRequestDto);

                return this.Ok(commentResponseDto);
            }
            catch (AuthenticationException ex)
            {
                return this.Unauthorized(ex.Message);
            }

        }
        
        [HttpGet]   
        [Route("Count")]
        public IActionResult Count()
        {
            var result = postService.GetCount();
            return this.Ok(result);
        }
        [HttpGet("Page")]
        public async Task<IActionResult> Get(int page = 1)
        {
            var result = await postService.GetPaggination(--page * 7, 7);
            return this.Ok(result);
        }

        [HttpPost]
        [Route("Filter/Count")]
        public IActionResult CountFilter(PostsSortingAndFilterModel model)
        {
            var result = postService.GetFilterCount(model.DateStart, model.DateEnd, model.TitleContains);
            return this.Ok(result);
        }
        [HttpPost]
        [Route("{postId}/Like")]
        public async Task<IActionResult> LikePost([FromHeader] string authorizationEmail, [FromHeader] string authorizationPassword
                               , int postId, LikeWebModel webModel)
        {
            try
            {
                var user = await this.authHelper.TryGetUserAsync(authorizationEmail, authorizationPassword);

                var likeRequestDto = new LikeRequestDto
                {
                    UserId = int.Parse(webModel.UserId),
                    PostId = postId
                };
                await this.postService.LikePost(likeRequestDto);
                return this.Ok();
            }
            catch (AuthenticationException ex)
            {
                return this.Unauthorized(ex.Message);
            }
        }

        [HttpDelete]
        [Route("{postId}/Like")]
        public async Task<IActionResult> DislikePost([FromHeader] string authorizationEmail, [FromHeader] string authorizationPassword
                               , int postId, LikeWebModel webModel)
        {

            try
            {
                var user = await this.authHelper.TryGetUserAsync(authorizationEmail, authorizationPassword);

                var likeRequestDto = new LikeRequestDto
                {
                    UserId = int.Parse(webModel.UserId),
                    PostId = postId
                };
                await this.postService.RemoveLike(likeRequestDto);
                return this.Ok();
            }
            catch (AuthenticationException ex)
            {
                return this.Unauthorized(ex.Message);
            }
        }

        [HttpPost]
        [Route("FilterPage")]
        public async Task<IActionResult> GetFiltered(PostsSortingAndFilterModel model, int page = 1)
        {
            var result = await postService.GetFilterPaggination(model.SortingType
                                                             , model.SortingDirection
                                                             , model.DateStart
                                                             , model.DateEnd
                                                             , model.TitleContains
                                                             , --page * 7, 7);
            return this.Ok(result);
        }

    }
}

