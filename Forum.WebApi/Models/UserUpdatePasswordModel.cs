﻿namespace Forum.WebApi.Models
{
    public class UserUpdatePasswordModel
    {
        public string Id { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }
}
