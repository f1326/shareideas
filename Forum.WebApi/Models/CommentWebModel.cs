﻿namespace Forum.WebApi.Models
{
    public class CommentWebModel
    {
        public string Title { get; set; }
        public string Content { get; set; }
    }
}
