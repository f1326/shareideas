﻿namespace Forum.WebApi.Models
{
    public class UserUpdateDisplayNameViewModel
    {
        public string Id { get; set; }
        public string NewDisplayName { get; set; }
        public string Password { get; set; }
    }
}
