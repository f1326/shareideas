﻿using Forum.Services.Models;

namespace Forum.WebApi.Models.Contracts
{
    public interface IWebModelMapper
    {
        UserRequestDto ToUserRequestDto(UserWebModel userWebModel);
        PostRequestDto ToPostRequestDto(PostWebModel postWebModel);
        PostRequestDto ToPostRequestDto(PostUpdateWebModel postWebModel, int userId);
        CommentRequestDto ToCommentRequestDto(CommentWebModel commentWebModel, int postId, int userId);
    }
}
