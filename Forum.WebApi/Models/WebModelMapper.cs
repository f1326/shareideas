﻿
using Forum.Services.Models;
using Forum.WebApi.Models.Contracts;

namespace Forum.WebApi.Models
{
    public class WebModelMapper : IWebModelMapper
    {
        public PostRequestDto ToPostRequestDto(PostWebModel postWebModel)
        {
            var postRequestDto = new PostRequestDto
            { 
            Title = postWebModel.Title,
            Content = postWebModel.Content,
            UserId = postWebModel.UserId
            };

            return postRequestDto;
        }

        public PostRequestDto ToPostRequestDto(PostUpdateWebModel postWebModel, int userId)
        {
            var postRequestDto = new PostRequestDto
            {
                Title = postWebModel.Title,
                Content = postWebModel.Content,
                UserId = userId
            };

            return postRequestDto;
        }

        public UserRequestDto ToUserRequestDto(UserWebModel userWebModel)
        {
            var userRequestDto = new UserRequestDto
            { 
            Username = userWebModel.Username,
            Password = userWebModel.Password,
            DisplayName = userWebModel.DisplayName,
            Email = userWebModel.Email
            };

            return userRequestDto;
        }

        public CommentRequestDto ToCommentRequestDto(CommentWebModel commentWebModel, int postId, int userId)
        {
            var commentRequestDto = new CommentRequestDto
            { 
            Title = commentWebModel.Title,
            Content = commentWebModel.Content,
            UserId = userId,
            PostId = postId
            };

            return commentRequestDto;
        }
    }
}
