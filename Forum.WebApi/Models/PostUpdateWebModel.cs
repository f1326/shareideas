﻿using System.ComponentModel.DataAnnotations;

namespace Forum.WebApi.Models
{
    public class PostUpdateWebModel
    {
        [Required, MinLength(ModelsConstraints.PostTitleMinLength, ErrorMessage = "Please provide title with minimum 2 letters.")]
        public string Title { get; set; }

        [Required, MinLength(ModelsConstraints.PostContentMinLength, ErrorMessage = "Please provide content with minimum 10 letters.")]
        public string Content { get; set; }
    }
}
