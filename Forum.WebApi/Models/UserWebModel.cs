﻿using System.ComponentModel.DataAnnotations;

namespace Forum.WebApi.Models
{
    public class UserWebModel
    {
        [Required, MinLength(ModelsConstraints.NameMinLength, ErrorMessage = "Please provide username with minimum 2 letters."), MaxLength(ModelsConstraints.NameMaxLength, ErrorMessage ="Please provide username with maximum 20 letters.")]
        public string Username { get; set; }

        [Required]
        [RegularExpression(@"^(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,}$")]
        public string Password { get; set; }

        [Required, EmailAddress]
        public string Email { get; set; }

        [Required, MinLength(ModelsConstraints.NameMinLength, ErrorMessage = "Please provide display name with minimum 2 letters."), MaxLength(ModelsConstraints.NameMaxLength, ErrorMessage = "Please provide display name with maximum 20 letters.")]
        public string DisplayName { get; set; }

    }
}
