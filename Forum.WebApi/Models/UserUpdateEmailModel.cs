﻿namespace Forum.WebApi.Models
{
    public class UserUpdateEmailModel
    {
        public string Id { get; set; }
        public string NewEmail { get; set; }
        public string Password { get; set; }
    }
}
