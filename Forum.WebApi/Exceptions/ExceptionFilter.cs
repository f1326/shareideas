﻿using Forum.Services.Exceptions;
using System;
using System.Net;
using System.Net.Http;
using System.Security.Authentication;
using System.Web.Http.Filters;

namespace Forum.Api.Exceptions
{
    public class ExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            //var exceptionType = actionExecutedContext.Exception.GetType();
            //if (exceptionType == typeof(EntityNotFoundException))
            //{
            //    actionExecutedContext.Response = new HttpResponseMessage(HttpStatusCode.NotFound);
            //}
            HttpStatusCode statusCode = HttpStatusCode.InternalServerError;
            string errMsg = String.Empty;
            var exceptionType = actionExecutedContext.Exception.GetType();

            if (exceptionType == typeof(AuthenticationException))
            {
                errMsg = "Unauthorized access";
                statusCode = HttpStatusCode.Unauthorized;
            }
            else if (exceptionType == typeof(EntityNotFoundException))
            {
                errMsg = "Data is not found";
                statusCode = HttpStatusCode.NotFound;
            }
            else
            {
                errMsg = "Comntact to admin";
                statusCode = HttpStatusCode.InternalServerError;
            }
            var response = new HttpResponseMessage(statusCode)
            {
                Content = new StringContent(errMsg),
                ReasonPhrase = "From Exception Filter"
            };
            actionExecutedContext.Response = response;
            base.OnException(actionExecutedContext);
        }
    }
}
