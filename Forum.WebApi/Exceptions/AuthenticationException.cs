﻿using System;

namespace Forum.WebApi.Exceptions
{
    public class AuthenticationException : ApplicationException
    {
        public AuthenticationException(string msg) 
            : base(msg)
        {
        }
    }
}
