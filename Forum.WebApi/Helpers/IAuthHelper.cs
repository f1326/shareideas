﻿using Forum.Services.Forum.Services.DTOs;
using System.Threading.Tasks;

namespace Forum.WebApi.Helpers
{
    public interface IAuthHelper
    {
        Task<UserResponseDto> TryGetUserAsync(string authorizationHeader, string authorizationPassword);
    }
}
