﻿
using Forum.Services.Exceptions;
using Forum.Services.Forum.Services.Contracts;
using Forum.Services.Forum.Services.DTOs;
using Forum.WebApi.Exceptions;
using System.Threading.Tasks;

namespace Forum.WebApi.Helpers
{
    public class AuthHelper: IAuthHelper
    {
        private readonly IUserService userService;
        public AuthHelper(IUserService userService)
        {
            this.userService = userService;
        }

        //  authorizationHeader = email,password
        public async Task<UserResponseDto> TryGetUserAsync(string authorizationEmail,string  authorizationPassword)
        {
            if (authorizationEmail == null || authorizationPassword == null)
            {
                throw new AuthenticationException("Please provide email and password.");
            }
            
            try
            {
                var user = await this.userService.GetUserByEmailAsync(authorizationEmail);
                if (user.Password != authorizationPassword)
                {
                    throw new AuthenticationException("Password is not correct.");
                }

                return user;
            }
            catch (EntityNotFoundException ex)
            {
                throw new EntityNotFoundException(ex.Message);
            }
        }
    }
}
