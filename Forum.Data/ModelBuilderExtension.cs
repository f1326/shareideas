﻿using Forum.Data.Models;

using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;

namespace Forum.Data
{
    public static class ModelBuilderExtension
    {
        static ModelBuilderExtension()
        {
            Users = new List<User>()
            {
                new User
                {
                    Id= 1,
                    Username = "georgi_admin",
                    Password ="Password123@",
                    Email ="georgi@telerik.com",
                    DisplayName="ADMIN-G",
                    MemberSince = DateTime.ParseExact("2020-05-05 22:12 PM", "yyyy-MM-dd HH:mm tt", System.Globalization.CultureInfo.InvariantCulture),
                    Role = Models.Enums.UserRole.Admin,
                    IsActive = true,
                    Rating = 100,
                    ProfileImage = "/images/userDefault.png"
                },
                new User
                {
                    Id= 2,
                    Username = "deyana",
                    Password ="Password123@",
                    Email ="deyana@telerik.com",
                    DisplayName="ADMIN-D",
                    MemberSince = DateTime.ParseExact("2020-05-05 22:12 PM", "yyyy-MM-dd HH:mm tt", System.Globalization.CultureInfo.InvariantCulture),
                    Role = Models.Enums.UserRole.Admin,
                    IsActive = true,
                    Rating = 200,
                    ProfileImage = "/images/userDefault.png"
                },
                new User
                {
                    Id= 3,
                    Username = "testModerator1",
                    Password ="Password123@",
                    Email ="moderator1@telerik.com",
                    DisplayName="MODERATOR-1",
                    MemberSince = DateTime.ParseExact("2020-05-05 22:12 PM", "yyyy-MM-dd HH:mm tt", System.Globalization.CultureInfo.InvariantCulture),
                    Role = Models.Enums.UserRole.Moderator,
                    IsActive = true,
                    Rating = 200,
                    ProfileImage = "/images/userDefault.png"
                },
                new User
                {
                    Id= 4,
                    Username = "testModerator2",
                    Password ="Password123@",
                    Email ="moderator2@telerik.com",
                    DisplayName="MODERATOR-2",
                    MemberSince = DateTime.ParseExact("2020-05-05 22:12 PM", "yyyy-MM-dd HH:mm tt", System.Globalization.CultureInfo.InvariantCulture),
                    Role = Models.Enums.UserRole.Moderator,
                    IsActive = false,
                    Rating = 200,
                    ProfileImage = "/images/userDefault.png"
                },
                new User
                {
                    Id= 5,
                    Username = "testUser",
                    Password ="Password123@",
                    Email ="user1@telerik.com",
                    DisplayName="USER-TEST",
                    MemberSince = DateTime.ParseExact("2020-05-05 22:12 PM", "yyyy-MM-dd HH:mm tt", System.Globalization.CultureInfo.InvariantCulture),
                    Role = Models.Enums.UserRole.Moderator,
                    IsActive = true,
                    Rating = 200,
                    ProfileImage = "/images/userDefault.png"
                },
            };
            Posts = new List<Post>()
            {
                new Post
                {
                    Id =1,
                    Title = "How to get the last index of an array?",
                    Content = "How to get the last index of an array in C#?",
                    UserId = 1,
                    CreatedAt= DateTime.ParseExact("2020-11-01 16:12 PM", "yyyy-MM-dd HH:mm tt", System.Globalization.CultureInfo.InvariantCulture)
                },
                new Post
                {
                    Id =2,
                    Title = "How to enable CORS in ASP.net Core WebAPI",
                    Content = "I have a backend ASP.Net Core Web API hosted on an Azure Free Plan (Source Code: code also have a Client Website which I want to make consume that API. The Client Application will not be hosted on Azure, but rather will be hosted on Github Pages or on another Web Hosting Service that I have access to. Because of this the domain names won't line up.Looking into this, I need to enable CORS on the Web API side, however I have tried just about everything for several hours now and it is refusing to work.",
                    UserId = 4,
                    CreatedAt= DateTime.ParseExact("2020-12-01 16:12 PM", "yyyy-MM-dd HH:mm tt", System.Globalization.CultureInfo.InvariantCulture)

                },
                new Post
                { 
                    Id =3,
                    Title = "How to horizontally center an element?",
                    Content = "How can I horizontally center a DIV within another DIV using CSS?",
                    UserId = 2,
                    CreatedAt= DateTime.ParseExact("2020-12-01 16:12 PM", "yyyy-MM-dd HH:mm tt", System.Globalization.CultureInfo.InvariantCulture)

                },
            };
            Comments = new List<Comment>()
            {
                new Comment
                {
                    Id =1,
                    Title="If you don't want to set a fixed width on the inner div you could do something like this:",
                    Content = "#outer {width: 100%;text - align: center;}# inner {display: inline-block;}",
                    CreatedAt =  DateTime.ParseExact("2021-10-08 16:12 PM", "yyyy-MM-dd HH:mm tt", System.Globalization.CultureInfo.InvariantCulture),
                    UserId = 3,
                    PostId =3
                },
                new Comment
                {
                    Id=2,
                    Title="You can apply this CSS to the inner <div>:",
                    Content = "#inner {width: 50%; margin: 0 auto;}",
                    CreatedAt =  DateTime.ParseExact("2021-11-01 16:12 PM", "yyyy-MM-dd HH:mm tt", System.Globalization.CultureInfo.InvariantCulture),
                    UserId = 1,
                    PostId =3
                }
            };
            Likes = new List<Like>()
            {
                new Like
                {
                    Id = 1,
                    PostId = 1,
                    UserId = 2
                },
                new Like
                {
                    Id = 2,
                    PostId = 2,
                    UserId = 1
                },
                new Like
                {
                    Id = 3,
                    PostId = 2,
                    UserId = 3
                },
                new Like
                {
                    Id = 4,
                    PostId = 2,
                    UserId = 4
                },
            };
        }

       
        public static IEnumerable<Post> Posts { get; }
        public static IEnumerable<User> Users { get; }
        public static IEnumerable<Comment> Comments { get; }
        public static IEnumerable<Like> Likes { get; }
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Post>().HasData(Posts);
            modelBuilder.Entity<User>().HasData(Users);
            modelBuilder.Entity<Comment>().HasData(Comments);
            modelBuilder.Entity<Like>().HasData(Likes);
        }
    }
}
