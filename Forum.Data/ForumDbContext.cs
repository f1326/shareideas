﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Forum.Data.Configurations;
using Forum.Data.Models;

using Microsoft.EntityFrameworkCore;

namespace Forum.Data
{
    public class ForumDbContext : DbContext
    {
        public ForumDbContext(DbContextOptions<ForumDbContext> options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Like> Likes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new PostConfiguration());

            modelBuilder.Entity<User>().HasQueryFilter(a => EF.Property<bool>(a, "IsDeleted") == false);
            modelBuilder.Entity<Post>().HasQueryFilter(a => EF.Property<bool>(a, "IsDeleted") == false);
            modelBuilder.Entity<Comment>().HasQueryFilter(a => EF.Property<bool>(a, "IsDeleted") == false);
            modelBuilder.Entity<Like>().HasQueryFilter(a => EF.Property<bool>(a, "IsDeleted") == false);

            var entityTypes = modelBuilder.Model.GetEntityTypes().ToList();

            var foreignKeys = entityTypes
             .SelectMany(e => e.GetForeignKeys().Where(f => f.DeleteBehavior == DeleteBehavior.Cascade));

            foreach (var foreignKey in foreignKeys)
            {
                foreignKey.DeleteBehavior = DeleteBehavior.Restrict;
            }

            base.OnModelCreating(modelBuilder);

            modelBuilder.Seed();
        }

        public override int SaveChanges()
        {
            this.UpdateSoftDeleteStatuses();
            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default)
        {
            this.UpdateSoftDeleteStatuses();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        private void UpdateSoftDeleteStatuses()
        {
            foreach (var entry in ChangeTracker.Entries())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.CurrentValues["IsDeleted"] = false;
                        break;
                    case EntityState.Deleted:
                        entry.State = EntityState.Modified;
                        entry.CurrentValues["IsDeleted"] = true;
                        break;
                }
            }
        }
    }
}
