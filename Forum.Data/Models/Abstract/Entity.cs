﻿using System.ComponentModel.DataAnnotations;

namespace Forum.Data.Models.Abstract
{
    public abstract  class Entity
    {
        [Key]
        public int Id { get; set; }

        public bool IsDeleted { get; set; }
    }
}
