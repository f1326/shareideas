﻿using Forum.Data.Models.Abstract;
using Forum.Data.Models.Common;
using Forum.Data.Models.Enums;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Forum.Data.Models
{
    public class User : Entity
    {
        [Required, MinLength(DataModelsConstants.NameMinLength), MaxLength(DataModelsConstants.NameMaxLength)]
        public string Username { get; set; }

        [Required]
        [RegularExpression(@"^(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,}$")]
        public string Password { get; set; }

        [Required, EmailAddress]
        public string Email { get; set; }

        [Required, MinLength(DataModelsConstants.NameMinLength), MaxLength(DataModelsConstants.NameMaxLength)]
        public string DisplayName { get; set; }

        public int Rating { get; set; }
        public string ProfileImage { get; set; }
        public DateTime MemberSince { get; set; }
        public bool IsActive { get; set; }
        public string ActivationCode { get; set; }
        public UserRole Role { get; set; }
        public ICollection<Post> Posts { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public ICollection<Like> Likes { get; set; }
        public bool Blocked { get; set; }

    }
}
