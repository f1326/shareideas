﻿namespace Forum.Data.Models.Common
{
    public static class DataModelsConstants
    {
        public const int NameMinLength = 2;
        public const int NameMaxLength = 20;

        public const int PostTitleMinLength = 2;
        public const int PostContentMinLength = 10;
    }
}
