﻿using Forum.Data.Models.Abstract;
using Forum.Data.Models.Common;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Forum.Data.Models
{
    public class Post : Entity
    {
        [Required,MinLength(DataModelsConstants.PostTitleMinLength)]
        public string Title { get; set; }

        [Required, MinLength(DataModelsConstants.PostContentMinLength)]
        public string Content { get; set; }
        public DateTime CreatedAt { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public ICollection<Like> Likes { get; set; }
        public ICollection<Comment> Comments { get; set; }
    }
}
