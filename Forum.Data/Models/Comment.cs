﻿using Forum.Data.Models.Abstract;

using System;

namespace Forum.Data.Models
{
    public class Comment :Entity
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime CreatedAt { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }

        public int PostId { get; set; }
        public Post Post { get; set; }
    }
}
