﻿namespace Forum.Data.Models.Enums
{
    public enum UserRole
    {
        Member = 1,
        Moderator = 2,
        Admin = 3
    }
}
