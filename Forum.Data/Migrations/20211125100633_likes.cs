﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Forum.Data.Migrations
{
    public partial class likes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Liked",
                table: "Likes");

            migrationBuilder.InsertData(
                table: "Likes",
                columns: new[] { "Id", "IsDeleted", "PostId", "UserId" },
                values: new object[,]
                {
                    { 1, false, 1, 2 },
                    { 2, false, 2, 1 },
                    { 3, false, 2, 3 },
                    { 4, false, 2, 4 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Likes",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Likes",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Likes",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Likes",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.AddColumn<bool>(
                name: "Liked",
                table: "Likes",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
