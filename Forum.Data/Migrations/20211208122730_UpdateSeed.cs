﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Forum.Data.Migrations
{
    public partial class UpdateSeed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Content", "Title" },
                values: new object[] { "#outer {width: 100%;text - align: center;}# inner {display: inline-block;}", "If you don't want to set a fixed width on the inner div you could do something like this:" });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Content", "Title" },
                values: new object[] { "#inner {width: 50%; margin: 0 auto;}", "You can apply this CSS to the inner <div>:" });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Content", "Title" },
                values: new object[] { "", "How to get the last index of an array?" });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Content", "Title" },
                values: new object[] { "I have a backend ASP.Net Core Web API hosted on an Azure Free Plan (Source Code: code also have a Client Website which I want to make consume that API. The Client Application will not be hosted on Azure, but rather will be hosted on Github Pages or on another Web Hosting Service that I have access to. Because of this the domain names won't line up.Looking into this, I need to enable CORS on the Web API side, however I have tried just about everything for several hours now and it is refusing to work.", "How to enable CORS in ASP.net Core WebAPI" });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Content", "Title" },
                values: new object[] { "How can I horizontally center a <div> within another <div> using CSS?", "How to horizontally center an element?" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Content", "Title" },
                values: new object[] { "Ihad the 2 shots of the vaciine and im feeling well :)", "I think it is!" });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Content", "Title" },
                values: new object[] { "After the first shot of the vaccine i felt lose in my head also i heard frind of my wifr got pregnat from the vaccine.", "Strongly disagree" });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Content", "Title" },
                values: new object[] { "Bryant was behind Michael Jordan (No. 1), LeBron James (No. 2), Kareem Abdul-Jabbar (No. 3), Bill Russell (No. 4), Magic Johnson (No. 5), Wilt Chamberlain (No. 6), Shaquille O'Neal (No. 7), Larry Bird (No. 8) and Tim Duncan (No. 9).First of all, whoever voted and put Kobe [at 10] just flat out disrespected him.", "Kobe Bryan the best player of 21'st" });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Content", "Title" },
                values: new object[] { "Bryant was behind Michael Jordan (No. 1), LeBron James (No. 2), Kareem Abdul-Jabbar (No. 3), Bill Russell (No. 4), Magic Johnson (No. 5), Wilt Chamberlain (No. 6), Shaquille O'Neal (No. 7), Larry Bird (No. 8) and Tim Duncan (No. 9).First of all, whoever voted and put Kobe [at 10] just flat out disrespected him.", "Lakers first lost in season 2021" });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Content", "Title" },
                values: new object[] { "Based on evidence from clinical trials in people 16 years and older, the Pfizer-BioNTech (COMIRNATY) vaccine was 95% effective at preventing laboratory-confirmed infection with the virus that causes COVID-19 in people who received two doses and had no evidence of being previously infected.", "Is Pfizer the best vaccine ?" });
        }
    }
}
