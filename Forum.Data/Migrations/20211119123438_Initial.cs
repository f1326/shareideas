﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Forum.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Username = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    DisplayName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    Rating = table.Column<int>(type: "int", nullable: false),
                    ProfileImageUrl = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MemberSince = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    ActivationCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Role = table.Column<int>(type: "int", nullable: false),
                    Blocked = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Forums",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    OwnerId = table.Column<int>(type: "int", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Forums", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Forums_Users_OwnerId",
                        column: x => x.OwnerId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Posts",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Content = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    ForumId = table.Column<int>(type: "int", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Posts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Posts_Forums_ForumId",
                        column: x => x.ForumId,
                        principalTable: "Forums",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Posts_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Comments",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Content = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    PostId = table.Column<int>(type: "int", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Comments_Posts_PostId",
                        column: x => x.PostId,
                        principalTable: "Posts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Comments_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Likes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PostId = table.Column<int>(type: "int", nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    Liked = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Likes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Likes_Posts_PostId",
                        column: x => x.PostId,
                        principalTable: "Posts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Likes_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "ActivationCode", "Blocked", "DisplayName", "Email", "IsActive", "IsDeleted", "MemberSince", "Password", "ProfileImageUrl", "Rating", "Role", "Username" },
                values: new object[,]
                {
                    { 1, null, false, "ADMIN-G", "georgi@telerik.com", true, false, new DateTime(2020, 5, 5, 22, 12, 0, 0, DateTimeKind.Unspecified), "Password123@", "default", 100, 3, "georgi_admin" },
                    { 2, null, false, "ADMIN-D", "deyana@telerik.com", true, false, new DateTime(2020, 5, 5, 22, 12, 0, 0, DateTimeKind.Unspecified), "Password123@", "default", 200, 3, "deyana" },
                    { 3, null, false, "MODERATOR-1", "moderator1@telerik.com", true, false, new DateTime(2020, 5, 5, 22, 12, 0, 0, DateTimeKind.Unspecified), "Password123@", "default", 200, 2, "testModerator1" },
                    { 4, null, false, "MODERATOR-2", "moderator2@telerik.com", false, false, new DateTime(2020, 5, 5, 22, 12, 0, 0, DateTimeKind.Unspecified), "Password123@", "default", 200, 2, "testModerator2" },
                    { 5, null, false, "USER-TEST", "moderator2@telerik.com", true, false, new DateTime(2020, 5, 5, 22, 12, 0, 0, DateTimeKind.Unspecified), "Password123@", "default", 200, 2, "testUser" }
                });

            migrationBuilder.InsertData(
                table: "Forums",
                columns: new[] { "Id", "CreatedAt", "Description", "IsDeleted", "OwnerId", "Title" },
                values: new object[] { 1, new DateTime(2020, 1, 1, 16, 12, 0, 0, DateTimeKind.Unspecified), "Most recent mathes discusion. PLayes stats and much more!", false, 1, "All about Basketball" });

            migrationBuilder.InsertData(
                table: "Forums",
                columns: new[] { "Id", "CreatedAt", "Description", "IsDeleted", "OwnerId", "Title" },
                values: new object[] { 2, new DateTime(2019, 5, 6, 12, 12, 0, 0, DateTimeKind.Unspecified), "What is Linq and the best way to use it.", false, 2, "LINQ" });

            migrationBuilder.InsertData(
                table: "Forums",
                columns: new[] { "Id", "CreatedAt", "Description", "IsDeleted", "OwnerId", "Title" },
                values: new object[] { 3, new DateTime(2021, 3, 3, 12, 12, 0, 0, DateTimeKind.Unspecified), "LEading news about the COVID pandemic!", false, 5, "COVID" });

            migrationBuilder.InsertData(
                table: "Posts",
                columns: new[] { "Id", "Content", "CreatedAt", "ForumId", "IsDeleted", "Title", "UserId" },
                values: new object[] { 1, "Bryant was behind Michael Jordan (No. 1), LeBron James (No. 2), Kareem Abdul-Jabbar (No. 3), Bill Russell (No. 4), Magic Johnson (No. 5), Wilt Chamberlain (No. 6), Shaquille O'Neal (No. 7), Larry Bird (No. 8) and Tim Duncan (No. 9).First of all, whoever voted and put Kobe [at 10] just flat out disrespected him.", new DateTime(2020, 11, 1, 16, 12, 0, 0, DateTimeKind.Unspecified), 1, false, "Kobe Bryan the best player of 21'st", 1 });

            migrationBuilder.InsertData(
                table: "Posts",
                columns: new[] { "Id", "Content", "CreatedAt", "ForumId", "IsDeleted", "Title", "UserId" },
                values: new object[] { 2, "Bryant was behind Michael Jordan (No. 1), LeBron James (No. 2), Kareem Abdul-Jabbar (No. 3), Bill Russell (No. 4), Magic Johnson (No. 5), Wilt Chamberlain (No. 6), Shaquille O'Neal (No. 7), Larry Bird (No. 8) and Tim Duncan (No. 9).First of all, whoever voted and put Kobe [at 10] just flat out disrespected him.", new DateTime(2020, 12, 1, 16, 12, 0, 0, DateTimeKind.Unspecified), 1, false, "Lakers first lost in season 2021", 4 });

            migrationBuilder.InsertData(
                table: "Posts",
                columns: new[] { "Id", "Content", "CreatedAt", "ForumId", "IsDeleted", "Title", "UserId" },
                values: new object[] { 3, "Based on evidence from clinical trials in people 16 years and older, the Pfizer-BioNTech (COMIRNATY) vaccine was 95% effective at preventing laboratory-confirmed infection with the virus that causes COVID-19 in people who received two doses and had no evidence of being previously infected.", new DateTime(2020, 12, 1, 16, 12, 0, 0, DateTimeKind.Unspecified), 3, false, "Is Pfizer the best vaccine ?", 2 });

            migrationBuilder.InsertData(
                table: "Comments",
                columns: new[] { "Id", "Content", "CreatedAt", "IsDeleted", "PostId", "Title", "UserId" },
                values: new object[] { 1, "Ihad the 2 shots of the vaciine and im feeling well :)", new DateTime(2021, 10, 8, 16, 12, 0, 0, DateTimeKind.Unspecified), false, 3, "I think it is!", 3 });

            migrationBuilder.InsertData(
                table: "Comments",
                columns: new[] { "Id", "Content", "CreatedAt", "IsDeleted", "PostId", "Title", "UserId" },
                values: new object[] { 2, "After the first shot of the vaccine i felt lose in my head also i heard frind of my wifr got pregnat from the vaccine.", new DateTime(2021, 11, 1, 16, 12, 0, 0, DateTimeKind.Unspecified), false, 3, "Strongly disagree", 1 });

            migrationBuilder.CreateIndex(
                name: "IX_Comments_PostId",
                table: "Comments",
                column: "PostId");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_UserId",
                table: "Comments",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Forums_OwnerId",
                table: "Forums",
                column: "OwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_Likes_PostId",
                table: "Likes",
                column: "PostId");

            migrationBuilder.CreateIndex(
                name: "IX_Likes_UserId",
                table: "Likes",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Posts_ForumId",
                table: "Posts",
                column: "ForumId");

            migrationBuilder.CreateIndex(
                name: "IX_Posts_UserId",
                table: "Posts",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Comments");

            migrationBuilder.DropTable(
                name: "Likes");

            migrationBuilder.DropTable(
                name: "Posts");

            migrationBuilder.DropTable(
                name: "Forums");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
