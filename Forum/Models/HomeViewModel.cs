﻿using Forum.Models.PostViewModels;
using System.Collections.Generic;

namespace Forum.Models
{
    public class HomeViewModel
    {
        public int NumberCreatedPosts { get; set; }
        public int NumberUsers { get; set; }
        public IEnumerable<PostResponseViewModel> latestPostsViewModel { get; set; }
        public IEnumerable<PostResponseViewModel> topMostCommentedPostsViewModel { get; set; }
    }
}
