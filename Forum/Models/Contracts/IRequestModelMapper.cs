﻿using Forum.Services.Models;

namespace Forum.Models.Contracts
{
    public interface IRequestModelMapper
    {
        UserRequestDto ToUserRequestDto(RegisterViewModel registerViewModel);
        UserUpdateProfilePictureRequestDto ToUserUpdateProfilePictureRequestDto(UserUpdateProfilePictureModel userUpratePictureModel);
    }
}
