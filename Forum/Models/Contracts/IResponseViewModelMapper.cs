﻿using Forum.Models.PostViewModels;
using Forum.Models.UserViewModels;
using Forum.Services.Forum.Services.DTOs;

namespace Forum.Models.Contracts
{
    public interface IResponseViewModelMapper
    {
        UserRegistrationViewModel UserRegistrationViewModel(UserRegistrationDto userRegistrationDto);
        UserResponseViewModel ToUserResponseViewModel(UserResponseDto userResponseDto);
        PostResponseViewModel ToPostResponseViewModel(PostResponseDto posrtResponseDto);
    }
}
