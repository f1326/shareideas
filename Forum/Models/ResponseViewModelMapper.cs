﻿using Forum.Models.Contracts;
using Forum.Models.PostViewModels;
using Forum.Models.UserViewModels;
using Forum.Services.Forum.Services.DTOs;
using System.Linq;

namespace Forum.Models
{
    public class ResponseViewModelMapper : IResponseViewModelMapper
    {
        public UserRegistrationViewModel UserRegistrationViewModel(UserRegistrationDto userRegistrationDto)
        {
            return new UserRegistrationViewModel
            { 
            Message = userRegistrationDto.Message, 
            Successfull = userRegistrationDto.Successfull, 
            Username = userRegistrationDto.Username, 
            DisplayName = userRegistrationDto.DisplayName, 
            Email = userRegistrationDto.Email            
            };
        }

        public UserResponseViewModel ToUserResponseViewModel(UserResponseDto userResponseDto)
        {
            return new UserResponseViewModel(userResponseDto);
        }

        public PostResponseViewModel ToPostResponseViewModel(PostResponseDto posrtResponseDto)
        {
            return new PostResponseViewModel
            { 
            Title = posrtResponseDto.Title,
            Content = posrtResponseDto.Content,
            Id = posrtResponseDto.Id,
            DateCreated = posrtResponseDto.DateCreated,
            Likes = posrtResponseDto.Likes.Count(),
            User = posrtResponseDto.User
            };
        }
    }
}
