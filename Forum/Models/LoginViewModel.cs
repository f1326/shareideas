﻿using System.ComponentModel.DataAnnotations;

namespace Forum.Models
{
    public class LoginViewModel
    {
        [Required(ErrorMessage ="Please provide an email."), EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage ="Please provide a  password.")]
        //[RegularExpression(@"^(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,}$", 
        //    ErrorMessage = "Password must be at least 8 symbols and must contain capital letter, digit and special symbol.")]
        public string Password { get; set; }
    }
}
