﻿using Forum.Models.Common;
using System.ComponentModel.DataAnnotations;

namespace Forum.Models
{
    public class RegisterViewModel : LoginViewModel
    {
        [Required(ErrorMessage ="Please confirm the password.")]
        [RegularExpression(@"^(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,}$",
            ErrorMessage = "Password must be at least 8 symbols and must contain capital letter, digit and special symbol.")]
        [Display(Name = "Confirm password")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Please provide an username."),
            MinLength(ModelsConstraints.NameMinLength), MaxLength(ModelsConstraints.NameMaxLength)]
        public string Username { get; set; }

        [Required(ErrorMessage ="Please provide a display name")]
        [MinLength(ModelsConstraints.NameMinLength), MaxLength(ModelsConstraints.NameMaxLength)]
        [Display(Name ="Display name")]
        public string DisplayName { get; set; }
    }
}
