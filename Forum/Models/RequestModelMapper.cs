﻿using Forum.Models.Contracts;
using Forum.Services.Models;

namespace Forum.Models
{
    public class RequestModelMapper : IRequestModelMapper
    {
        public UserRequestDto ToUserRequestDto(RegisterViewModel registerViewModel)
        {
            return new UserRequestDto
            { 
            Username = registerViewModel.Username, 
            Password = registerViewModel.Password, 
            Email = registerViewModel.Email, 
            DisplayName = registerViewModel.DisplayName
            };
        }

        public UserUpdateProfilePictureRequestDto ToUserUpdateProfilePictureRequestDto(UserUpdateProfilePictureModel userUpratePictureModel)
        {
            return new UserUpdateProfilePictureRequestDto
            {
                Id = userUpratePictureModel.UserId,
                Password = userUpratePictureModel.Password,
                Image = userUpratePictureModel.Image            
            };
        }
    }
}
