﻿using Forum.Data.Models;
using System.Collections.Generic;
using System.Linq;

namespace Forum.Models
{
    public class PostViewModel
    {
        public PostViewModel(Post post)
        {
            this.Id = post.Id;
            this.Title = post.Title;
            this.Description = post.Content;
            this.OwnerId = post.User.Id;
            this.OwnerName = post.User.DisplayName;
            this.OwnerPicture = post.User.ProfileImage;
            this.DateCreated = post.CreatedAt.ToString();
            this.OwnerRating = post.User.Rating;
            this.IsAdmin = post.User.Role.ToString().ToLower() == "admin";
            this.Comments = post.Comments?.Select(c => new CommentViewModel(c)).ToList();
            this.Likes = new List<string>();
        }
       
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int OwnerId { get; set; }
        public string OwnerName { get; set; }
        public string OwnerPicture { get; set; }
        public string DateCreated { get; set; }
        public int OwnerRating { get; set; }
        public bool IsAdmin { get; set; }
        public ICollection<CommentViewModel> Comments { get; set; }
        public ICollection<string> Likes { get; set; }
    }
}
