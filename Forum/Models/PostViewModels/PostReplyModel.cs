﻿
namespace Forum.Models
{
    public class PostReplyModel
    {
        public string Title { get; set; }
        public string Content { get; set; }
    }
}
