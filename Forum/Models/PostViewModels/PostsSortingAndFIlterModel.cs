﻿namespace Forum.Models
{
    public class PostsSortingAndFIlterModel
    {
        public string SortingType { get; set; }
        public string SortingDirection { get; set; }
        public string DateStart { get; set; }
        public string DateEnd { get; set; }
        public string TitleContains { get; set; }
    }
}
