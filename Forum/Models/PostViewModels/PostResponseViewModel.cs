﻿using System;

namespace Forum.Models.PostViewModels
{
    public class PostResponseViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime DateCreated { get; set; }
        public int Likes { get; set; }
        public string User { get; set; }
    }
}
