﻿namespace Forum.Models
{
    public class PaginationViewModel
    {
        public int Skip { get; set; }
        public int Take { get; set; }
        public int PageNumber { get; set; }
        public string Rating { get; set; }
        public string Direction { get; set; }
        public string UserStatus { get; set; }
        public string SortType { get; set; }

        public string SearchValue { get; set; }

    }
}
