﻿using Forum.Models.Common;
using System.Collections.Generic;

namespace Forum.Models.UserViewModels
{
    public class UsersListResponseViewModel : PaginatedViewModel
    {
        public IEnumerable<UserResponseViewModel> UsersList { get; set; }
    }
}
