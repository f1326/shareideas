﻿namespace Forum.Models
{
    public class UserRegistrationViewModel
    {
        public string Username { get; set; }
        public string DisplayName { get; set; }
        public string Email { get; set; }
        public string Message { get; set; }
        public bool Successfull { get; set; }
    }
}
