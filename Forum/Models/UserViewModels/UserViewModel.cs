﻿using Microsoft.AspNetCore.Http;

namespace Forum.Models
{
    public class UserViewModel
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Role { get; set; }
        public string Email { get; set; }
        public string DisplayName { get; set; }
        public string MemberSince { get; set; }
        public int Rating { get; set; }
        public string ProfileImagePath { get; set; }
        public string NewDisplayName { get; set; }
        public string Password { get; set; }

        public IFormFile NewImage { get; set; }
        // public UserUpdateDisplayNameViewModel DisplayNameModel { get; set; }
    }
}
