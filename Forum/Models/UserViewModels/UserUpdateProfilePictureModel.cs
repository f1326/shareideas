﻿using Microsoft.AspNetCore.Http;

namespace Forum.Models
{
    public class UserUpdateProfilePictureModel
    {
        public int UserId { get; set; }
        public string Password { get; set; }
        public IFormFile Image { get; set; }
    }
}
