﻿namespace Forum.Models
{
    public class ActivationViewModel : LoginViewModel
    {
        public string Message { get; set; }
    }
}
