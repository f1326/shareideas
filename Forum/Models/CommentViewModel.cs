﻿using Forum.Data.Models;

namespace Forum.Models
{
    public class CommentViewModel
    {
        public CommentViewModel(Comment comment)
        {
            this.Id = comment.Id;
            this.OwnerId = comment.User.Id;
            this.OwnerName = comment.User.DisplayName;
            this.OwnerProfilePicture = comment.User.ProfileImage;
            this.OnwerRating = comment.User.Rating;
            this.Title = comment.Title;
            this.Content = comment.Content;
            this.IsAdmin = comment.User.Role.ToString().ToLower() == "admin";
            this.DateCreated = comment.CreatedAt.ToString();
        }
        public int Id { get; set; }
        public int OwnerId { get; set; }
        public string OwnerName { get; set; } 
        public string OwnerProfilePicture { get; set; } 

        public int OnwerRating { get; set; } 
        public string Title { get; set; }
        public string Content { get; set; }
        public bool IsAdmin { get; set; }
        public string DateCreated { get; set; }
    }
}
