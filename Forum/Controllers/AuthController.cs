﻿using System;
using System.Threading.Tasks;

using Forum.Exceptions;
using Forum.Helpers;
using Forum.Models;
using Forum.Models.Contracts;
using Forum.Services.Exceptions;
using Forum.Services.Forum.Services.Contracts;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Forum.Controllers
{
    public class AuthController : Controller
    {
        private readonly IUserService userService;
        private readonly IAuthenticationHelper authHelper;
        private readonly IRequestModelMapper modelMapper;
        private readonly IResponseViewModelMapper responseModelMapper;
        public AuthController(IUserService userService, IAuthenticationHelper authHelper, IRequestModelMapper modelMapper, IResponseViewModelMapper responseModelMapper)
        {
            this.userService = userService;
            this.authHelper = authHelper;
            this.modelMapper = modelMapper;
            this.responseModelMapper = responseModelMapper;
        }

        //GET: /auth/login
        public IActionResult Login()
        {
            var loginViewModel = new LoginViewModel();

            return this.View(loginViewModel);
        }

        //POST: /auth/login
        [HttpPost]
        public async Task<IActionResult> Login([Bind("Email, Password")] LoginViewModel loginViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(loginViewModel);
            }

            try
            {
                var role = HttpContext.Session.GetString("Role");
                var user = await this.authHelper.TryGetUser(loginViewModel.Email, loginViewModel.Password);
                this.HttpContext.Session.SetInt32("CurrentUserId", user.Id);
                this.HttpContext.Session.SetString("CurrentUser", user.Username);
                this.HttpContext.Session.SetString("CurrentUserEmail", user.Email);
                this.HttpContext.Session.SetString("CurrentUserPassword", user.Password);
                this.HttpContext.Session.SetString("Role", user.Role);
                
                return this.RedirectToAction("Index", "Home");
            }
            catch (AuthenticationException ex)
            {
                this.ModelState.AddModelError("Password", ex.Message);
                return this.View(loginViewModel);
            }
            catch (EntityNotFoundException ex)
            {
                this.ModelState.AddModelError("Email", ex.Message);
                return this.View(loginViewModel);
            }

        }

        //GET: /auth/logout
        public IActionResult Logout()
        {
            this.HttpContext.Session.Remove("CurrentUser");
            this.HttpContext.Session.Remove("Role");
            this.HttpContext.Session.Remove("CurrentUserId");
            this.HttpContext.Session.Remove("CurrentUserEmail");
            this.HttpContext.Session.Remove("CurrentUserPassword");

            return this.RedirectToAction("Index", "Home");
        }

        //GET: /auth/register
        public IActionResult Register()
        {
            var registerViewModel = new RegisterViewModel();

            return this.View(registerViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> Register([Bind("Username, Password, ConfirmPassword, Email, DisplayName")] RegisterViewModel registerViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(registerViewModel);
            }

            var userExists = await this.userService.ExistUserWithEmailAsync(registerViewModel.Email);
            if (userExists)
            {
                this.ModelState.AddModelError("Email", "User with same email already exists.");
                return this.View(registerViewModel);
            }

             userExists = await this.userService.ExistUserWithUsernameAsync(registerViewModel.Username);
            if (userExists)
            {
                this.ModelState.AddModelError("Usernme", "User with same username already exists.");
                return this.View(registerViewModel);
            }
            if (!registerViewModel.Password.Equals(registerViewModel.ConfirmPassword))
            {
                this.ModelState.AddModelError("ConfirmPassword", "Confirm password should match password.");
                return this.View(registerViewModel);
            }

            var user = this.modelMapper.ToUserRequestDto(registerViewModel);
            var userRegistration = await this.userService.RegisterAsync(user);
            var userRegistrationViewModel = this.responseModelMapper.UserRegistrationViewModel(userRegistration);
            
                return this.View("RegistrationVerification", userRegistrationViewModel);
        }
       
        public async Task<IActionResult> ActivateAccount([FromQuery] string userId, [FromQuery] string activationCode)
        {
            if (userId == null || activationCode == null)
            {
                return this.RedirectToAction("Index", "Home");
            }

            try
            {
                var message = await this.userService.ActivateAccountAsync(int.Parse(userId), activationCode);
                var activationViewModel = new ActivationViewModel
                {
                    Message = message
                };

                return this.View(activationViewModel);
            }
            catch (UnauthorizedAccessException)
            {
                ViewBag.ErrorMessage = "Invalid user.";
                return this.View("NotFound");
            }
        }        
    }
}
