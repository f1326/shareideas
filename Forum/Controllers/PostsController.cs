﻿using System.Threading.Tasks;

using Forum.Models;
using Forum.Services.Forum.Services.Contracts;

using Microsoft.AspNetCore.Mvc;

namespace Forum.Controllers
{
    public class PostsController : Controller
    {
        private readonly IPostService postService;
        public PostsController(IPostService postService)
        {
            this.postService = postService;
        }
        public IActionResult Index()
        {
            return this.View();
        }

        public async Task<IActionResult> Post(int id)
        {
            var post = await postService.GetPostAsync(id);
            var postViewModel = new PostViewModel(post);
            postViewModel.Likes = postService.GetPostLikes(id);
            return this.View(postViewModel);
        }
    }
}
