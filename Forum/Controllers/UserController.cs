﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using Forum.Helpers;
using Forum.Models;
using Forum.Models.Contracts;
using Forum.Models.UserViewModels;

using Microsoft.AspNetCore.Mvc;
using Forum.Services.Forum.Services.Contracts;
using Microsoft.AspNetCore.Hosting;


namespace Forum.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService userService;
        private readonly IAuthenticationHelper authHelper;
        private readonly IWebHostEnvironment environment;
        private readonly IRequestModelMapper requestModelMapper;
        private readonly IResponseViewModelMapper responseModelMapper;

        public UserController(IUserService userService, IAuthenticationHelper authHelper, IWebHostEnvironment environment, IRequestModelMapper mapper, IResponseViewModelMapper responseModelMapper)
        {
            this.userService = userService;
            this.authHelper = authHelper;
            this.environment = environment;
            this.requestModelMapper = mapper;
            this.responseModelMapper = responseModelMapper;
        }

        public async Task<IActionResult> Index(int id)
        {
            if (!this.HttpContext.Session.Keys.Contains("CurrentUser"))
            {
                return this.RedirectToAction("Login", "Auth");
            }

            var user = await userService.GetUserByIdAsync(id);

            var viewModel = new UserViewModel
            {
                UserId = user.Id,
                Username = user.Username,
                Email = user.Email,
                Role = user.Role,
                DisplayName = user.DisplayName,
                MemberSince = user.MemberSince,
                Rating = user.Rating,
                ProfileImagePath = user.ProfileImageUrl,
            };

            return View(viewModel);
        }

        [HttpPost]
        public async Task<IActionResult>  ProfilePicture(UserUpdateProfilePictureModel updateProfilePictureModel)
        {
            if (!this.HttpContext.Session.Keys.Contains("CurrentUser"))
            {
                return this.RedirectToAction("Login", "Auth");
            }

            try
            {
                int id = updateProfilePictureModel.UserId;
                var user = await this.userService.GetUserMinAsync(updateProfilePictureModel.UserId);

                var extension = Path.GetExtension(updateProfilePictureModel.Image.FileName).TrimStart('.');
                var physicalPath = $"{environment.WebRootPath}\\profileImages";
                var updatePictureRequestDto = this.requestModelMapper.ToUserUpdateProfilePictureRequestDto(updateProfilePictureModel);
                await this.userService.UpdateProfileImageAsync(updateProfilePictureModel.UserId, updatePictureRequestDto, physicalPath, extension);

                return RedirectToAction("Index", new { id = updateProfilePictureModel.UserId });
            }
            catch (Exception ex)
            {
                 return RedirectToAction("Index", updateProfilePictureModel.UserId);
            }
        }

        [HttpGet]
        //[AuthorizationAttribute(Role = "Admin")]
        public IActionResult ShowUsers()
        {
            if (!this.HttpContext.Session.Keys.Contains("CurrentUser"))
            {
                return this.RedirectToAction("Login", "Auth");
            }

            return this.View();
        }

        [HttpPost]
        // [AuthorizationAttribute(Role = "Admin")]
        public async Task<IActionResult> ShowUsersPagination([FromBody] PaginationViewModel model)
        {
            if (!this.HttpContext.Session.Keys.Contains("CurrentUser"))
            {
                return this.RedirectToAction("Login", "Auth");
            }

            int? rating = null;
            bool ratingInteger = int.TryParse(model.Rating, out int ratingInt);
            if (ratingInteger)
            {
                rating = ratingInt;
            }

            var usersPaginationResponse =
                await this.userService.GetAllPaginationAsync(model.Skip, model.Take, rating, model.SortType, model.Direction, model.UserStatus, model.SearchValue);
            var usersListResponseModel = new List<UserResponseViewModel>();
            foreach (var userDto in usersPaginationResponse.Items)
            {
                var userResponseViewModel = this.responseModelMapper.ToUserResponseViewModel(userDto);
                usersListResponseModel.Add(userResponseViewModel);
            }

            var usersListViewModel = new UsersListResponseViewModel
            {
                UsersList = usersListResponseModel,
                PageNumber = model.PageNumber,
                ItemsCount = usersPaginationResponse.TotalItemsCount,
                ItemsPerPage = model.Take
            };

            return this.PartialView("_UsersTablePartial", usersListViewModel);
        }

        [HttpGet]
        [AuthorizationAttribute(Role = "Admin")]
        public async Task<IActionResult> BlockUser(int id)
        {
            try
            {
                if (!this.HttpContext.Session.Keys.Contains("CurrentUser"))
                {
                    return this.RedirectToAction("Login", "Auth");
                }

                var userDto = await this.userService.GetUserByIdAsync(id);
                var userViewModel = this.responseModelMapper.ToUserResponseViewModel(userDto);

                return this.View(userViewModel);
            }
            catch (Exception)
            {
                return this.RedirectToAction(nameof(this.ShowUsers));
            }

        }

        [HttpGet]
        [AuthorizationAttribute(Role = "Admin")]
        public async Task<IActionResult> BlockUserConfirmation(int id)
        {
            try
            {
                var blocked = await this.userService.BlockUser(id);

                return this.View("ShowUsers");
            }
            catch (Exception)
            {
                return this.RedirectToAction(nameof(this.ShowUsers));
            }

        }

        [HttpGet]
        [AuthorizationAttribute(Role = "Admin")]
        public async Task<IActionResult> UnblockUser(int id)
        {
            try
            {
                var unblocked = await this.userService.UnblockUser(id);

                return this.View("ShowUsers");
            }
            catch (Exception)
            {

                return this.RedirectToAction(nameof(this.ShowUsers));
            }

        }
    }
}
