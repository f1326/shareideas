﻿using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

using Forum.Models;
using Forum.Models.Contracts;
using Forum.Services.Forum.Services.Contracts;

using Microsoft.AspNetCore.Mvc;

namespace Forum.Controllers
{
    public class HomeController : Controller
    {
        private readonly IPostService postService;
        private readonly IUserService userService;
        private readonly IResponseViewModelMapper responseViewModelMapper;

        public HomeController(IPostService postService, IUserService userService, IResponseViewModelMapper responseViewModelMapper)
        {
            this.postService = postService;
            this.userService = userService;
            this.responseViewModelMapper = responseViewModelMapper;
        }

        public async Task<IActionResult> Index()
        {
            var usersCount = await this.userService.GetUsersCountAsync();
            var postCount = await this.postService.CountPosts();
            var topMostCommentedPostsDtos = await this.postService.GetMostPopularPostsAsync(10);
            var topMostCommentedPostsViewModels = 
                topMostCommentedPostsDtos.Select(p => this.responseViewModelMapper.ToPostResponseViewModel(p));

            var latestPosts = await this.postService.GetLatestPostsAsync(10);
            var latestPostsViewModel = 
                latestPosts.Select(p => this.responseViewModelMapper.ToPostResponseViewModel(p));

            var homeViewModel = new HomeViewModel
            {
                NumberUsers = usersCount,
                NumberCreatedPosts = postCount,
                latestPostsViewModel = latestPostsViewModel,
                topMostCommentedPostsViewModel = topMostCommentedPostsViewModels
            };

            return View(homeViewModel);
        }

        public IActionResult LearnMore()
        {
            return View();
        }

        public IActionResult ContactUs()
        {
            return View();
        }
        
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
