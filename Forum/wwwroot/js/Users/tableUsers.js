﻿let currentPageNumber = 1;
const PageSize = 5;
let isFiltered = false;

getData(currentPageNumber);


$("#container-table").on("click", "#previous", function (event) {
    getPreviousPageData(1);
});

$("#container-table").on("click", "#next", function (event) {
    getNextPageData(1);
});

$("#container-table").on("click", "#previous-first", function (event) {
    getPreviousPageData(3);
});
$("#container-table").on("click", "#previous-second", function (event) {
    getPreviousPageData(2);
});
$("#container-table").on("click", "#previous-third", function (event) {
    getPreviousPageData(1);
});
$("#container-table").on("click", "#next-first", function (event) {
    getNextPageData(1);
});
$("#container-table").on("click", "#next-second", function (event) {
    getNextPageData(2);
});
$("#container-table").on("click", "#next-third", function (event) {
    getNextPageData(3);
});



$("#filter-form").on("click", "#btn-submit-filters", function (event) {
    let n = 1;
    currentPageNumber = n;
    getFilteredData(currentPageNumber);
});

$("#search-form").on("click", "#search", function (event) {

    getFilteredData(currentPageNumber);
});

var searchInput = document.getElementById('searchValue');

searchInput.addEventListener("keyup", function (event) {
    // Number 13 is the "Enter" key on the keyboard
    if (event.keyCode === 13) {
        // Cancel the default action, if needed
        event.preventDefault();
        // Trigger the button element with a click
        document.getElementById("search").click();
    }
});

function getFilteredData(newPageNumber) {
    let rating = document.getElementById('rating').value;
    let userStatus = document.getElementById('userStatus').value;
    let sortType = document.getElementById('sortType').value;
    let direction = document.getElementById('sortdirection').value;
    let searchValue = document.getElementById('searchValue').value;
    let page = newPageNumber;
    isFiltered = true;


    let skipAmount = (newPageNumber * PageSize) - PageSize;

    let data = JSON.stringify({
        skip: skipAmount,
        take: PageSize,
        pagenumber: newPageNumber,
        rating: rating,
        userStatus: userStatus,
        sortType: sortType,
        direction: direction,
        searchValue: searchValue
    });

    $.ajax({
        url: "/User/ShowUsersPagination",
        type: "POST",
        data: data,
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            $("#container-table").empty();
            $("#container-table").append(response);
        },
        error: function () {

        }
    });
}


function getPreviousPageData(decreasing) {
    if (currentPageNumber > 1) {
        currentPageNumber = currentPageNumber - decreasing;
        if (isFiltered) {
            getFilteredData(currentPageNumber);
        } else {
            getData(currentPageNumber);
        }

    }
}

function getNextPageData(increasing) {
    currentPageNumber = currentPageNumber + increasing;
    if (isFiltered) {
        getFilteredData(currentPageNumber);
    } else {
        getData(currentPageNumber);
    }
}


function getData(newPageNumber) {
    let skipAmount = (newPageNumber * PageSize) - PageSize;

    let data = JSON.stringify({
        skip: skipAmount,
        take: PageSize,
        pagenumber: newPageNumber
    });

    $.ajax({
        url: "/User/ShowUsersPagination",
        type: "POST",
        data: data,
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            $("#container-table").empty();
            $("#container-table").append(response);
        },
        error: function () {

        }
    });
}