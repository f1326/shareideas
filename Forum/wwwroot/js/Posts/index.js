﻿var page = 1;
var maxPage;
var maxPageFilter;
var settingsGetPageCount = {
    "url": "http://localhost:6001/api/Posts/Count",
    "method": "GET",
    "timeout": 0,
    "headers": {
        "Content-Type": "application/json"
    },
};

$.ajax(settingsGetPageCount).done(function (response) {
    console.log(response)
    calculatePages(response)
});

var table = document.getElementById(`posts-table`)

/* buildTable();*/
filterPosts();
function buildTable() {

    var settings2 = {
        "url": `http://localhost:6001/api/Posts/Page?page=${page}`,
        "method": "GET",
        "timeout": 0,
        "headers": {
            "Content-Type": "application/json"
        },
    };
    $.ajax(settings2).done(function (response) {
        table.innerHTML = "";
        response.forEach(element =>
            table.innerHTML += `
          <div class="forum-item">
        <div class="row">
            <div class="col-md-9">
                <div class="forum-icon">
                    <i class="fa fa-comments" aria-hidden="true"></i>
                </div>
                <a href="/Posts/Post/${element.id}" class="forum-item-title">${element.title}</a>
                <div class="forum-sub-title">${element.content} </div>
            </div>
            <div class="col-md-1 forum-info">
                <span class="views-number">
                    ${element.comments.length}
                </span>
                <div>
                    <small>Comments</small>
                </div>
            </div>
            <div class="col-md-1 forum-info">
                <span class="views-number">
                    ${element.likes.length}
                </span>
                <div>
                    <small>Likes</small>
                </div>
            </div>
        </div>
    </div>

                `)
        table.innerHTML += `<div id="pagination-wrapper">
    </div>`
        pageButtons(maxPage);
    })

}
function nextPage() {
    page += 1;
    table.innerHTML = `<div id="spinner" class="loading loading--full-height"></div>`;
    buildTable();
}
function prevPage() {
    page -= 1;
    table.innerHTML = `<div id="spinner" class="loading loading--full-height"></div>`;
    buildTable();
}
function nextPageFlt() {
    page += 1;
    table.innerHTML = `<div id="spinner" class="loading loading--full-height"></div>`;
    filterPosts();
}
function prevPageFlt() {
    page -= 1;
    table.innerHTML = `<div id="spinner" class="loading loading--full-height"></div>`;
    filterPosts();
}

function calculatePages(posts) {
    maxPage = Math.ceil(posts / 7);
    console.log(`Pages: ${maxPage}`);
}
$(document).on('click', '#addPostBtn', function () {

    const postTitleInput = document.getElementById('postTitleInput').value;
    const postCommentInput = document.getElementById('postCommentInput').value;
    var authorizationEmail = document.getElementById(`userEmail`).value;
    var authorizationPassword = document.getElementById(`userPassword`).value;

    if (postTitleInput.length < 5) {
        document.getElementById(`addPostHead`).textContent = "Post title must be atleast 5 characters"
    } else if (postCommentInput < 10) {
        document.getElementById(`addPostHead`).textContent = "Post content must be atleast 10 characters"
    } else {
        var settingsPost = {
            "url": `http://localhost:6001/api/Posts`,
            "method": "POST",
            "timeout": 0,
            "headers": {
                "Content-Type": "application/json",
                "authorizationEmail": authorizationEmail,
                "authorizationPassword": authorizationPassword
            },
            "data": JSON.stringify({
                "title": postTitleInput,
                "content": postCommentInput
            }),
        };
        $.ajax(settingsPost).done(function (response) {
            location.replace(`http://localhost:5000/Posts/Post/${response.id}`)
        });
    }

});

function filterPosts() {
    var sortingValue = document.getElementById(`sorting-options-posts`).value;
    var sortingDirection = document.getElementById(`sorting-direction-posts`).value;
    var startDate = document.getElementById(`filter-start-date-posts`).value.toString();
    var endDate = document.getElementById(`filter-end-date-posts`).value.toString();
    var titleValue = document.getElementById(`searchInput`).value;

    var filterCount = {
        "url": "http://localhost:6001/api/Posts/Filter/Count",
        "method": "POST",
        "timeout": 0,
        "headers": {
            "Content-Type": "application/json"
        },
        "data": JSON.stringify({
            "DateStart": startDate,
            "DateEnd": endDate,
            "TitleContains": titleValue
        }),
    };

    $.ajax(filterCount).done(function (response) {
        maxPageFilter = Math.ceil(response / 7);
    });

    var settings = {
        "url": `http://localhost:6001/api/Posts/FilterPage?page=${page}`,
        "method": "POST",
        "timeout": 0,
        "headers": {
            "Content-Type": "application/json"
        },
        "data": JSON.stringify({
            "SortingType": sortingValue,
            "SortingDirection": sortingDirection,
            "DateStart": startDate,
            "DateEnd": endDate,
            "TitleContains": titleValue
        }),
    };

    $.ajax(settings).done(function (response) {
        table.innerHTML = "";
        response.forEach(element =>
            table.innerHTML += `
          <div class="forum-item">
        <div class="row">
            <div class="col-md-9">
                <div class="forum-icon">
                    <i class="fa fa-comments" aria-hidden="true"></i>
                </div>
                <a href="/Posts/Post/${element.id}" class="forum-item-title">${element.title}</a>
                <div class="forum-sub-title">${element.content} </div>
            </div>
            <div class="col-md-1 forum-info">
                <span class="views-number">
                    ${element.comments.length}
                </span>
                <div>
                    <small>Comments</small>
                </div>
            </div>
            <div class="col-md-1 forum-info">
                <span class="views-number">
                    ${element.likes.length}
                </span>
                <div>
                    <small>Likes</small>
                </div>
            </div>
        </div>
    </div>
                `)
        table.innerHTML += `<div id="pagination-wrapper">
    </div>`
        pageButtonsFilter(maxPageFilter)
    });
}

function pageButtons(pages) {

    var wrapper = document.getElementById('pagination-wrapper')

    wrapper.innerHTML = ``

    var maxLeft = (page - 1)
    var maxRight = (page + 1)

    if (maxLeft < 1) {
        maxLeft = 1
        maxRight = 3
    }

    if (maxRight > pages) {
        maxLeft = pages - (3 - 1)

        if (maxLeft < 1) {
            maxLeft = 1
        }
        maxRight = pages
    }

    for (var _page = maxLeft; _page <= maxRight; _page++) {
        if (_page == page) {
            wrapper.innerHTML += `<button value=${_page} class="page btn btn btn-secondary">${_page}</button>`

        } else {
            wrapper.innerHTML += `<button value=${_page} class="page btn btn btn-outline-secondary">${_page}</button>`
        }
    }

    if (page != 1) {
        wrapper.innerHTML = `<button value=${1} class="page btn  btn btn-outline-secondary">&#171; First</button>` + wrapper.innerHTML
    }

    if (page != pages) {
        wrapper.innerHTML += `<button value=${pages} class="page btn btn-outline-secondary">Last &#187;</button>`
    }

    $('.page').on('click', function () {
        $('#table-body').empty()

        page = Number($(this).val())

        buildTable()
    })

}

function pageButtonsFilter(pages) {

    var wrapper = document.getElementById('pagination-wrapper')

    wrapper.innerHTML = ``

    var maxLeft = (page - 1)
    var maxRight = (page + 1)

    if (maxLeft < 1) {
        maxLeft = 1
        maxRight = 3
    }

    if (maxRight > pages) {
        maxLeft = pages - (3 - 1)

        if (maxLeft < 1) {
            maxLeft = 1
        }
        maxRight = pages
    }

    for (var _page = maxLeft; _page <= maxRight; _page++) {
        if (_page == page) {
            wrapper.innerHTML += `<button value=${_page} class="pageFlt btn btn-secondary">${_page}</button>`
        } else {
            wrapper.innerHTML += `<button value=${_page} class="pageFlt btn btn-outline-secondary">${_page}</button>`
        }
    }

    if (page != 1) {
        wrapper.innerHTML = `<button value=${1} class="pageFlt btn btn-outline-secondary">&#171; First</button>` + wrapper.innerHTML
    }

    if (page != pages) {
        wrapper.innerHTML += `<button value=${pages} class="pageFlt btn btn-outline-secondary">Last &#187;</button>`
    }

    $('.pageFlt').on('click', function () {
        $('#table-body').empty()

        page = Number($(this).val())

        filterPosts()
    })

}
