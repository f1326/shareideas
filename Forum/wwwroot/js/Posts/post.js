﻿
function clearInputs() {
    postReplyTitleInput = document.getElementById('postReplyTitleInput').value = "";
    postReplyCommentInput = document.getElementById('postReplyCommentInput').value = "";
    document.getElementById(`editCommentHeadError`).textContent = ""
    document.getElementById(`addCommentHead`).textContent = ""
}

function likePost() {

    var postId = document.getElementById(`postId`).value;
    var userId = document.getElementById(`userId`).value;
    var authorizationEmail = document.getElementById(`userEmailPost`).value;
    var authorizationPassword = document.getElementById(`userPasswordPost`).value;

    var settings = {
        "url": `http://localhost:6001/api/Posts/${postId}/Like`,
        "method": "POST",
        "timeout": 0,
        "headers": {
            "Content-Type": "application/json",
            "authorizationEmail": authorizationEmail,
            "authorizationPassword": authorizationPassword
        },
        "data": JSON.stringify({
            "userId": `${userId}`
        }),
    };

    $.ajax(settings).done(function (response) {
        var addLike = parseInt(document.getElementById(`post-likes-count`).textContent) + 1;
        document.getElementById(`like-btn`).style.display = "none";
        document.getElementById(`remove-like-btn`).style.display = "inline";
        document.getElementById(`post-likes-count`).textContent = addLike;
    });

}
function dislikePost() {

    var postId = document.getElementById(`postId`).value;
    var userId = document.getElementById(`userId`).value;
    var authorizationEmail = document.getElementById(`userEmailPost`).value;
    var authorizationPassword = document.getElementById(`userPasswordPost`).value;

    var settings = {
        "url": `http://localhost:6001/api/Posts/${postId}/Like`,
        "method": "DELETE",
        "timeout": 0,
        "headers": {
            "Content-Type": "application/json",
            "authorizationEmail": authorizationEmail,
            "authorizationPassword": authorizationPassword
        },
        "data": JSON.stringify({
            "userId": `${userId}`
        }),
    };

    $.ajax(settings).done(function (response) {
        var removeLike = parseInt(document.getElementById(`post-likes-count`).textContent) - 1;
        document.getElementById(`like-btn`).style.display = "inline";
        document.getElementById(`remove-like-btn`).style.display = "none";
        document.getElementById(`post-likes-count`).textContent = removeLike;
    });
}


function editComment() {
    var authorizationEmail = document.getElementById(`userEmailPost`).value;
    var authorizationPassword = document.getElementById(`userPasswordPost`).value;
    var postId = document.getElementById(`postId`).value;

    var titpleInput = document.getElementById(`editPostReplyTitleInput`).value;
    var contentInput = document.getElementById(`editPostReplyCommentInput`).value;
    var idInput = document.getElementById(`editPostReplyId`).value;
    postId

    var settings = {
        "url": `http://localhost:6001/api/Posts/${postId}/comments/${idInput}`,
        "method": "PUT",
        "timeout": 0,
        "headers": {
            "Content-Type": "application/json",
            "authorizationEmail": authorizationEmail,
            "authorizationPassword": authorizationPassword
        },
        "data": JSON.stringify({
            "title": titpleInput,
            "content": contentInput
        }),
    };

    $.ajax(settings).done(function (response) {
        document.getElementById(`comment-tittle-${idInput}`).textContent = titpleInput;
        document.getElementById(`comment-content-${idInput}`).textContent = contentInput;

        document.getElementById(`editAlertPlaceholder-${idInput}`).innerHTML =
            `
                 <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>Hooray!</strong> Comment updated successfully!
                     <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>`
        //document.getElementById(`editCommentHead`).textContent = "Comment UPDATED!";
        $("#exit-reply-model-edit").click()
    });
}

function addPostDataToFields() {
    var titleValue = document.getElementById(`post-title`).textContent;
    var contentValue = document.getElementById(`post-content`).textContent;

    document.getElementById(`editPostTitleInput`).value = titleValue;
    document.getElementById(`editPostCommentInput`).value = contentValue;
}

function editPost() {
    var authorizationEmail = document.getElementById(`userEmailPost`).value;
    var authorizationPassword = document.getElementById(`userPasswordPost`).value;
    var postId = document.getElementById(`postId`).value;

    var titpleInput = document.getElementById(`editPostTitleInput`).value;
    var contentInput = document.getElementById(`editPostCommentInput`).value;
    var settings = {
        "url": `http://localhost:6001/api/Posts/${postId}`,
        "method": "PUT",
        "timeout": 0,
        "headers": {
            "Content-Type": "application/json",
            "authorizationEmail": authorizationEmail,
            "authorizationPassword": authorizationPassword
        },
        "data": JSON.stringify({
            "title": titpleInput,
            "content": contentInput
        }),
    };

    $.ajax(settings).done(function (response) {
        document.getElementById(`post-title`).textContent = titpleInput;
        document.getElementById(`post-content`).textContent = contentInput;
        //document.getElementById(`editPostHead`).textContent = "Post UPDATED!";
        document.getElementById(`liveAlertPlaceholderEdit`).innerHTML = `
                <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>Hooray!</strong> Post updated successfully!
                     <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>`
        $("#exit-post-model-edit").click()

    });
}

function addCommentDataToFields(id) {
    var titleValue = document.getElementById(`comment-tittle-${id}`).textContent;
    var contentValue = document.getElementById(`comment-content-${id}`).textContent;

    document.getElementById(`editPostReplyTitleInput`).value = titleValue;
    document.getElementById(`editPostReplyCommentInput`).value = contentValue;
    document.getElementById(`editPostReplyId`).value = id;
}

function showDeleteAlertComment(id) {
    var alertPlaceholder = document.getElementById(`liveAlertPlaceholder-${id}`)
    alertPlaceholder.innerHTML = `
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong>Holy guacamole!</strong> Are you sure you want to delete this reply? &nbsp &nbsp &nbsp &nbsp &nbsp
            <button type="button" class="btn btn-outline-danger" onclick="deletePostComment(${id})">Delete</button>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>`
}

function showDeleteAlertPost(id) {
    var alertPlaceholder = document.getElementById(`liveAlertPlaceholderDelete`)
    alertPlaceholder.innerHTML = `
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong>Holy guacamole!</strong> Are you sure you want to delete this post? &nbsp &nbsp &nbsp &nbsp &nbsp
            <button type="button" class="btn btn-outline-danger" onclick="deletePost(${id})">Delete</button>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>`
}

function deletePostComment(id) {
    var commentToDelete = document.getElementById(`comment-${id}`);
    commentToDelete.innerHTML = ``;
    var authorizationEmail = document.getElementById(`userEmailPost`).value;
    var authorizationPassword = document.getElementById(`userPasswordPost`).value;

    var settingsDeletePostReply = {
        "url": `http://localhost:6001/api/Posts/comments/${id}`,
        "method": "DELETE",
        "timeout": 0,
        "headers": {
            "Content-Type": "application/json",
            "authorizationEmail": authorizationEmail,
            "authorizationPassword": authorizationPassword
        }
    };

    $.ajax(settingsDeletePostReply).done(function (response) {
        console.log(response);
    });
}
function deletePost(id) {
    document.getElementById('main').innerHTML = `
        <div class="alert alert-success" role="alert">
          <h4 class="alert-heading">Post deleted!</h4>
          <p>You succesfuly deleted this post and all of his comments.</p>
          <hr>
          <p class="mb-0">If you have any problems with the forum contact us!</p>
        </div>
        `;

    var authorizationEmail = document.getElementById(`userEmailPost`).value;
    var authorizationPassword = document.getElementById(`userPasswordPost`).value;

    var settingsDeletePostReply = {
        "url": `http://localhost:6001/api/Posts/${id}`,
        "method": "DELETE",
        "timeout": 0,
        "headers": {
            "Content-Type": "application/json",
            "authorizationEmail": authorizationEmail,
            "authorizationPassword": authorizationPassword
        }
    };

    $.ajax(settingsDeletePostReply).done(function (response) {
        console.log(response);
    });
}
$(document).on('click', '#addPostReplyBtn', function () {

    const postId = document.getElementById('postId').value;
    const postReplyTitleInput = document.getElementById('postReplyTitleInput').value;
    const postReplyCommentInput = document.getElementById('postReplyCommentInput').value;
    var authorizationEmail = document.getElementById(`userEmailPost`).value;
    var authorizationPassword = document.getElementById(`userPasswordPost`).value;

    var noPostReply = document.getElementById(`noPostReply`);
    var comments = document.getElementById(`comments`);

    if (postReplyTitleInput.length < 5) {

        document.getElementById(`editCommentHeadError`).textContent = "Comment title must be atleast 5 characters"
    } else if (postReplyCommentInput.length < 10) {

        document.getElementById(`editCommentHeadError`).textContent = "Comment content must be atleast 10 characters"
    }
    else {
        var settingsPostReply = {
            "url": `http://localhost:6001/api/Posts/${postId}/comments`,
            "method": "POST",
            "timeout": 0,
            "headers": {
                "Content-Type": "application/json",
                "authorizationEmail": authorizationEmail,
                "authorizationPassword": authorizationPassword
            },
            "data": JSON.stringify({
                "title": postReplyTitleInput,
                "content": postReplyCommentInput
            }),
        };
        $.ajax(settingsPostReply).done(function (response) {
            var today = new Date();
            var date = today.getFullYear() + '.' + (today.getMonth() + 1) + '.' + today.getDate();
            var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
            var dateTime = date + ' г. ' + time;
            if (noPostReply != null) {
                noPostReply.innerHTML = "";
            }
            var commentsCopy = comments.innerHTML;
            comments.innerHTML = `
        <br />
    <div class="card" id="comment-${response.id}">
        <div id="editAlertPlaceholder-${response.id}"></div>
        <div id="liveAlertPlaceholder-${response.id}">
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Hooray!</strong> Succesfully added coment!
                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        </div>
        <div class="card-body row">
            <div class="col-2 text-center">
                <img src="${response.userPicturePath}" width="60px" />
                <br />
                <p>${response.userDisplayName}</p>
                <br />
                            Points:
                            (${response.userRating})
                        </div>
            <div class="col-10">
                <p class="card-text comment-title" id="comment-tittle-${response.id}"> ${postReplyTitleInput}</p>
                <p class="card-text" id="comment-content-${response.id}"> ${postReplyCommentInput}</p>
            </div>
        </div>
        <div class="card-footer text-muted">
            Posted on: ${dateTime}
            <div class="post-options-btns">
                <button type="button" class="btn btn-outline-success" onclick="addCommentDataToFields(${response.id})" data-bs-toggle="modal" data-bs-target="#editPostReplyModal">Edit</button>
                <button type="button" class="btn btn-outline-danger" onclick="showDeleteAlertComment(${response.id})">Delete</button>
            </div>
        </div>
    </div>
    <br>`
            comments.innerHTML += commentsCopy;
            document.getElementById(`editCommentHeadError`).textContent = ""
            document.getElementById(`addCommentHead`).textContent = "Comment added!"
            $("#exit-reply-model").click()
        });
    }

});
