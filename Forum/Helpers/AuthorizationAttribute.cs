﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Http;
using System;
using Forum.Exceptions;

namespace Forum.Helpers
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public class AuthorizationAttribute : Attribute, IAuthorizationFilter
    {
        public string Role { get; set; }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var role = context.HttpContext.Session.GetString("Role");
            if (!role.Equals(this.Role, StringComparison.CurrentCultureIgnoreCase))
            {
                throw new AuthorizationException($"You are not authorized.");
            }
        }
    }
}
