﻿
using Forum.Exceptions;
using Forum.Services.Exceptions;
using Forum.Services.Forum.Services.Contracts;
using Forum.Services.Forum.Services.DTOs;
using System.Threading.Tasks;

namespace Forum.Helpers
{
    public class AuthenticationHelper : IAuthenticationHelper
    {
        private readonly IUserService userService;
        public AuthenticationHelper(IUserService userService)
        {
            this.userService = userService;
        }

        
        public async Task<UserResponseDto> TryGetUser(string email, string password)
        {
            try
            {
                var user = await this.userService.GetUserByEmailAsync(email);
                if (user.Password != password)
                {
                    throw new AuthenticationException("Invalid password!");
                }
                if (!user.IsActive)
                {
                    throw new AuthenticationException("The account is not confirmed with verification link!");
                }

                return user;
            }
            catch (EntityNotFoundException)
            {
                throw new EntityNotFoundException($"User with email: {email} doesn't exist!");
            }
        }
    }
}
