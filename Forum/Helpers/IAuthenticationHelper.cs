﻿using Forum.Services.Forum.Services.DTOs;
using System.Threading.Tasks;

namespace Forum.Helpers
{
    public interface IAuthenticationHelper
    {
        Task<UserResponseDto> TryGetUser(string username, string password);
    }
}
