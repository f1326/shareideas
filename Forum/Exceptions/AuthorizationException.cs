﻿using System;

namespace Forum.Exceptions
{
    public class AuthorizationException : ApplicationException
    {
        public AuthorizationException(string msg)
            : base(msg)
        {  
        }
    }
}
