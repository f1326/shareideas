﻿using System;

namespace Forum.Exceptions
{
    public class AuthenticationException : ApplicationException
    {
        public AuthenticationException(string msg) 
            : base(msg)
        {
        }
    }
}
