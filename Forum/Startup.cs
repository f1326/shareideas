
using System;

using Forum.Data;
using Forum.Helpers;
using Forum.Models;
using Forum.Models.Contracts;
using Forum.Services.Forum.Services;
using Forum.Services.Forum.Services.Contracts;
using Forum.Services.Models;
using Forum.Services.Models.Contracts;
using Forum.Services.Providers;
using Forum.Services.Providers.Contracts;
using Forum.WebApi.Helpers;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Forum
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromSeconds(1000);
                options.Cookie.HttpOnly = true;
                options.Cookie.IsEssential = true;
            });
            services.AddControllersWithViews();
            services.AddScoped<IAuthenticationHelper, AuthenticationHelper>();
            services.AddScoped<IAuthHelper, AuthHelper>();
            services.AddScoped<IPostService, PostService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ICommentService, CommentService>();
            services.AddScoped<IFilterProvider, FilterProvider>();
            services.AddScoped<IRequestMapper, RequestMapper>();
            services.AddScoped<ISortProvider, SortProvider>();
            services.AddScoped<IRequestModelMapper, RequestModelMapper>();
            services.AddScoped<IResponseViewModelMapper, ResponseViewModelMapper>();

            var connectionString = this.Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<ForumDbContext>(options =>
            {
                options.UseSqlServer(connectionString).EnableSensitiveDataLogging();
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseStaticFiles();

            app.UseRouting();

            app.UseSession();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
